<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Models\Consultation;

class ConsultationController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['meta']['title']	= 'Nair online consultation';
		$this->context['meta']['desc'] = 'From creams to waxes, our online consultation will help you choose the right hair removal product for you.';
		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= 'main/layouts_consultation';
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		return view('Main.Consultation.index', $this->context);
	}

	public function questions()
	{
		
		$this->context['pageViewJS']	= 'Main.Consultation.partials.consultation_js';

		return view('Main.Consultation.questions', $this->context);
	}

	public function results(Consultation $consultation, string $choices)
	{
		// http://local.nair/online-consultation/results/4-2-3-5-4-3
		// $choices = '4-2-3-5-4-3'

		// convert $choices to array
		$answers = explode("-",$choices);

		// check to see if all answers are numeric, if not redirect back to start
		foreach($answers as $ans) {
			if (!is_numeric($ans)) {
				redirect("/online-consultation");
			}
		}

		// if all the answers are good
		$indexers = $answers; // store copy of answers array

		// build string to compare against nair-2k18.consultation.REF to find relevant products
		$indexers[0] = 0;
		$answerstring = implode("-",$indexers);
		// $answerstring = '0-2-3-5-4-3'		
		$products = $consultation->getProducts($answerstring);
		$selectedProducts = [];
	    $productMatrix = config('consultation.productMatrix');

	    // collate selected products
	    foreach ($productMatrix AS $productIndex ) {
	    	if ($products[0][ $productIndex['field'] ] === 1) {
	    		$selectedProducts[] = $productIndex;
	    	}
	    }

		// question  / answer / outcome blocks based on choices made 
	    $answerMatrix = config('consultation.answerMatrix');
	    $items = [];
	    foreach ($answers AS $key => $value) {
	    	$answer = $answerMatrix[$key]['answers'][$value - 1];
			$items[] = [
				'question' => $answerMatrix[$key]['question'],
				'response' => $answer['response'],
				'text' => $answer['text']
			];	    	
	    }

	    // select random toptip
	    $toptips = config('consultation.toptips');
		shuffle($toptips);

		$this->context['pageViewJS']	= 'Main.Consultation.partials.consultation_results_js';
		$this->context['choices'] = $answers;
		$this->context['products'] = $selectedProducts;
		$this->context['items'] = $items;
		$this->context['toptip'] = $toptips[0]; // pick first toptip in shuffled deck

		return view('Main.Consultation.results', $this->context);
	}

}
