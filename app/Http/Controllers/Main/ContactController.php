<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use App\Http\Requests\Main\ContactRequest;
use App\Models\Contact;
use DB;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ContactController extends MainController {

	public function __construct()
	{
		parent::__construct();

		$this->context['meta']['title']	= 'Contact us';
		$this->context['meta']['desc'] = 'If you have a question about the Nair Range, please get in touch here.';
		$this->context['pageViewJS']	= 'Main.Contact.partials.contact_js';
		$this->context['pageViewCSS']	= 'main/layouts_contactus';

		$this->context['titleOptions'] = ['' => '- Select -', 'Mr' => 'Mr', 'Mrs' => 'Mrs', 'Miss' => 'Miss', 'Ms' => 'Ms'];
		$this->context['tabs'] = [
			[
				'name' => 'enquiries',
				'label' => 'Enquiries',
				'url' => '/contact-us'
			],
			[
				'name' => 'images',
				'label' => 'Images',
				'url' => '/contact-us/images'
			],
			[
				'name' => 'press-releases',
				'label' => 'Press Releases',
				'url' => '/contact-us/press-releases'
			]
		];
	}

	public function index()
	{
		$this->context['tab'] = 'enquiries';
		$this->context['mode'] = 'form';
		return view('Main.Contact.index', $this->context);
	}

	public function images()
	{
		$this->context['tab'] = 'images';
		$this->context['mode'] = 'form';
		return view('Main.Contact.images', $this->context);
	}

	public function press_releases()
	{
		$this->context['tab'] = 'press-releases';
		$this->context['mode'] = 'form';
		return view('Main.Contact.press_releases', $this->context);
	}

	public function store(ContactRequest $request, Contact $contact)
	{	

		// validation rules in EnquiryRequest.rules()
		// if execution reaches this point the form has passed validation

		$details = array(
			'title'			=> $request->title,
			'firstname'		=> $request->firstname,
			'surname'		=> $request->surname,
			'company'		=> ( gettype($request->company ) !== 'NULL' ) ? $request->company : '-',
			'phone'			=> $request->phone,
			'email'			=> $request->email,
			'product'		=> ( gettype($request->product ) !== 'NULL' ) ? $request->product : '-',
			'comments'		=> $request->comments
		);

		$contact->fill($details);
		$contact->save();

		// send response meail to user supplied email address
		Mail::send(['html' => 'Main.Emails.Contact.user'], $details, function($message) use($details)
		{
		    $message->from(env('FROM_ENQUIRY'), env('EMAIL_FROM_NAME'));
		    $message->to( $details['email'] );
		    $message->subject('Your Enquiry');
		});

		// send email to admin email address 
		Mail::send(['text' => 'Main.Emails.Contact.admin'], $details, function($message) use( $details )
		{
		    $message->from(env('FROM_ENQUIRY'), env('EMAIL_FROM_NAME'));
		    $message->to( env('EMAIL_ENQUIRY') );
			if ( env('EMAIL_ENQUIRY_CC') !== '') {
				$message->cc(env('EMAIL_ENQUIRY_CC'));	
			}
		    $message->subject('Enquiry - Form Submission');
		});

		$this->context['mode'] = 'thanks';
		return view('Main.Contact.index', $this->context);
	}

}
