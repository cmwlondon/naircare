<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class EffectiveController extends MainController {

	public function __construct()
	{
		parent::__construct();

		$this->context['meta']['desc'] = 'Each Nair formula is designed to gently remove hair from below the surface of your skin to leave it feeling silky smooth for much longer than shaving.';
		$this->context['pageViewJS']	= 'Main.Effective.partials.effective_js';
		$this->context['pageViewCSS']	= 'main/layouts_effective';

		$this->pagedata = array(
			'shaving' => array(
				'title' => 'Nair effective hair removal - Shaving',
				'framecount' => 31
			),
			'wax' => array(
				'title' => 'Nair effective hair removal - Waxes',
				'framecount' => 25
			),
			'creams' => array(
				'title' => 'Nair effective hair removal - Creams',
				'framecount' => 38
			)
		);
	}

	public function index($section = 'shaving')
	{
		$this->context['section'] = $section;
		$this->context['framecount'] = $this->pagedata[$section]['framecount'];
		$this->context['meta']['title'] = $this->pagedata[$section]['title'];
		return view('Main.Effective.'.$section, $this->context);
	}

}
