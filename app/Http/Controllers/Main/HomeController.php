<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class HomeController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';
	}

	/**
	 * Show the application home screen to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		
		$this->context['meta']['title']	= 'Nair Hair Removal';
		$this->context['meta']['desc'] = '';
		$this->context['pageViewCSS']	= 'main/home_v6/layouts_home';

		// $this->context['pageViewJS']	= 'Main.home.partials.home_js';
		// $this->context['pageViewCSS']	= 'main/pages/layouts_home';

		return view('Main.Home.home', $this->context);
	}

	/*
	v1
	v2
	v3
	v5
	v6
	argon_oil
	*/
}
