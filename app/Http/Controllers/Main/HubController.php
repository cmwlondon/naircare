<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class HubController extends MainController {

	public function __construct()
	{
		parent::__construct();

		$this->context['pageViewJS']	= 'Main.Hub.partials.hub_js';
		$this->context['pageViewCSS']	= 'main/layouts_hub';

		$this->hubPageTitles = array(
			'wax' => array (
				'title' => 'Wax from Nair',
				'description' => 'Nair Wax Kits can be used on even the shortest hair with beautiful results that last for weeks.'
			),
			'cream' => array (
				'title' => 'Cream from Nair',
				'description' => 'Nair Creams are the fast and fuss-free way to achieve that silky smooth feeling.'
			),
			'face' => array (
				'title' => 'Remove hair from your face',
				'description' => 'Nair face products are specially developed and dermatologically tested to remove hair on delicate skin.'
			),
			'underarm' => array (
				'title' => 'Take Care of Your Underarms',
				'description' => 'Effectively remove hair from your underarms with the Nair range.'
			),
			'bikini' => array (
				'title' => 'Take care of your Bikini Area',
				'description' => 'Nair Products gently remove hair from more sensitive areas to make sure your skin is left radiant and smooth.'
			),
			'legs' => array (
				'title' => 'Take Care of your Legs',
				'description' => 'Remove hair and get beautifully smooth legs with Nair creams and waxes.'
			),
			'everywhere' => array (
				'title' => 'Nair Everywhere',
				'description' => 'Find a product for every hair removal need in the Nair range.'
			),
			'pamper' => array (
				'title' => 'Pamper with Nair',
				'description' => 'Nair pampering creams and waxes make hair removal an indulgent experience that leaves you with silky smooth skin.'
			),
			'sensitive-skin' => array (
				'title' => 'Nair Sensitive',
				'description' => 'Nair Sensitive Range is enriched with protective and nourishing ingredients to ensure that it gently removes hair on even the most sensitive skin.'
			)

		);
		/*
		/hub/cream
		/hub/face
		/hub/underarm
		/hub/bikini
		/hub/legs
		/hub/everywhere
		/hub/pamper
		/hub/sensitive-skin
		*/
	}

	public function index(string $section)
	{
		
		$this->context['meta']['title']	= $this->hubPageTitles[$section]['title'];
		$this->context['meta']['desc'] = $this->hubPageTitles[$section]['description'];

		return view('Main.Hub._section_'.$section, $this->context);
	}


}
