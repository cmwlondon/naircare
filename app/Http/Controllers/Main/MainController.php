<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Controller;
use Request;

class MainController extends Controller {
	
	public function __construct() {
		$this->context = array(
			'pageViewCSS' 		=> "",
			'pageViewJS'		=> "",
			// 'site_url'			=> env('APP_URL'),
			'site_url'			=> url('',[]),
			'class_section'	=> '',
			'meta'				=> [
									'title' => 'Nair',
									'link' => url(Request::path()),
									'pagetype' => 'article',
									'desc' => 'Naircare.co.uk is home to our innovative range of hair removal products that leave your skin feeling beautifully smooth all over.',
									'image' => url('/images/orajel-logo.png')
									]
		);
	}
}