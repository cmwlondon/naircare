<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class MiscController extends MainController {

	public function __construct()
	{
		parent::__construct();

		$this->context['pageViewJS']	= 'Main.Misc.partials.misc_js';
		$this->context['pageViewCSS']	= 'main/layouts_legal';

	}

	public function index()
	{
		return view('Main.Misc.index', $this->context);
	}

	public function terms()
	{
		$this->context['meta']['title']	= 'Terms and Conditions';
		$this->context['meta']['desc'] = 'Terms and Conditions for naircare.co.uk. Nair takes care of hair.';
		return view('Main.Misc.terms', $this->context);
	}

	public function cookies()
	{
		$this->context['meta']['title']	= 'Cookie Notice';
		$this->context['meta']['desc'] = 'Cookie notice for naircare.co.uk.  Nair takes care of hair.';
		return view('Main.Misc.cookies', $this->context);
	}

	public function privacy()
	{
		$this->context['meta']['title']	= 'Policies';
		$this->context['meta']['desc'] = 'Policies for naircare.co.uk.  Nair takes care of hair.';
		return view('Main.Misc.privacy', $this->context);
	}

	public function mailtest()
	{
		// example showing how to pass arbitrary variables/values into the mail send method
		$details = array(
			'goto' => 'hockey.richard@gmail.com',
			'title'				=> 'TITLE',
			'firstname'		=> 'firstname',
			'surname'		=> 'surname',
			'company'			=> 'acme',//$request->company,
			'phone'				=> 'phone',
			'email'				=> 'email',
			'product'			=> 'product',
			'comments'		    => 'comments'
		);

		Mail::send(['text' => 'Main.Emails.Contact.admin'], $details, function($message) use( $details )
		// Mail::send(['html' => 'Main.Emails.Contact.user'], $details, function($message) use($details)
		{
		    $message->from(env('FROM_ENQUIRY'), env('EMAIL_FROM_NAME'));
		    $message->to( $details['goto'] );
			$message->cc(env('EMAIL_ADMIN_CC'));
		    $message->subject('Nair Enquiry');
		});

		return view('Main.Misc.mailtest', $this->context);
	}
}
