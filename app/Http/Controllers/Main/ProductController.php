<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;
use Illuminate\Support\Facades\Route;
use Request;
use Response;
use App\Models\ProductReviews;
use App\Http\Requests\Main\ProductReviewRequest;
use Mail;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ProductController extends MainController {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(Request $request)
	{
		parent::__construct();

		$this->context['pageViewJS']	= '';
		$this->context['pageViewCSS']	= '';
		$this->context['meta']['desc'] = 'Nair hair removal products including hair removal cream for your face, legs and bikini line.';
		$this->range = $request::route()->getName();
		$this->context['routename'] = $this->range;

		$this->productPageTitle = array(
			'japanese-cherry-blossom' => array(
				'body-wax-strips' => 'Oil Body Wax Strips',
				'facial-wax-strips' => 'Facial Wax Strips'
			),
			'argan-oil' => array(
				'body-wax' => 'Salon Divine Body Wax',
				'upper-lip-kit' => 'Upper Lip Kit',
				'facial-brush-on' => 'Facial Brush-On',
				'glide-on' => 'Bikini and Underarm Glide-On',
				'bikini-brush-on' => 'Bikini Brush-On',
				'power-cream' => 'Shower Power Cream'
			),
			'nair-collection' => array(
				'sensitive' => 'Nair Sensitive Cream',
				'tough-hair' => 'Nair Tough Hair Cream',
				'triple-action-cream' => 'Nair Triple Action Cream',
				'triple-action-balm' => 'Nair Triple Action Balm'
			)
		);
	}

	// display product based on route name (/app/routes/web.php) and product name
	// /[route name]/[product name]
	public function index(ProductReviews $review, string $section)
	{
		
		$this->context['meta']['title']	= $this->productPageTitle[$this->range][$section];
		$this->context['pageViewJS']	= 'Main.Product.partials.product_js';
		$this->context['pageViewCSS']	= 'main/collections/layouts_collections';
		$this->context['class_section'] = $section;
		$this->context['reviews'] = $review->getActiveReviewsForProduct($section);
		
		return view('Main.Product.'.$this->range.'.'.$section, $this->context);
	}

	public function addReview(ProductReviewRequest $request, ProductReviews $review)
	{
		$stars = '';
		$stars = str_pad($stars ,  $request->rating, '*');
		$submission = array(
			'r_name' => $request->name,
			'r_comment' => $request->comment,
			'r_rating' => $request->rating,
			'r_product' => $request->product,
			'r_when' => time(),
			'r_active' => 0 // set to inactive by default
		);
		$review->fill( $submission );
		$review->save();

		$response = array(
			'status' => 'ok',
			'review_id' => $review->r_id
		);

		// send review approval email to admin address
		$submission['r_id'] = $review->r_id;
		$submission['stars'] = $stars;

		Mail::send(['text' => 'Main.Emails.approval'], $submission, function($message) use ( $submission )
		// Mail::send(['html' => 'Main.Emails.Contact.user'], $details, function($message) use($details)
		{
		    $message->from(env('FROM_ENQUIRY'), env('EMAIL_FROM_NAME'));
		    $message->to( env('EMAIL_ADMIN_REVIEW') );
			// $message->cc(env('EMAIL_ADMIN_CC'));
		    $message->subject('Nair new review approval');
		});

		return Response::json( $response , 200);
	}

	public function listUnapprovedReviews(ProductReviews $review)
	{

		/*
		// SELECT r_name,r_comments,r_rating,r_product FROM productreviews order by r_product ASC
		// SELECT r_product FROM productreviews group by r_product order by r_product ASC
		// SELECT * FROM productreviews WHERE r_product IN (SELECT r_product FROM productreviews group by r_product order by r_product ASC)
		// select count(r_id) as n, r_product from `productreviews` where r_active = 1 group by `r_product` order by `r_product` asc

		$this->context['products'] = $review->getDistinctProducts();
		$groupedProductReviews = [];
		foreach ( $this->context['products'] as $product)
		{
			$thisProduct = $product->r_product;
			$reviews = $review->getAllReviewsForProduct($thisProduct);

			array_push($groupedProductReviews, [
				'product' => $thisProduct,
				'reviews' => $reviews
			]);
		}

		$this->context['allReviews'] = $groupedProductReviews;
		$this->context['review'] = $review->getReview($id);

		<ul>
		@foreach ( $allReviews as $product)
		<li><h2>{{ $product['product'] }}</h2>
			<ul>
			@foreach ( $product['reviews'] as $review)
			<li @if($review['r_active'] === 1) style="background-color:#00ff00;" @endif>{{ $review['r_name'] }} {{ $review['r_rating'] }} {{ $review['r_active'] }}</li>
			@endforeach
			</ul>
		</li>
		@endforeach
		</ul>
		*/

		$this->context['pageViewCSS']	= 'main/layouts_legal';
		$this->context['reviewList'] = $review->getUnapprovedReviews();

		return view('Main.Product.unapproved-review-list', $this->context);
	}

	// admin approval email links to this view, display review information and 'Approve' button which links to 'approveReview' method below
	public function approveReviewForm(ProductReviews $review, int $id)
	{
		$this->context['review'] = $review->getReview($id);
		$this->context['pageViewCSS']	= 'main/layouts_legal';
		return view('Main.Product.view-review', $this->context);
	}

	public function approveReview(ProductReviews $review, int $id)
	{
		$subject = $review->getReview($id);
		$subject->approveReview();
		$this->context['review'] = $subject;

		$this->context['pageViewCSS']	= 'main/layouts_legal';
		return view('Main.Product.approval', $this->context);
	}
}
