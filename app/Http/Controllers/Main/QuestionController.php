<?php namespace App\Http\Controllers\Main;

use App\Http\Controllers\Main\MainController;

class QuestionController extends MainController {

	public function __construct()
	{
		parent::__construct();

		$this->context['meta']['title']	= 'Frequently Asked Questions';
		$this->context['meta']['desc'] = 'Answers to those common questions you may have about Nair products. What products should I use if I have sensitive skin? How long should I leave cream on for?';
		$this->context['pageViewJS']	= 'Main.Question.partials.question_js';
		$this->context['pageViewCSS']	= 'main/layouts_questions';

		$this->context['sections'] = array(
			array(
				'name' => 'nair',
				'link' => '/faqs',
				'label' => 'Nair Range'
			),
			array(
				'name' => 'creams',
				'link' => '/faqs/creams',
				'label' => 'Creams'
			),
			array(
				'name' => 'waxes',
				'link' => '/faqs/waxes',
				'label' => 'Waxes'
			)
		);
	}

	public function index($section = 'nair')
	{
		$this->context['section'] = $section;
		return view('Main.Question.'.$section, $this->context);
	}

}
