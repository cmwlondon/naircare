<?php namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;
use Validator;
use Request;

class ContactRequest extends FormRequest {

	public function __construct() {

	}

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		
		$rules = [
			'title'				=> 'required|max:255',
			'firstname'			=> 'required|max:255',
			'surname'      		=> 'required|max:255',
			'company'			=> 'max:255',
			'phone'				=> 'required|min:5|max:20',
			'email'				=> 'required|email',
			'product'			=> 'max:255',
			'comments'			=> 'required|max:255'
		];

		return $rules;
	}

	public function messages()
	{
	    return [
		    'title.required' => 'The Title field is required.',
		    'firstname.required' => 'The First Name field is required',
		    'surname.required' => 'The Last Name field is required',
		    'company.required' => 'The Company field is required',
		    'email.required' => 'The Email Address field is required',
		    'phone.required' => 'The Phone field is required',
		    'comments.required' => 'The Comment or Question field is required',
		    'phone.required' => 'The Phone field is required',
		    'phone.min' => 'The Phone number must be at least 5 digits long',
		    'phone.max' => 'The Phone number must be less than 21 digits long'
	    ];
	}

}
