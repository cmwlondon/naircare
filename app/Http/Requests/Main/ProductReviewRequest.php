<?php namespace App\Http\Requests\Main;

use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Http\FormRequest;
use Response;
// use Validator;
// use Request;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class ProductReviewRequest extends FormRequest {

	public function __construct() {

	}

	public function authorize() {
		
		return true;
	}

	public function rules() {
		
		$rules = [
	        'name' => 'required|max:255',
	        'comment' => 'required|max:255'
	        // 'rating' => 'required'
		];

		return $rules;
	}

	public function messages()
	{
	    return [
		    // 'title.required' => 'The Title field is required.',
	    ];
	}

	// convert default validation fail 422 error response to 200 to prevent jquery ajax breaking
	// https://laracasts.com/discuss/channels/laravel/how-make-a-custom-formrequest-error-response-in-laravel-55?page=1
	protected function failedValidation(Validator $validator)
	{
		// echo "<pre>".print_r($validator->errors(),true)."</pre>";
		// die();
		$resp = array('status' => 'fail', 'errors' => $validator->errors() );

	    throw new HttpResponseException( response()->json( array('status' => 'fail', 'errors' => $validator->errors()) , 200) );
	}

}
