<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Carousel extends Model
{
    /*
    id
    hub
    normnal
    blur
    */

    /*
    protected $table = 'faqs';
    public $timestamps = false;
    protected $fillable = [
        'hub','normal','blur'
    ];
    */

    /*
$config['page_links'] = array(

        'argan-body-wax-strips'     => array(
            'uri'                       => site_url('argan-oil/body-wax-strips')
        ),
        'argan-facial-wax-strips'   => array(
            'uri'                       => site_url('argan-oil/facial-wax-strips')
        ),
        // 'argan-face-and-eyebrow' => array(
        //  'uri'                       => site_url('argan-oil/face-and-eyebrow-roll-on-wax')
        // ),
        'upper-lip-kit'     => array(
            'uri'                       => site_url('argan-oil/upper-lip-kit')
        ),
        'facial-brush-on-cream'     => array(
            'uri'                       => site_url('argan-oil/facial-brush-on')
        ),
        'bikini-and-underarm-glide-on'          => array(
            'uri'                       => site_url('argan-oil/glide-on')
        ),
        'bikini-brush-on-cream'     => array(
            'uri'                       => site_url('argan-oil/bikini-brush-on')
        ),
        // 'argan-roll-on-wax'          => array(
        //  'uri'                       => site_url('argan-oil/roll-on-wax')
        // ),
        'salon-divine-body-wax'         => array(
            'uri'                       => site_url('argan-oil/body-wax')
        ),
        'shower-power-cream'            => array(
            'uri'                       => site_url('argan-oil/power-cream')
        ),

        'tough-hair'                => array(
            'uri'                      => site_url('nair-collection/tough-hair')
        ),
        'sensitive-cream'           => array(
            'uri'                      => site_url('nair-collection/sensitive')
        ),
        'triple-action-balm'            => array(
            'uri'                      => site_url('nair-collection/triple-action-balm')
        ),
        'triple-action-cream'           => array(
            'uri'                      => site_url('nair-collection/triple-action-cream')
        ),
        'jcb-body-wax-strips'       => array(
            'uri'                      => site_url('japanese-cherry-blossom/body-wax-strips')
        ),
        'jcb-facial-wax-strips'     => array(
            'uri'                      => site_url('japanese-cherry-blossom/facial-wax-strips')
        ),

);
    $config['carousels'] = array(
        'wax' => array('salon-divine-body-wax','jcb-body-wax-strips','jcb-facial-wax-strips','triple-action-balm'),
        'cream' => array('facial-brush-on-cream','bikini-and-underarm-glide-on','bikini-brush-on-cream','shower-power-cream','upper-lip-kit','triple-action-balm'),
        'face' => array('facial-brush-on-cream','upper-lip-kit','jcb-facial-wax-strips'),
        'legs' => array('salon-divine-body-wax','shower-power-cream','jcb-body-wax-strips','triple-action-cream','triple-action-balm'),
        'everywhere' => array('salon-divine-body-wax','bikini-and-underarm-glide-on','shower-power-cream','triple-action-balm'),
        'underarm' => array('bikini-and-underarm-glide-on','salon-divine-body-wax','shower-power-cream','jcb-body-wax-strips','triple-action-cream','triple-action-balm'),
        'bikini' => array('bikini-and-underarm-glide-on','salon-divine-body-wax','bikini-brush-on-cream','shower-power-cream','jcb-body-wax-strips','triple-action-cream','triple-action-balm'),
        'sensitive-skin' => array('upper-lip-kit','bikini-and-underarm-glide-on','facial-brush-on-cream','salon-divine-body-wax'),
        'pamper' => array('upper-lip-kit','bikini-and-underarm-glide-on','facial-brush-on-cream','salon-divine-body-wax')
    );

    */

    public function getCarousel(string $hub)
    {
    }

}
