<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consultation extends Model
{
	/*
	consultation

	routes
	id
	answers

	product
	id
	ref
	title
	image
	url

	route_products
	id
	route_id -> route.id
	product_id -> product.id
	*/
    protected $table = 'consultation';
    protected $primaryKey = "ID";
    public $timestamps = false;

    public function getProducts(string $answers)
    {
    	return $this->where('REF','=',$answers)->get();
    }

}
