<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = 'contacts';
    /*
    id
    title
    firstname
    surname
    email
    phone
    company
    product
    comments
    created_at
    updated_at
    */

    protected $fillable = array(
		'title',
		'firstname',
		'surname',
		'company',
		'phone',
		'email',
		'product',
		'comments'
    );
    
}
