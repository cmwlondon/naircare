<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hub extends Model
{
    /*
    id
    content
    image
    carousel
    */

    /*
    protected $table = 'faqs';
    public $timestamps = false;
    protected $fillable = [
        'hub','normal','blur'
    ];
    */

    public function getHub(string $hub)
    {
        return $this->where('name'.'=','$hub')->get();
    }

}
