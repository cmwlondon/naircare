<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductReviews extends Model
{
    /*
    reviews: model,request -> nair-2k18.productreviews
        r_id
        r_product = string $section
        r_name
        r_comment
        r_rating
        r_when
        r_active
    */

    protected $table = 'productreviews';
    protected $primaryKey = 'r_id';
    public $timestamps = false;

    protected $fillable = array(
        'r_product',
        'r_name',
        'r_comment',
        'r_rating',
        'r_when',
        'r_active'
    );

    public function getDistinctProducts()
    {
        return $this->groupBy('r_product')->orderBy('r_product', 'asc')->get(['r_product']);
    }

    public function getAllReviewsForProduct(string $product)
    {
        return $this->where('r_product','=',$product)->get();        
    }

    public function getActiveReviewsForProduct(string $product)
    {
        return $this->where('r_product','=',$product)->where('r_active','=',1)->get();
    }

    public function getUnapprovedReviews()
    {
        return $this->where('r_active','=',0)->get();
    }

    public function getReview(int $id)
    {
        return $this->findOrFail($id);
    }

    public function approveReview()
    {
        // $review = $this->findOrFail($id);   
        $this->r_active = 1;
        $this->save();

        return $this->get();
    }
}
