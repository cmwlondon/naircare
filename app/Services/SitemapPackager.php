<?php namespace App\Services;

use Response;
use Carbon\Carbon;
use Storage;
use Illuminate\Routing\RouteCollection;
use LSS\Array2XML;

class SitemapPackager {

	public function __construct(RouteCollection $routeCollection)
	{
		Carbon::setLocale('en');

		$this->routeCollection = $routeCollection;

		$this->sitemap = [
			'@attributes' => [
				'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
				'xsi:schemaLocation' => 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd',
				'xmlns' => 'http://www.sitemaps.org/schemas/sitemap/0.9'
			],

			'url' => []
		];


	}

	public function create() {
		$now = new Carbon();
		$now = $now->toW3cString();

		$this->addUrl(url('/'),$now,'daily','1.0');
		$this->addUrl(url('/hub/wax'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/cream'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/face'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/underarm'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/bikini'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/legs'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/everywhere'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/pamper'),$now,'monthly','0.9');
		$this->addUrl(url('/hub/sensitive-skin'),$now,'monthly','0.9');

		$this->addUrl(url('/japanese-cherry-blossom/body-wax-strips'),$now,'monthly','0.9');
		$this->addUrl(url('/japanese-cherry-blossom/facial-wax-strips'),$now,'monthly','0.9');
		$this->addUrl(url('/argan-oil/body-wax'),$now,'monthly','0.9');
		$this->addUrl(url('/argan-oil/upper-lip-kit'),$now,'monthly','0.9');
		$this->addUrl(url('/argan-oil/facial-brush-on'),$now,'monthly','0.9');
		$this->addUrl(url('/argan-oil/glide-on'),$now,'monthly','0.9');
		$this->addUrl(url('/argan-oil/bikini-brush-on'),$now,'monthly','0.9');
		$this->addUrl(url('/argan-oil/power-cream'),$now,'monthly','0.9');
		$this->addUrl(url('/nair-collection/sensitive'),$now,'monthly','0.9');
		$this->addUrl(url('/nair-collection/tough-hair'),$now,'monthly','0.9');
		$this->addUrl(url('/nair-collection/triple-action-cream'),$now,'monthly','0.9');
		$this->addUrl(url('/nair-collection/triple-action-balm'),$now,'monthly','0.9');

		$this->addUrl(url('/effective-hair-removal'),$now,'monthly','0.9');
		$this->addUrl(url('/effective-hair-removal/shaving'),$now,'monthly','0.9');
		$this->addUrl(url('/effective-hair-removal/wax'),$now,'monthly','0.9');
		$this->addUrl(url('/effective-hair-removal/creams'),$now,'monthly','0.9');

		$this->addUrl(url('/faqs'),$now,'monthly','0.9');
		$this->addUrl(url('/faqs/creams'),$now,'monthly','0.9');
		$this->addUrl(url('/faqs/waxes'),$now,'monthly','0.9');

		$this->addUrl(url('/online-consultation'),$now,'monthly','0.9');
		$this->addUrl(url('/contact-us'),$now,'monthly','0.9');
		$this->addUrl(url('/contact-us/images'),$now,'monthly','0.9');
		$this->addUrl(url('/contact-us/press-releases'),$now,'monthly','0.9');
		$this->addUrl(url('/terms-and-conditions'),$now,'monthly','0.9');
		$this->addUrl(url('/cookie-notice'),$now,'monthly','0.9');
		$this->addUrl(url('/privacy-policy'),$now,'monthly','0.9');

		// Convert and save
		$xml = Array2XML::createXML('urlset', $this->sitemap);
		// above line generates error:
		// DOMDocument::createTextNode() expects parameter 1 to be string, object given

		$response = $xml->saveXML();
		//dd(Storage::disk('public'));
		Storage::disk('public')->put('sitemap.xml', $response);

		$response = Storage::disk('public')->get('sitemap.xml'); // /storage/app/public/sitemap.xml
		
		return Response::make($response, '200')->header('Content-Type', 'text/xml');
	}

	private function addUrl($url,$lastmod,$freq,$priority) {
		$this->sitemap['url'][] = ['loc' => $url,
							'lastmod' => $lastmod,
							'changefreq' => $freq,
							'priority' => $priority
							];
	}
}
