<?php
/*
configuration / content data for onlone consultation (/online-consultation)
app/Http/Controllers/Main/ConsultationController.php
*/
return [
    'productMatrix' => [
    	[
    		'field' => 'JCB Facial Wax Strips',
    		'title' => 'Japanese Cherry Blossom Facial Wax Strips',
    		'image' => '/images/main/products/thumbs/2017-products/nair-jcb-face-3d-pack-suggested.jpg',
    		'url' => '/japanese-cherry-blossom/facial-wax-strips'
    	],
    	[
    		'field' => 'JCB Body Wax Strips',
    		'title' => 'Japanese Cherry Blossom Body Wax Strips',
    		'image' => '/images/main/products/thumbs/2017-products/nair-jcb-body-3d-pack-suggested.jpg',
    		'url' => '/japanese-cherry-blossom/body-wax-strips'
    	],
    	[
    		'field' => 'AO Bye Bye Pain', // Argan Oil Salon Divine Body Wax
    		'title' => 'Argan Oil Salon Divine Body Wax',
    		'image' => '/images/main/products/thumbs/argan-body-wax.jpg',
    		'url' => '/argan-oil/body-wax'
    	],
    	[
    		'field' => 'AO Shower Power Cream',
    		'title' => 'Argan Oil Shower Power Cream',
    		'image' => '/images/main/products/thumbs/2017-products/shower-power-cream-suggested.jpg',
    		'url' => '/argan-oil/power-cream'
    	],
    	[
    		'field' => 'AO Facial Brush On',
    		'title' => 'Argan Oil Facial Brush On',
    		'image' => '/images/main/products/thumbs/2017-products/facial-brush-on-cream-suggested.jpg',
    		'url' => '/argan-oil/facial-brush-on'
    	],
    	[
    		'field' => 'AO Bikini and Underarm Glide On',
    		'title' => 'Argan Oil Bikini and Underarm Glide On',
    		'image' => '/images/main/products/thumbs/2017-products/bikini-and-underarm-glide-on-suggested.jpg',
    		'url' => '/argan-oil/glide-on'
    	],
    	/*[
    		'field' => 'AO Ultra Precision Roll On Wax',
    		'title' => '',
    		'image' => '',
    		'url' => ''
    	],*/
    	[
    		'field' => 'AO Upper Lip Kit',
    		'title' => 'Argan Oil Upper Lip Kit',
    		'image' => '/images/main/products/thumbs/2017-products/upper-lip-kit-suggested.jpg',
    		'url' => '/argan-oil/upper-lip-kit'
    	],
    	/*[
    		'field' => 'AO Washable Roll on Wax',
    		'title' => '',
    		'image' => '',
    		'url' => ''
    	],*/
    	[
    		'field' => 'AO Bikni Brush On', // typo in db table field
    		'title' => 'Argan Oil Bikini Brush On',
    		'image' => '/images/main/products/thumbs/2017-products/bikini-brush-on-cream-suggested.jpg',
    		'url' => '/argan-oil/bikini-brush-on'
    	],
    	[
    		'field' => 'Nair Collection Sensitive Cream',
    		'title' => 'Nair Collection Sensitive Cream',
    		'image' => '/images/main/products/thumbs/nair-sensitive.jpg',
    		'url' => '/nair-collection/sensitive'
    	],
    	[
    		'field' => 'Nair Collection Tough Hair Cream',
    		'title' => 'Nair Collection Tough Hair Cream',
    		'image' => '/images/main/products/thumbs/nair-tough-hair.jpg',
    		'url' => '/nair-collection/tough-hair'
    	]
    ],
	'answerMatrix' => [
    	[
    		'question' => 'What&rsquo;s your natural hair colour?',
    		'answers' => [
    			[
    				'response' => 'Dark',
    				'text' => 'You have dark hair so may feel that your re-growth is very obvious but don&rsquo;t worry there are ways to minimise this.'
    			],
    			[
    				'response' => 'Brown',
    				'text' => 'You have naturally brown hair so minimising the re-growth may be a priority for you.'
    			],
    			[
    				'response' => 'Blonde',
    				'text' => 'Blonde hair is often thinner than dark but this doesn&rsquo;t mean you aren&rsquo;t just as keen to remain hair free!'
    			],
    			[
    				'response' => 'Red',
    				'text' => 'Red hair can actually mean thicker hair.  Don&rsquo;t worry though, there are some amazing (and sensitive) products around to keep you hair free.'
    			]
    		]
    	],
    	[
    		'question' => 'Would you describe your skin as sensitive?',
    		'answers' => [
    			[
    				'response' => 'Yes',
    				'text' => 'With sensitive skin such as yours make sure you opt for hair removal products that are sensitive.'
    			],
    			[
    				'response' => 'No',
    				'text' => ''
    			]
    		]
    	],
    	[
    		'question' => 'Which most describes the hairs on your legs?',
    		'answers' => [
    			[
    				'response' => 'Thick &amp; grows too quickly',
    				'text' => 'As you have thick hair you may find hair removal a chore. Don&rsquo;t worry; Nair has created products especially for coarse hair. Waxing even occasionally will help to thin out your hair but you must avoid shaving as it will only make the hair feel blunter.'
    			],
    			[
    				'response' => 'Average, but re-growth is a problem',
    				'text' => 'Fast re-growth can sometimes make you feel that you are always trying to tackle your hair, so it&rsquo;s important to avoid shaving as this will make your hair feel stubbly when it grows back, than waxing or using hair removal creams.'
    			],
    			[
    				'response' => 'It&rsquo;s fair and I&rsquo;d like to keep it that way',
    				'text' => 'You&rsquo;re one of the lucky ones with fair hair, so to keep it that way you need to avoid shaving.  It will only make re-growth feel blunter and stubblier, than after waxing or using hair removal creams.'
    			]
    		]
    	],
    	[
    		'question' => 'Which area do you struggle with most?',
    		'answers' => [
    			[
    				'response' => 'Bikini',
    				'text' => 'You struggle with the bikini line which can be a tough place to remove hair since skin can be delicate while the hair can be quite coarse.  Use a cream to gently dissolve the hair below the skins surface.  Re-growth will appear finer than after shaving.  For longer-lasting results use a wax. If you&rsquo;re not an experienced waxer then practise on your leg first and make sure that you have mastered the technique before you move on to your bikini area.'
    			],
    			[
    				'response' => 'Back of legs',
    				'text' => 'You struggle with the back of your legs which is a really awkward place to remove hair.  Nair creams are great for hard to reach areas - just smooth on, wait a few minutes and rinse off.'
    			],
    			[
    				'response' => 'Knee',
    				'text' => 'You struggle with the knee&rsquo;s which are one of the easiest areas to accidentally nick yourself with a razor. Instead try using a cream which are great as they coat every part of your knee so you won&rsquo;t be left with any stragglers.'
    			],
    			[
    				'response' => 'Face',
    				'text' => 'You struggle with the face, which you must treat with extra care. To remove hairs from your face, only use products that are developed for this area such as Nair facial brush-on.'
    			],
    			[
    				'response' => 'Underarms',
    				'text' => 'You struggle with underarm hair which is unsurprising as hair here grows in all sorts of directions, so waxing yourself can be very tricky!  Nair glide-on cream is designed to make the job easier and works in minutes.'
    			]
    		]
    	],
    	[
    		'question' => 'Which of the following do you most suffer from?',
    		'answers' => [
    			[
    				'response' => 'In-growing hairs',
    				'text' => 'In-grown hairs are a really common problem but can easily be avoided.  The key is to exfoliate regularly as this takes away the dead skin cells that can be trapped on the surface of the skin.'
    			],
    			[
    				'response' => 'Patchy re-growth (e.g. knees, ankles, backs of legs)',
    				'text' => 'Each hair on your body grows at a different rate so this can mean that hair will appear to be growing back faster in some areas.  To combat this, make sure you give your hair a chance to grow to a suitable length before waxing, or use a depilatory cream.  Nair waxes can be used on hair as short as 2mm, so make sure that even your slowest growing hairs have reached this length before using to maximise the smoothness.'
    			],
    			[
    				'response' => 'Razor rash/bumps',
    				'text' => 'Nowadays, with so many great alternatives to shaving, there really isn&rsquo;t any need to rely on razors.  With creams and waxes there&rsquo;s no risk of nicks and cuts, and regrowth will appear less visible.  You&rsquo;ll also stay smoother for longer than shaving as the hair is removed from below the skins surface.'
    			],
    			[
    				'response' => 'Dry skin',
    				'text' => 'Dry skin on legs is very common as it&rsquo;s often a part of the body we overlook when moisturising (especially in winter!) try applying a cream type moisturiser straight after your shower to help lock-in moisture and drink plenty of water.'
    			]
    		]
    	],
    	[
    		'question' => 'How long do you expect results to last?',
    		'answers' => [
    			[
    				'response' => 'A day or less',
    				'text' => 'Luckily with Nair depilatory creams you can now remove hair quickly, but with results that last longer than shaving - make the most of them!'
    			],
    			[
    				'response' => '2-5 days',
    				'text' => 'Creams are a great way to get smoother skin for longer than shaving. Whether you have 3 minutes, 5 minutes or 10 minutes there is an effective cream for you, plus Nair have developed formulations that even smell good so no more over-whelming odours!'
    			],
    			[
    				'response' => '6 days plus',
    				'text' => 'You can&rsquo;t beat waxing for long-lasting smoothness. We&rsquo;ve developed a range suitable for all abilities - not just for the professionals!'
    			]
    		]
    	]
    ],
    'toptips' => [
		'Your pain threshold is at its lowest immediately before and during your period, so avoid waxing at this time and opt for hair removal creams instead.',
		'Always pull wax strips back on themselves (opposite of hair growth direction) in a brisk manner, never try to peel them off or remove them slowly as this can be uncomfortable and ineffective.  Hold the skin taut while you remove the strip, and make sure that as you remove it you keep it parallel to the skin - do not pull it up and away from the body.',
		'Perspiration or damp skin can affect the grip of the wax on the hairs so always wax on dry skin. If necessary, apply a very light un-perfumed talc first. Never wax straight after a bath or shower - allow at least an hour in-between.',
		'A build up of dead skin cells can help cause in-growing hairs by blocking the opening of the hair follicle, so ensure you exfoliate at least once per week, ideally one-two days before removing hair.',
		'Wearing tight clothes can cause added sensitivity after waxing and can also cause in-growing hairs, so after removing hair avoid wearing tight clothing for at least 24 hours.',
		'Always do a patch test before removing hair, even if you have used the product before.  Make sure that you test it on the area where you will be removing hair, as different areas of skin can behave differently.',
	]
];
?>