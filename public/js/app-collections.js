$(document).ready(function(){
	$("#bInstructions").click(function(event) {
		//event.preventDefault();
		togglePanel("#p_instructions");
		$("#overlay_master").removeClass("hidden");
		
		if($('.instructionText').length > 0) {
			$(".instructionText").each(function(index) {
				$(".instructionText").hide();
			});
		}
	});
	
	$("#bReview").click(function(event) {
		//event.preventDefault();
		togglePanel("#p_reviews");
		$("#overlay_master").removeClass("hidden");
	});
	
	$("#b_inner_instructions").click(function(event) {
		//event.preventDefault();
		togglePanel("#p_instructions");
	});
	
	$("#b_inner_ingredients").click(function(event) {
		//event.preventDefault();
		togglePanel("#p_ingredients");
	});
	
	$("#b_inner_reviews").click(function(event) {
		//event.preventDefault();
		togglePanel("#p_reviews");
	});
	
	$("#bClose").click(function(event) {
		event.preventDefault();
		$("#overlay_master").addClass("hidden");
		$(".copy").removeClass("panel-vis");
		$(".suggested-products").removeClass("panel-vis");
	});
	
	$('.stars-select a').each(function(index,element) {
		$(this).click(function(e) {
			e.preventDefault();
			setScore($(this).attr("data-id"));
		});
	});
	
	
	$(".submit").click(function() { 
		var p_name     = $(frmName).val();
		var p_comment  = $(frmComment).val();
		var p_stars    = $(stars).val();
		var token = $('input[name="_token"]').val();

		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/product/addreview",
			data: {
				"name" : p_name,
				"comment" : p_comment,
				"rating" : p_stars,
				"product" : p_product,
				"_token" : token
			},
			success : function (response) {
				if (response.status === 'ok') {
					window.top.location = '#reviews';
					$('#theForm').toggleClass('hidden');
					$('#theThanks').toggleClass('hidden');
				} else {

					var errorMessage = "The following errors occured: \n";
					Object.entries(response.errors).forEach(function(error) {
						errorMessage = errorMessage + error[1][0] + "\n";
					});

					alert( errorMessage );
				}
			}
		})

	});

	$("h3.instructionsExpand").click(function(e) {
		var container 	= $('#p_instructions'),
			stepNum		= $("h3#"+e.target.id),
			stepText	= $("div#i-"+e.target.id);
		
		$(".instructionText").each(function(index) {
			$(".instructionText").hide();
		});

		stepText.show();

		container.animate({
                    scrollTop: stepNum.offset().top - container.offset().top + container.scrollTop()
       				}, 2000);
	});
	
	$("#bVideo").on('click', function(){
		$('.nair-background-fade, .nair-video-wrap, .nair-video').css({'display':'block'});
		$('#jquery_jplayer_1').jPlayer("play");
	});
	
	$("#close-nair-video").on('click', function(){
		$('.nair-background-fade, .nair-video-wrap').css({'display':'none'});
		$('#jquery_jplayer_1').jPlayer("stop");
	});
	
	/*======================================================================
	 * Perform video resize for mobile portrait and landscape changes
	 *======================================================================
	 */
	var current_width = $(window).width();
	
	// if(current_width < 400){
	// 	jQuery('body').css({'width':'300px'});
	// }
	
	$(window).resize(function(){
		var current_width = $(window).width();
		if(current_width < 480){
			$('#jp_video_0').css({'width':'320px','height':'200px','margin-left':'0px','margin-top':'-20px'});
			$('#jquery_jplayer_1').css({'height':'180px'});
			$('.nair-video-container').css({'width':'320px','height':'185px'});
		}
		else if(current_width < 734){
			$('#jp_video_0').css({'width':'600px','height':'310px','margin-left':'-25px'});
			$('#jquery_jplayer_1').css({'height':'288px'});
			$('.nair-video-container').css({'width':'550px','height':'310px'});
		}
		else if(current_width < 960){
			$('.nair-video-wrap').css({'margin-top':'0'});			
		}
		else{
			$('#jp_video_0').css({'width':'734px','height':'410px','margin-left':'-2px'});
			$('#jquery_jplayer_1').css({'height':'388px'});
			$('.nair-video-container').css({'width':'730px','height':'410px'});
		}
	});
	
});

function togglePanel(panel) {
	$.each(["#p_instructions","#p_ingredients","#p_reviews"], function(index, value) { 
		if (panel == value) {
			$(value).removeClass("hidden");
		} else {
			$(value).addClass("hidden");
		}
	});
	$(".copy").addClass("panel-vis");
	$(".suggested-products").addClass("panel-vis");
}

function setScore(id) {
	$('input[name=stars]').val(id);
	for(var i=1; i<=5; i++) {
		$('.stars-select').removeClass('s'+i);
	}
	$('.stars-select').addClass('s'+id);
}