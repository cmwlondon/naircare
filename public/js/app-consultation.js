var questionNum = 1;
var answers = [];
var groups;

$(document).ready(function(){
	groups = $(".group");
	groups.each(function(i,el) {
		$(el).find("h2, .c-btn").each(function(i2, el2) {
			$(el2).css("margin-left","480px");
		});
	});
	setTimeout(changeQuestion,500)
	
	$(".c-btn a").each(function(index,el) {
		$(el).click(function(e) {
			e.preventDefault();
			var sec = $(this).attr("data-sec");
			var ans = $(this).attr("data-ans");
			answers[sec-1] = ans;
			
			if (questionNum == 6) {
				window.top.location = siteurl + '/online-consultation/results/' + answers.join("-");
			} else {
				questionNum++;
				changeQuestion();
			}
		});
	});
	
	$("a.back").click(function(e) {
		e.preventDefault();
		questionNum--;
		changeQuestion();
	});
	
});

function changeQuestion() {
	$(".progress span").each(function(index,el) {
		if ($(el).attr("data-id") == questionNum) {
			$(el).addClass('big');
		} else {
			$(el).removeClass('big');
		}
	});
	
	groups.each(function(i,el) {
		
		if (parseInt($(el).attr("data-grp")) == questionNum) {
			$(el).removeClass("hidden");
			TweenLite.to($(el).find("h2"), 2, {css:{marginLeft:0}, ease:Power2.easeOut, delay:0});
			$(el).find(".c-btn").each(function (i2,el2) {
					TweenLite.to($(el2), 1, {css:{marginLeft:15+(i2*30)}, ease:Power2.easeOut, delay:i2*0.25});
			});
		} else {
			$(el).addClass("hidden");
			$(el).find("h2, .c-btn").each(function(i2, el2) {
				TweenLite.killTweensOf($(el2));
				$(el2).css("margin-left","480px");
			});
		}
		
		if (questionNum == 1) {
			$("a.back").addClass("hidden");
		} else {
			$("a.back").removeClass("hidden");
		}
	});
}