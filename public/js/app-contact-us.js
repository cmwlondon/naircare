var pos = 0;
var size = 101;
var maxWidth = 0;
var count = 0;

$(document).ready(function(){
	if ($("#carousel")) {
		count = $("#carousel ul li").length;
		maxWidth = count*size;
		$("#carousel ul").css("width", (maxWidth*2)+"px");
		
		$('#carousel ul li').clone().appendTo('#carousel ul');
		
		
		$('.arrow').click(function(e) {
			e.preventDefault();
			pos += ($(this).attr("data-direction") == "left") ? -1 : 1;

			doTween();
		});
	}

	$("#main-info").mCustomScrollbar({
		scrollButtons:{
			enable:true
		}
	});
}); // End of document ready



function doTween() {
	var obj = $("#carousel ul");
	var tLeft = parseInt(obj.css("left"));
	if (pos < 0) {
		pos = (count-1);
		obj.css("left", (tLeft-maxWidth)+"px");
	}
	if (pos > count) {
		pos = 1;
		obj.css("left", (tLeft+maxWidth)+"px");
	}
	TweenLite.to(obj, 0.5, {css:{left:(-1 * pos * size)}, ease:Power2.easeOut, overwrite:"all"});
}