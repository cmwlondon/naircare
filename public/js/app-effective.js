var tl;
var delay = 1/12;
var frameWidth = 287;
var frameHeight = 300;
var ratio = frameHeight/frameWidth;
var anim;
var loaded = false;
var loadCount = 0;
var bReplay;

$(document).ready(function(){
	if (frameCount > 0) {
		var fv = "9";
		if (swfobject.hasFlashPlayerVersion(fv)) {
			var flashvars = {};
			var params = {};
			params.wmode = "opaque";
			var attributes = {};
			attributes.id = "flash-embed";
			var ret = swfobject.embedSWF("/flash/why-nair-"+section+".swf", "flash-anim", "100%", "100%", fv, false, flashvars, params, attributes);
		} else {
			// Do the HTML backup version of the animation
			if( window.devicePixelRatio >= 2 ) {
				/*alert( "Hi Res @ Pixel Ratio : " + window.devicePixelRatio + " - Size : " + screen.width * window.devicePixelRatio);*/
				
				$("#flash-anim").remove();
				anim = $("#anim");
				tl = new TimelineLite({onComplete:completed, delay:2}); // create a TimelineLite instance
				window.onresize = function(event) {
					setScreen();
				}
				setScreen();
				loadAssets();
				
			} else {
				/*alert( "Normal @ Pixel Ratio 1 - Size : " + screen.width + "+" + screen.height);*/
				
				bStatic = new Image();
				$(bStatic)
				.load(function () {
					$(bStatic).appendTo('#anim');
				})
				.error(function () {})
				.attr('src', '/images/main/effective/static-' + section + '.png')
				.attr('width', '100%')
				.attr('class', 'bstatic');
				
				
			}
		}
	}
	
	
}); // End of document ready

function setScreen() {
	frameWidth = anim.width();
	frameHeight = frameWidth*ratio;
	if (loaded) {
		tl.stop();
		tl.seek(0);
		tl.clear();
		$('.breplay').hide();
		$('.breplay').removeClass("pointer");
		anim.css("background-size",(frameWidth*frameCount) + "px " + frameHeight + "px");
		initAnim();
	}
};

function loadAssets() {
	var img1 = new Image();
	$(img1)
	.load(function () {
		trackLoaded(1);
	})
	.error(function () {})
	.attr('src', '/images/main/effective/' + section + '-animation.png');
	
	var img2 = new Image();
	$(img2)
	.load(function () {
		trackLoaded(2);
	})
	.error(function () {})
	.attr('src', '/images/main/effective/shim-' + section + '.png');
	
	bReplay = new Image();
	$(bReplay)
	.load(function () {
		trackLoaded(3);
	})
	.error(function () {})
	.attr('src', '/images/main/effective/button-replay.png')
	.attr('width', '100%')
	.attr('class', 'breplay');
}

function trackLoaded(i) {
	if (i == 1) {
		anim.css("background","url(/images/main/effective/" + section + "-animation.png)");
		anim.css("background-size",(frameWidth*frameCount) + "px " + frameHeight + "px");
		anim.css("background-repeat","no-repeat");
		anim.css("background-position","0 0");
		anim.addClass("masked");
	}
	loadCount++;
	if (loadCount == 2) {
		$("#anim img.shim").attr("src","/images/main/effective/shim-" + section + ".png");
		$(bReplay).hide();
		$(bReplay).appendTo('#anim');
		
		loaded = true;
		initAnim();
	}	
	
}

function initAnim() {
	for (f = 0; f<frameCount; f++) {
		tl.append(TweenLite.to(anim, 0.0, {css:{'backgroundPosition': ((frameWidth * f * -1)) + "px 0"}, delay:delay}));
	}
	tl.play();
}

function completed() {
	$('.breplay').fadeIn('fast', function() {
		$(this).addClass("pointer");
		$(this).click(function() {
			tl.restart();
			$(this).hide();
			$(this).removeClass("pointer");
		});
	});	
}
