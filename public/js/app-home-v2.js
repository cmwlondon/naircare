var mode = 1;

var peelFiles = [
				{"file":"peel-01.jpg"},
				{"file":"peel-02.jpg"},
				{"file":"peel-03.jpg"},
				{"file":"peel-04.jpg"},
				{"file":"peel-05.jpg"},
				{"file":"peel-06.jpg"},
				{"file":"peel-07.jpg"},
				{"file":"peel-08.jpg"},
				{"file":"peel-09.jpg"},
				{"file":"peel-10.jpg"},
				{"file":"peel-11.jpg"},
				{"file":"peel-12.jpg"},
				{"file":"peel-13.png"}
			];
var peelArr = [];


var files = [	{"file":"polaroid-1-foot.png",x:79,y:297,z:25,"id":"p25"},
				{"file":"polaroid-2-leg.png",x:122,y:288,z:24,"id":"p24"},
				{"file":"polaroid-3-leg.png",x:202,y:290,z:20,"id":"p20"},
				{"file":"polaroid-4-leg.png",x:257,y:309,z:21,"id":"p21"},
				{"file":"polaroid-5-leg.png",x:314,y:298,z:22,"id":"p22"},
				{"file":"polaroid-6-leg.png",x:339,y:266,z:23,"id":"p23"},
				{"file":"polaroid-7-leg.png",x:268,y:254,z:19,"id":"p19"},
				{"file":"polaroid-8-leg.png",x:339,y:237,z:18,"id":"p18"},
				{"file":"polaroid-9-leg.png",x:396,y:291,z:17,"id":"p17"},
				{"file":"polaroid-10-leg.png",x:415,y:205,z:15,"id":"p15"},
				{"file":"polaroid-11-leg.png",x:449,y:270,z:14,"id":"p14"},
				{"file":"polaroid-12-bikini.png",x:508,y:221,z:16,"id":"p16"},
				{"file":"polaroid-13-leg.png",x:480,y:182,z:12,"id":"p12"},
				{"file":"polaroid-14-bikini.png",x:527,y:269,z:8,"id":"p8"},
				{"file":"polaroid-15-bikini.png",x:548,y:175,z:13,"id":"p13"},
				{"file":"polaroid-16-left-arm.png",x:576,y:217,z:9,"id":"p9"},
				{"file":"polaroid-17-underarm.png",x:597,y:140,z:11,"id":"p11"},
				{"file":"polaroid-18-bikini.png",x:639,y:184,z:10,"id":"p10"},
				{"file":"polaroid-19-bikini.png",x:678,y:209,z:7,"id":"p7"},
				{"file":"polaroid-20-shoulder.png",x:651,y:130,z:6,"id":"p6"},
				{"file":"polaroid-21-right-arm.png",x:687,y:185,z:5,"id":"p5"},
				{"file":"polaroid-22-neck.png",x:714,y:130,z:3,"id":"p3"},
				{"file":"polaroid-23-face.png",x:733,y:82,z:4,"id":"p4"},
				{"file":"polaroid-24-head.png",x:714,y:50,z:2,"id":"p2"},
				{"file":"polaroid-25-head.png",x:770,y:42,z:1,"id":"p1"}
			];
							
var enalrges = [	{"file":"enlarge-face.png",x:729,y:77,z:50,"id":"e4","ref":"/hub/face"},
					{"file":"enlarge-underarm.png",x:584,y:129,z:51,"id":"e11","ref":"/hub/underarm"},
					{"file":"enlarge-underarm-2.png",x:752,y:177,z:52,"id":"e5","ref":"/hub/underarm"},
					{"file":"enlarge-everywhere.png",x:68,y:286,z:53,"id":"e25","ref":"/hub/everywhere"},
					{"file":"enlarge-everywhere-2.png",x:615,y:272,z:54,"id":"e9","ref":"/hub/pamper"},
					{"file":"enlarge-leg.png",x:331,y:259,z:55,"id":"e23","ref":"/hub/legs"},
					{"file":"enlarge-leg-2.png",x:402,y:195,z:56,"id":"e15","ref":"/hub/legs"},
					{"file":"enlarge-bikini.png",x:494,y:209,z:56,"id":"e16","ref":"/hub/bikini"},
				];
							
var imgArr = [];
var enlargeArr = [];
var reflect, sidepanel, packshot, start, justheat, smoothon, discover;
var loaded = false;

$(document).ready(function(){
	window.onresize = function(event) {
		setScreen();
	}
	setScreen();
	$('.bDiscover').bind('click',function() {
		$('#alt-peel').css("opacity", 0);
		$('.home-text').css("opacity", 1);
	})
}); // End of document ready

function setScreen() {
	var contentWidth = $(window).width();
	if (contentWidth >= 960 && loaded == false) {
		$("#voucher-header").css("opacity", 0);
		$(".home-text").css("opacity", 0);
		startHomeSystemPeel();
		//startHomeSystemPolaroid();
	}

	if (mode == 2 || contentWidth <= 768) {
		$('#side-nav').css('left',"-109px");
	} else {
		$('#side-nav').css('left',"-200px");
	}
};

function startHomeSystemPeel() {
	mode = 1;
	loaded = true;
	var canvas = $('#home-canvas-peel');
	canvas.css("display","block");

	start = new Image();	
	$(start)
	// once the image has loaded, execute this code
	.load(function () {trackLoadedPeel(null);})
	// if there was an error loading the image, react accordingly
	.error(function () {/* notify the user that the image could not be loaded*/})
	.css('left', 302)
	.css('top', 0)
	.css('z-index', 103)
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home-peel/start.jpg');
	canvas.append(start);

	justheat = new Image();	
	$(justheat)
	// once the image has loaded, execute this code
	.load(function () {trackLoadedPeel(null);})
	// if there was an error loading the image, react accordingly
	.error(function () {/* notify the user that the image could not be loaded*/})
	.css('left', 302)
	.css('top', 0)
	.css('z-index', 102)
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home-peel/just-heat.jpg');
	canvas.append(justheat);

	smoothon = new Image();	
	$(smoothon)
	// once the image has loaded, execute this code
	.load(function () {trackLoadedPeel(null);})
	// if there was an error loading the image, react accordingly
	.error(function () {/* notify the user that the image could not be loaded*/})
	.css('left', 302)
	.css('top', 0)
	.css('z-index', 101)
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home-peel/smooth-on.jpg');
	canvas.append(smoothon);

	$.each(peelFiles, function(index, value) { 
		//trace(index + ': ' + value.file); 
		var img = new Image();
		
		$(img)
		// once the image has loaded, execute this code
		.load(function () {
			trackLoadedPeel($(this));
		})
		// if there was an error loading the image, react accordingly
		.error(function () {/* notify the user that the image could not be loaded*/})
		
		// *finally*, set the src attribute of the new image to our image
		.css('left', 302)
		.css('top', 0)
		.css('z-index', 100-index)
		.attr('id', 'peel-'+(100-index))
		.attr('src', '/images/main/home-peel/'+value.file);
		
		canvas.append(img);
	});
	
	packshot = new Image();	
	$(packshot)
	// once the image has loaded, execute this code
	.load(function () {
		trackLoadedPeel(null);
	})
	// if there was an error loading the image, react accordingly
	.error(function () {/* notify the user that the image could not be loaded*/})
	.css('left', 302)
	.css('top', 0)
	.css('z-index', 0)
	.attr('class', 'packshot')
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home-peel/pack-shot.jpg');
	canvas.append(packshot);

	discover = new Image();	
	$(discover)
	// once the image has loaded, execute this code
	.load(function () {
		trackLoadedPeel(null);
	})
	// if there was an error loading the image, react accordingly
	.error(function () {/* notify the user that the image could not be loaded*/})
	.css('left', 52)
	.css('top', 29)
	.css('z-index', 105)
	.attr('class', 'discover')
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home-peel/discover-more.png');
	canvas.append(discover);

	$(discover).bind('click',function() {
		doDiscover();
	})


	sidepanel = new Image();	
	$(sidepanel)
	// once the image has loaded, execute this code
	.load(function () {
		trackLoadedPeel(null);
	})
	// if there was an error loading the image, react accordingly
	.error(function () {
		// notify the user that the image could not be loaded
	})
	.css('left', 0)
	.css('top', 0)
	.css('z-index', 1000)
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home-peel/side-panel.png');
	canvas.append(sidepanel);
}

function trackLoadedPeel(itm) {
	peelArr.push(itm);
	trace(peelArr.length + ':' + (peelFiles.length+6));
	if (peelArr.length == (peelFiles.length+6)) {
		startPeelAnim();
	}
}

function startPeelAnim() {
	trace("Start The Peel Animation");
	var fadeSpeed = 2;
	var baseDelay = 0;
	baseDelay += 1;

	TweenLite.to(start, fadeSpeed, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	baseDelay += 2.0;
	TweenLite.to(start, fadeSpeed, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += fadeSpeed;
	TweenLite.to(justheat, 0, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});



	TweenLite.to(start, fadeSpeed, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	baseDelay += 2.0;
	TweenLite.to(start, fadeSpeed, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += fadeSpeed;
	TweenLite.to(smoothon, 0, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});


	TweenLite.to(start, fadeSpeed, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	

	baseDelay += 1.0;
	$.each(peelFiles, function(index, value) { 
		var item = $('#peel-'+(100-index));
		baseDelay += 0.07;
		if (index == (peelFiles.length-1)) {
			TweenLite.to(item, 0.75, {css:{left:-40}, ease:Power2.easeOut, delay:baseDelay, onComplete:removePeels});	
		} else {
			TweenLite.to(item, 0, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
		}
	});

	baseDelay += 0.5;

	
	TweenLite.to($('#peel-panel-overlay'), 0.5, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	baseDelay += 0.25;
	TweenLite.to(discover, 0.5, {css:{left:302}, ease:Power2.easeOut, delay:baseDelay});

}

function doDiscover() {
	TweenLite.to(discover, 0.5, {css:{left:-250}, ease:Power2.easeOut, delay:0});
	TweenLite.to(sidepanel, 0.5, {css:{left:-370}, ease:Power2.easeOut, delay:0.5});
	TweenLite.to(packshot, 0.5, {css:{left:902}, ease:Power2.easeOut, delay:0.5, onComplete:startHomeSystemPolaroid});
}

function removePeels() {
	$.each(peelFiles, function(index, value) { 
		$('#peel-'+(100-index)).remove();
	});
}

function loadSideNav() {
	var sItem = $('#side-nav');
	if (sItem.css('left') == "-200px") {
		sItem.animate({'left': "-109px"}, {'duration': 250});
		$("#bSideNav").removeClass('out');
	}
}


function startHomeSystemPolaroid() {
	mode = 2;
	loaded = true;
	var canvas = $('#home-canvas');
	canvas.css("display","block");

	loadSideNav();

	$('#home-canvas-peel').remove();
	$('#alt-peel').remove();

	$.each(files, function(index, value) { 
		//trace(index + ': ' + value.file); 
		var img = new Image();
		
		$(img)
		// once the image has loaded, execute this code
		.load(function () {
			trackLoaded($(this));
		})
		// if there was an error loading the image, react accordingly
		.error(function () {
			// notify the user that the image could not be loaded
		})
		
		// *finally*, set the src attribute of the new image to our image
		.css('left', value.x + 960)
		.css('top', value.y)
		.css('z-index', value.z + 200)
		.attr('id', value.id)
		.attr('data-x', value.x)
		.attr('data-y', value.y)
		.attr('src', '/images/main/home/polaroids/'+value.file);
		
		canvas.append(img);
	});
	
	reflect = new Image();
		
	$(reflect)
	// once the image has loaded, execute this code
	.load(function () {
		trackLoaded(null);
	})
	// if there was an error loading the image, react accordingly
	.error(function () {
		// notify the user that the image could not be loaded
	})
	
	.attr('class', 'reflect')
	// *finally*, set the src attribute of the new image to our image
	.attr('src', '/images/main/home/reflection.png');
	canvas.append(reflect);

	$.each(enalrges, function(index, value) { 
		//trace(index + ': ' + value.file); 
		var img = new Image();
		
		$(img)
		// once the image has loaded, execute this code
		.load(function () {
			trackEnlarges($(this));
		})
		// if there was an error loading the image, react accordingly
		.error(function () {
			// notify the user that the image could not be loaded
		})
		
		// *finally*, set the src attribute of the new image to our image
		.css('left', value.x)
		.css('top', value.y)
		.css('z-index', value.z + 200)
		.attr('id', value.id)
		.attr('data-x', value.x)
		.attr('data-y', value.y)
		.attr('data-ref', value.ref)
		.attr('src', '/images/main/home/polaroids/'+value.file)
		.attr('class', 'hidden');
		
		canvas.append(img);
	});
}

function trackLoaded(itm) {
	imgArr.push(itm);
	if (imgArr.length == 26) {
		startAnim();
	}
}

function trackEnlarges(itm) {
	enlargeArr.push(itm);
	if (enlargeArr.length == 1) {
		$.each([4,11,5,25,9,23,15,16], function(index, value) { 
			$("#p"+value).addClass("pointer");
			$("#p"+value).bind('mouseenter', function() {
				$("#e"+value).removeClass("hidden");
			})
			$("#p"+value).bind('mouseleave', function() {
				$("#e"+value).addClass("hidden");
			})
			$("#e"+value).addClass("pointer");
			$("#e"+value).bind('mouseenter', function() {
				$("#e"+value).removeClass("hidden");
			})
			$("#e"+value).bind('mouseleave', function() {
				$("#e"+value).addClass("hidden");
			})
			$("#e"+value).bind('click', function() {
				window.top.location = $("#e"+value).attr("data-ref");
			})
		});
	}
}

function startAnim() {
	$.each(imgArr, function(index, value) { 
		if (value != null) {
			var speed = (Math.random()/2)+0.25;
			TweenLite.to(value, speed, {css:{left:value.attr("data-x")}, ease:Power2.easeOut, delay:(index*0.07)});
		}
		if (index == 25) {
			TweenLite.to(reflect, 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:(25*0.07)});	
			TweenLite.to($("#voucher-header"), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:((25*0.07)+0.5)});
			TweenLite.to($(".home-text"), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:((25*0.07)+0.5)});
		}
	});
}