var loaded = false;

$(document).ready(function(){
	window.onresize = function(event) {
		setScreen();
	}
	
	$('#home-canvas-peel').show();
	
	/*======================================================================
	 * Perform video resize for mobile portrait and landscape changes
	 *======================================================================
	 */
	var current_width = $(window).width();
	
	if(current_width < 400){
		jQuery('body').css({'width':'300px'});
	}
	
	$(window).resize(function(){
		var current_width = $(window).width();
		if(current_width < 480)
			$('#jp_video_0').css({'width':'300px'});
		else if(current_width > 480)
			$('#jp_video_0').css({'width':'439px'});
	});

	beginAnim();
}); // End of document ready

function setScreen() {
	var contentWidth = $(window).width();	
};

function beginAnim() {
	var baseDelay = 0;
	baseDelay += 1.5;

	TweenLite.to($('#side_panel .offer'), 0, {css:{opacity:1}});

	TweenLite.to($('#side_panel .slide'), 1, {css:{left:0}, ease:Power2.easeOut, delay:baseDelay});

	TweenLite.to($('.woman'), 1, {css:{top:0}, ease:Power2.easeOut, delay:baseDelay});
	baseDelay += 0.75;
	TweenLite.to($('.woman .layer2'), 0.5, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	TweenLite.to($('.tono'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	TweenLite.to($('.logo1'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	TweenLite.to($('.takes-care'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 1;
	TweenLite.to($('.skin-zoom'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});
	baseDelay += 1;
	TweenLite.to($('.skin-zoom'), 0.5, {css:{width:340,height:340,left:"-45px",top:"-64px"}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	
	// Perform a bit of a tidy up.
	TweenLite.to($('.slide-up'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.woman'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.tono'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.logo1'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.takes-care'), 0, {css:{opacity:0}, delay:baseDelay});


	TweenLite.to($('.stick'), 0, {css:{rotation:-30}, ease:Power2.easeOut, delay:baseDelay});
	TweenLite.to($('.stick'), 0.5, {css:{left:"0",top:"-19px",rotation:0}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	TweenLite.to($('.stick'), 0.5, {css:{left:"-3",top:"100px",rotation:-5}, ease:Power2.easeOut, delay:baseDelay});
	TweenLite.to($('.wax'), 0.5, {css:{top:"-98px"}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	TweenLite.to($('.stick'), 0.5, {css:{left:"0",top:"190px",rotation:5}, ease:Power2.easeOut, delay:baseDelay});
	TweenLite.to($('.wax'), 0.5, {css:{top:"0"}, ease:Power2.easeOut, delay:baseDelay});
	baseDelay += 0.25;
	TweenLite.to($('.instruction1'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:(baseDelay)});

	baseDelay += 0.25;
	TweenLite.to($('.stick'), 0.2, {css:{left:"0",top:"240px",rotation:0}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 1.25;
	TweenLite.to($('.instruction1'), 0.5, {css:{opacity:0}, ease:Power2.easeOut, delay:(baseDelay)});

	baseDelay += 0.5;
	TweenLite.to($('.instruction2'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:(baseDelay)});

	baseDelay += 0.1;
	TweenLite.to($('.timer'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:(baseDelay)});

	baseDelay += 0.5;
	for (var f = 1; f <= 18; f++) {
		TweenLite.to($('.timer'), 0, {css:{backgroundPosition:"-"+((f-1)*65)+"px"}, delay:(baseDelay+(f*0.16))});
	}

	baseDelay += 3.5;
	TweenLite.to($('.instruction3'), 0.5, {css:{opacity:1}, ease:Power2.easeOut, delay:(baseDelay)});

	baseDelay += 0.5;
	TweenLite.to($('.peel-start'), 0.5, {css:{bottom:0}, ease:Power2.easeOut, delay:(baseDelay)});

	baseDelay += 1;
	TweenLite.to($('.peel-start'), 0, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	TweenLite.to($('.peel-frames'), 0, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	for (var f = 1; f <= 8; f++) {
		TweenLite.to($('.peel-frames'), 0, {css:{backgroundPosition:"-"+((f-1)*251)+"px"}, delay:(baseDelay+(f*0.06))});
	}

	// Perform a bit of a tidy up.
	TweenLite.to($('.skin-zoom'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.stick'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.wax'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.instruction2'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.timer'), 0, {css:{opacity:0}, delay:baseDelay});
	TweenLite.to($('.instruction3'), 0, {css:{opacity:0}, delay:baseDelay});

	baseDelay += ((8*0.06)+1);
	TweenLite.to($('.no-mess'), 0.2, {css:{opacity:1}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 2.5;
	TweenLite.to($('.peel-frames'), 0.5, {css:{opacity:0}, ease:Power2.easeOut, delay:baseDelay});
	TweenLite.to($('.no-mess'), 0.5, {css:{opacity:0}, ease:Power2.easeOut, delay:(baseDelay-0.25)});

	TweenLite.to($('.slide-up-end'), 0.5, {css:{bottom:0}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	TweenLite.to($('.pack'), 0.75, {css:{opacity:1, width:251, height:209, left:0, top:10}, ease:Power2.easeOut, delay:baseDelay});

	TweenLite.to($('#side_panel .block-text'), 0.75, {css:{opacity:0}, ease:Power2.easeOut});

	baseDelay += 0.5;
	TweenLite.to($('.discover'), 0.5, {css:{left:0}, ease:Power2.easeOut, delay:baseDelay});

	baseDelay += 0.5;
	TweenLite.to($('#side_panel .offer'), 0.5, {css:{opacity:1}, ease:Power2.easeOut});
}



