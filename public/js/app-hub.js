var itemCount = 0;

$(document).ready(function(){
	var items = $('#carousel-master').find('li');
	itemCount = items.length;
	var division = Math.round(360/itemCount);
	var radius = 130;
	var center = {"x":163, "y":100};
	var tileSizeWidth = 230;
	var tileSizeHeight = 230;
	var tilt = 0.2;
	
	var maxTop = Math.cos(convert("degs",0*division))* radius * tilt
	
	//for (var a = 0; a < 360; a++) {
		//trace(Math.sin(convert("degs",a))* radius);
	//}

	$.each(items, function(currentID, value) { 		
		var me = $(this);
		var meImg = me.find('img.norm');
		var meBlur = me.find('img.blur');
		
		me.attr("data-index",currentID);
		
		trace(currentID*division);
		
		var tx = Math.sin(convert("degs",currentID*division))* radius;
		var ty = (Math.cos(convert("degs",currentID*division))* radius * tilt);
		var tz = Math.round(ty + 100);
		var ts = (((ty / 2) + maxTop) / (maxTop * 2)) + 0.25;
		var newSizeW = tileSizeWidth*ts;
		var newSizeH = tileSizeHeight*ts;
		
		me.css('left',(tx + center.x) + "px");
		me.css('top',(ty + center.y) + "px");
		me.css('z-index',tz);
		
		meImg.css('width' ,newSizeW + "px");
		meImg.css('height', newSizeH + "px");
		meImg.css('left', (0-(newSizeW/2)) + "px");
		meImg.css('top', (0-(newSizeH/2)) + "px");
		
		meBlur.css('width' ,newSizeW + "px");
		meBlur.css('height', newSizeH + "px");
		meBlur.css('left', (0-(newSizeW/2)) + "px");
		meBlur.css('top', (0-(newSizeH/2)) + "px");

		meBlur.css('opacity', ((150-tz)/150*3)-0.4);
		
	});
	
	/*======================================================================
	 * carousel nav methods
	 *======================================================================
	 */
	(function($){
		var  _navOver	= false
			,_miniNav	= $('#mini-nav')
			,_bLeft		= '#bLeft'
			,_bRight	= '#bRight'
			,_fade		= 'hubCarFade'
			,_over		= 'hubCarOver';
			
		_miniNav.on('mouseenter', function(){
			var delayOver = setTimeout(function(){
				if(_navOver === false)
					$(_bLeft+', '+_bRight).addClass(_fade);
			}, 100);
		});
		
		_miniNav.on('mouseleave', function(){
			$(_bLeft+', '+_bRight).removeClass(_fade);
		});
		
		$(_bLeft+', '+_bRight).on('mouseover', function(){
			_navOver = true;
			$(_bLeft+', '+_bRight).removeClass(_fade);
			$(this).addClass(_over);
		});
		
		$(_bLeft+', '+_bRight).on('mouseout', function(){
			_navOver = false;
			$(_bLeft+', '+_bRight).addClass(_fade);
			$(this).removeClass(_over);
		});
	}(jQuery));
	
	$('#bLeft').click(function() {
		$.each(items, function(key, value) {
			var me = $(this);
			var currentID = Number(me.attr("data-index"));
			currentID = (currentID == itemCount-1) ? 0 : (currentID+1);
			me.attr("data-index",currentID);
			animItem(me,currentID);
		});
	});
	
	$('#bRight').click(function() {
		$.each(items, function(key, value) {
			var me = $(this);
			var currentID = Number(me.attr("data-index"));
			currentID = (currentID == 0) ? itemCount-1 : (currentID-1);
			me.attr("data-index",currentID);
			animItem(me,currentID);
		});
	});
	
	function animItem(me,currentID) {
		var meImg = me.find('img.norm');
		var meBlur = me.find('img.blur');
		
		var speed = 0.5;
		var tx = Math.sin(convert("degs",currentID*division))* radius;
		var ty = (Math.cos(convert("degs",currentID*division))* radius * tilt);
		var tz = Math.round(ty + 100);
		var ts = (((ty / 2) + maxTop) / (maxTop * 2)) + 0.25;
		var newSizeW = tileSizeWidth*ts;
		var newSizeH = tileSizeHeight*ts;

		TweenLite.to(me, speed, {css:{"left":(tx + center.x)}, ease:Power2.easeOut, onUpdate:fixZ, onUpdateParams:[me,meBlur]});
		TweenLite.to(me, speed, {css:{"top":(ty + center.y)}, ease:Power2.easeOut});

		TweenLite.to(meImg, speed, {css:{"width":newSizeW}, ease:Power2.easeOut});
		TweenLite.to(meImg, speed, {css:{"height":newSizeH}, ease:Power2.easeOut});
		TweenLite.to(meImg, speed, {css:{"left":(0-(newSizeW/2))}, ease:Power2.easeOut});
		TweenLite.to(meImg, speed, {css:{"top":(0-(newSizeH/2))}, ease:Power2.easeOut});
		
		TweenLite.to(meBlur, speed, {css:{"width":newSizeW}, ease:Power2.easeOut});
		TweenLite.to(meBlur, speed, {css:{"height":newSizeH}, ease:Power2.easeOut});
		TweenLite.to(meBlur, speed, {css:{"left":(0-(newSizeW/2))}, ease:Power2.easeOut});
		TweenLite.to(meBlur, speed, {css:{"top":(0-(newSizeH/2))}, ease:Power2.easeOut});
	
	}
	
	function fixZ(me,meBlur) {
		var newZ = Math.round(parseInt(me.css("top")) - center.y + 100);
		me.css('z-index',newZ);
		meBlur.css('opacity', ((150-newZ)/150*3)-0.4);
	}
	
});

function convert(type, num) {
	var result = 0;
	if (type == "rads") {
		result = num*180/Math.PI;
	}
	if (type == "degs") {
		result = num*Math.PI/180;
	}
	return result;
}