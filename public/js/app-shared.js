var snObjectCount = 0;
var listGroup;
var snbusy = false;

$(document).ready(function(){
	//doCookieNotify($('#cookie-notify'));

	$(document).cookieNotify($('#cookie-notify'));

	$('#side-nav').css('left',"-109px");

	//trace("Doing This!!")
	if ($('#cookie-notify').length) {
		var cookieHeight = $('#cookie-notify').height() + 20;
		$('.cookieFix').css('margin-top',cookieHeight+'px');
	}

	/* Make the side nav slide out */
	$("#bSideNav").click(function(event){
		event.preventDefault();
		var sItem = $('#side-nav');
		trace(sItem.css('left'));
		if (sItem.css('left') == "-109px") {
			sItem.animate({'left': "0px"}, {'duration': 250});
			$(this).addClass('out');
		} else {
			sItem.animate({'left': "-109px"}, {'duration': 250});
			$(this).removeClass('out');
		}
	});

	/* Store the initial number of items and then clone those to make a double loop */
	listGroup = $('.sn-list');
	snObjectCount = listGroup.children().length;
	var children = listGroup.children().clone();
  	listGroup.prepend(children);

	/* Make the side move up and down */
	$(".sn-arr-up").click(function(event){
		event.preventDefault();
		triggerSNMove("up");
	});

	$(".sn-arr-down").click(function(event){
		event.preventDefault();
		triggerSNMove("down");
	});


	/* Deal with the main menu via Javascript when we're on phones */
	$(".bExpand").click(function(event) {
		event.preventDefault();
		// Check Screen Size
		var contentWidth = $(window).width();
		if (contentWidth <= 480) {
			toggleSubMenu($(this).attr("data-id"));
		}
	});

	/*======================================================================
	 * Define play params for each page
	 *======================================================================
	 */
	var videoObj = {
		 'uri' : window.location.href
		,'home': {
			 'title'  : 'Discover Argon Oil'
			,'m4v'    : '/videos/nair-home-page-video.mp4'
			,'poster' : '/videos/stills/nair-home-page-video.png'
			,'width'  : '439px'
			,'height' : '179px'
		}
		,'notebook': {
			 'title'  : 'Discover Argon Oil'
			,'m4v'    : '/videos/nair-home-page-video.mp4'
			,'poster' : '/videos/stills/nair-home-page-video.png'
			,'width'  : '439px'
			,'height' : '179px'
		}
		,'grazia': {
			 'title'  : 'Discover Argon Oil'
			,'m4v'    : '/videos/nair-home-page-video.mp4'
			,'poster' : '/videos/stills/nair-home-page-video.png'
			,'width'  : '439px'
			,'height' : '179px'
		}
		,'argan-oil-roll-on-wax': {
			 'title'  : 'Discover Argon Oil Roll-On'
			,'m4v'    : '/videos/nair-washable-roll-on.mp4'
			,'poster' : '/videos/stills/nair-washable-roll-on.png'
			,'width'  : '734px'
			,'height' : '410px'
		}
		,'argan-oil-shower-power-cream': {
			 'title'  : 'Discover Argon Oil Shower Power Cream'
			,'m4v'    : '/videos/nair-shower-cream.mp4'
			,'poster' : '/videos/stills/nair-shower-cream.png'
			,'width'  : '734px'
			,'height' : '412px'
		}
		,'argan-oil-body-wax-strips': {
			 'title'  : 'Discover Argon Oil Wax Strips'
			,'m4v'    : '/videos/nair-wax-strips.mp4'
			,'poster' : '/videos/stills/nair-wax-strips.png'
			,'width'  : '734px'
			,'height' : '410px'
		}
		,'argan-oil-facial-wax-strips': {
			 'title'  : 'Discover Argon Oil Wax Strips'
			,'m4v'    : '/videos/nair-wax-strips.mp4'
			,'poster' : '/videos/stills/nair-wax-strips.png'
			,'width'  : '734px'
			,'height' : '410px'
		}
	}

	/*======================================================================
	 * get uri component for video selection
	 *======================================================================
	 */
	videoObj['uri'] = videoObj['uri'].split('/');
	if(typeof videoObj['uri'][3] === 'undefined' || videoObj['uri'][3] === ''){
		videoObj['uri'] = 'home';
	}
	else{
		videoObj['uri'] = videoObj['uri'].pop();
		videoObj['uri'] = videoObj['uri'].replace('#','');
	}

	/*======================================================================
	 * Initialise video player
	 *======================================================================
	 */
	if($("#bVideo").length !== 0)
		initJplayer(videoObj);
	else if($(".video-wrap").length !== 0)
		initJplayer(videoObj);

		/*======================================================================
		* Cookie controls
		*======================================================================
		*/

		var cookieInstance = new CookieClass();
		$(".cookie-disclaimer .btn-close").bind("click",() => cookieInstance.closeCookieDisclaimer());

		cookieInstance.setCookieName(cookieInstance.checkCookie());
		cookieInstance.updateDisclaimerStyles();
		window.clearCookie = () => {
			cookieInstance.clearCookie();
		}
});

/*======================================================================
* Cookie methods
*======================================================================
*/

function CookieClass() {
	this.cookieDisclaimer = false,
	this.cookieName = 'SterimarDisclaimerCookie2018'
}

CookieClass.prototype = {

	setCookieName: function(bool) {
		this.cookieDisclaimer = bool;
	},

	updateDisclaimerStyles: function() {
		if (!this.cookieDisclaimer) {
			$(".cookie-disclaimer").addClass("shown");
		} else {
			$(".cookie-disclaimer").removeClass("shown");
		}
	},

	closeCookieDisclaimer: function(e) {
		this.cookieDisclaimer = true;
		this.updateDisclaimerStyles();
		this.setCookie();
	},

	setCookie: function() {
		var expiresDays = 1/24;
		var d = new Date();
		d.setTime(d.getTime() + (expiresDays*24*60*60*1000));
		var expires = "expires=" + d.toGMTString();
		document.cookie = this.cookieName + "=policy;"+expires+";path=/";
	},

	getCookie: function() {
		var name = this.cookieName + "=";
		var decodedCookie = decodeURIComponent(document.cookie);
		var ca = decodedCookie.split(';');
		for(var i = 0; i < ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0) == ' ')
			c = c.substring(1);
			if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
		}
		return "";
	},

	checkCookie: function() {
		var user = this.getCookie();
		if (user != "")
			return true;
		else
			return false;
	},

	clearCookie: function() {
		var d = new Date();
		var expires = "expires=" + d.toGMTString();
		document.cookie = this.cookieName + "=policy;"+expires+";path=/";
		this.cookieDisclaimer = false;
		this.updateDisclaimerStyles();
	}
}


/*======================================================================
* Cookie methods end
*======================================================================
*/

function initJplayer(videoObj) {

	$("#jquery_jplayer_1").jPlayer({
		ready: function () {

			/*======================================================================
			 * Set video and image placeholder
			 *======================================================================
			 */
			$(this).jPlayer("setMedia", {
				 title  : videoObj[videoObj['uri']].title
				,m4v	: videoObj[videoObj['uri']].m4v
				,poster : videoObj[videoObj['uri']].poster
			});

			/*======================================================================
			 * Hack video player up into tiny pieces and reform anew :)
			 *======================================================================
			 */
			$('.jp-video-play, .jp-toggles, .jp-volume-bar, .jp-volume-max, .jp-current-time, .jp-duration, .jp-progress').css({'display':'none'});
			$('a.jp-mute, a.jp-unmute').css({'left':'10px'});
			$('a.jp-play, a.jp-stop').css({
				 'width':'20px'
				,'height':'28px'
				,'margin':'0px'
				,'margin-top':'10px'
			});
			$('a.jp-mute, a.jp-unmute').css({
				'margin-top':'4px'
			});
			$('.jp-controls').css({
				'margin-left': '25px'
			});

			/*======================================================================
			 * Animate controls
			 *======================================================================
			 */
			$('.jp-gui').addClass('slideUp');

			/*======================================================================
			 * Bind handlers
			 *======================================================================
			 */
			$(document)
			.on('mouseleave', '#jp_video_0, .jp-interface, #jp_poster_0', function(){
				$('.jp-gui').removeClass('slideUp');
			})
			.on('mouseenter', '#jp_video_0, .jp-interface, #jp_poster_0', function(){
				$('.jp-gui').addClass('slideUp');
			})
			.on('click', '.jp-stop', function(){
				$('#jp_video_0').css({'display':'none'});
				$('#jp_poster_0').css({'display':'block'});
			})
			.on('click', '.jp-play', function(){
				$('#jp_poster_0').css({'display':'none'});
				$('#jp_video_0').css({'display':'block'});
			})
			.on('click', '.jp-mute', function(event){
				event.preventDefault();
				$('.jp-volume-bar, .jp-volume-max').css({'display':'none'});
				console.log(this);
			});
		}
		,swfPath	: "/js"
		,supplied	: "m4v, ogv"
		,size: {
			 width	: videoObj[videoObj['uri']].width
			,height : videoObj[videoObj['uri']].height
		}
	});

}

function toggleSubMenu(num) {
	var expanders = [1,2];
	$.each([1,2], function(index, value) {
		var tParent = $(".main-menu .b"+value);
		var subMenu = tParent.find("menu");
		var itemCount = subMenu.find("li").length;
		var targetHeight = (parseInt(tParent.css("height")) > 27) ? 27 : (((itemCount+1)*27) + 27 + 5);
		if (value != num) {
			targetHeight = 27;
		}
		tParent.animate({
			height: targetHeight + "px"
		}, 1000, function() {});
	});
}

function triggerSNMove(direction) {
	var objectHeight = 84;
	var currentPos = listGroup.cssNumber('top');
	//trace(currentPos + ' vs ' + (0 - ((objectHeight*snObjectCount)-420)))
	// If we go too far down, loop it back to the top.
	if (currentPos >= 0 ) {
		currentPos -= (objectHeight*snObjectCount);
		listGroup.css('top',currentPos+'px');
	}

	// If we go too far up, loop it back to the bottom.
	else if (currentPos < (420 - (objectHeight*(snObjectCount-1)*2)) ) {
		currentPos += (objectHeight*snObjectCount);
		listGroup.css('top',currentPos+'px');
	}

	if (snbusy == false) {
		snbusy = true;
		if (direction == "up") {
			 listGroup.animate({
					top: (currentPos + objectHeight) + 'px'
				}, {duration:200, queue:true, complete:function() {snbusy = false;}});
		} else {
			listGroup.animate({
					top: (currentPos - objectHeight) + 'px'
				}, {duration:200, queue:true, complete:function() {snbusy = false;}});
		}
	}
}


jQuery.fn.cssNumber = function(prop){
    var v = parseInt(this.css(prop),10);
    return isNaN(v) ? 0 : v;
};


/* Trace (console.log)
================================================== */
function trace(msg) {
	if (window.console) {
		console.log(msg);
	}
	else if (typeof(jsTrace) != 'undefined') {
		jsTrace.send(msg);
	}
	else {
		//alert(msg);
	}
}
