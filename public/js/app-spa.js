
$(document).ready(function(){
	
	$("#product").chosen({disable_search_threshold: 20});
	$("#source").chosen({disable_search_threshold: 20});

	if (posted == '1') {
		$('#stage1').hide();
	} else {
		$('#stage2').hide();
	}
	
	$("#show_terms").bind('click',function(e) {
		e.preventDefault();
		$('#terms-box').show();
		return false;
	})

	$("a.closer").bind('click',function(e) {
		e.preventDefault();
		$('#terms-box').hide();
		return false;
	})

	$(".bSubmit").bind('click',function(e) {
		e.preventDefault();
		if ($(".chzn-select").val() == "") {
			$('#product-error').removeClass("hidden");
		} else {
			$('#stage1').slideUp('slow',function () {
				$('#stage2').slideDown();
			});
		}
		return false;
	});

	$("#terms-info").mCustomScrollbar({
		scrollButtons:{
			enable:true
		}
	});

	$('#terms-box').hide();
});
