﻿﻿/*
* HOLEST - jQuery Full Carousel 1.2.1
*
* Copyright 2010,(http://www.holest.com)
*
*
* Depends:
*   jquery.ui.core.js
*   jquery.ui.widget.js
*/
(function (a) {
    a.fn.extend({
        reflect: function (b) {
            b = a.extend({
                height: 1 / 3,
                opacity: 0.5
            }, b);
            return this.unreflect().each(function () {
                var c = this;
                if (/^img$/i.test(c.tagName)) {
                    function d() {
                        var g = c.width,
                            f = c.height,
                            l, i, m, h, k;
                        i = Math.floor((b.height > 1) ? Math.min(f, b.height) : f * b.height);
                        if (a.browser.msie && jQuery.browser.version < 9) {
                            l = a("<img />").attr("src", c.src).css({
                                width: g,
                                height: f,
                                marginBottom: i - f,
                                filter: "flipv progid:DXImageTransform.Microsoft.Alpha(opacity=" + (b.opacity * 100) + ", style=1, finishOpacity=0, startx=0, starty=0, finishx=0, finishy=" + (i / f * 100) + ")"
                            })[0];
                        } else {
                            l = a("<canvas />")[0];
                            if (!l.getContext) {
                                return;
                            }
                            h = l.getContext("2d");
                            try {
                                a(l).attr({
                                    width: g,
                                    height: i
                                });
                                h.save();
                                h.translate(0, f - 1);
                                h.scale(1, -1);
                                h.drawImage(c, 0, 0, g, f);
                                h.restore();
                                h.globalCompositeOperation = "destination-out";
                                k = h.createLinearGradient(0, 0, 0, i);
                                k.addColorStop(0, "rgba(255, 255, 255, " + (1 - b.opacity) + ")");
                                k.addColorStop(1, "rgba(255, 255, 255, 1.0)");
                                h.fillStyle = k;
                                h.rect(0, 0, g, i);
                                h.fill();
                            } catch (j) {
                                return
                            }
                        }
                        a(l).css({
                            display: "block",
                            border: 0
                        });
                        m = a(/^a$/i.test(c.parentNode.tagName) ? "<span />" : "<div />").insertAfter(c).append([c, l])[0];
                        m.className = c.className;
                        a.data(c, "reflected", m.style.cssText = c.style.cssText);
                        a(m).css({
                            width: g,
                            height: f + i,
                            overflow: "hidden"
                        });
                        c.style.cssText = "display: block; border: 0px";
                        c.className = "reflected"
                    }
                    if (c.complete) {
                        d();
                    } else {
                        a(c).load(d);
                    }
                }
            })
        },
        unreflect: function () {
            return this.unbind("load").each(function () {
                var c = this,
                    b = a.data(this, "reflected"),
                    d;
                if (b !== undefined) {
                    d = c.parentNode;
                    c.className = d.className;
                    c.style.cssText = b;
                    a.removeData(c, "reflected");
                    d.parentNode.replaceChild(c, d)
                }
            })
        }
    })
})(jQuery);

(function(jQuery, undefined) {
    jQuery.widget("holest.holest_carousel", {
        options: {
            contentdiv:null,//jQuery object that contains DIVs for info-panel content
            info_animation:'slide',//Amination to use with info-panel. Can be: fade,updown,slide
            animation_speed:600,//Animation speed
            movment: 1/18,//Movment in radians per each refresh interval
            refresh_interval:30,//refresh interval = 1/fps (ms)
            reflection:true,// Value indicating that image mirrors should be created
            reflection_height: 0.6,//Height of mirror image
            reflection_opacity: 1/2,//Opacitu or mirrored image
            InitialDirection:-1,//Initila direction; -1 is clockwise, 1 is counterclockwise
            radius:350,//Radius of rotation
            radius_effect:0.25,//Ratio or radis that will be added when fast spin is triggered 
            center:[420,140],//Center of rotation
            horisontal_aspect:0.1,//Perspectiv angle
            speedup_acc:30,// How manu times rotation accelerates on fast spin
						speedup_goto:20,// How manu times rotation accelerates during gotoItem
            speeddown_acc:1/10,//How manu times rotation deaccelerates on click
            speedup_acc_rate:-1/10,//Rate of fast spin deacceleration
            speeddown_acc_rate:1/5000,//Rate of acceleration after stop
            speedup_trigger_time:300,//Maximum time that can pass for user to drag mouse to trigger fast spin
            speedup_path_length_ratio:1/3,//Minimal length in width ratio of control which user must drag mouse to trigger fast spin
            item_width:120,//Overide default item width = 120
            item_height:120,//Overide default item height = 120
            doubleclickstop:true,//If true the rotation stops on doubleclick
			disablegoto:false,
			TopIndex:0,
			//EVENTS
			onInfoClick: function(Index){},
            onClick: function(Index){},
            onInfoDblClick: function(Index){},
            onDblClick: function(Index){},
			onTopIndexChange:function(Index){}
        },
        THIS:null,
        ItemCount:0,
        INFOPANEL:null,
        TopItem:null,
        TopIndex:null,
        PAUSE:false, 
		FSActive:false,
        ChangeDirection:null, 
        CurrentDirection:null,
		MoveToItem:null,
		MoveToItemTriggered:false,
        _create: function() {
            var self = this,
                options = self.options;
		    self.THIS = jQuery(this.element);
            var images = self.THIS.find('IMG');
			images.appendTo(self.THIS);
			self.THIS.find('::not(IMG)').remove();
			
			
			//self.THIS.css('position','static');
			//images.remove();
			
			
			if(self.options.onInfoClick) self.onInfoClick = self.options.onInfoClick;
            if(self.options.onClick) self.onClick = self.options.onClick;
            if(self.options.onInfoDblClick) self.onInfoDblClick = self.options.onInfoDblClick;
            if(self.options.onDblClick) self.onDblClick = self.options.onDblClick;
			
		    self.render = function(){
		    
			self.THIS.addClass("holest-carousel");
            self.THIS.addClass("unselectable");
            self.THIS.attr("unselectable","on"); 
            self.THIS[0].onselectstart =function(){return false;};
			if(!options.item_width) options.item_width = 120;
			if(!options.item_height) options.item_height = 120;
			self.ItemCount = self.THIS.find('IMG').length; 
            
			var DATE = new Date(); 
            var prev_refresh = DATE.getTime();
            var pos=0;
            var CurrentItemIndex = 0;
            var Speed = self.options.movment;
            var MOUSE_DOWN = false; 
            var DRAG_FINISH = false; 
            var IMIDIETE_POS = 0;
            var LAST_IMIDIETE_POS = 0;
            var DRAG_START_POS = null;
            var DRAG_START_TIME = null;
            var DRAG_END_POS = null;
            var ACCELERATION = 1;
            var ACCELERATION_RATE = 0;
            var PrevTopIndex = null;
            var TopIndex = null;
            var TopZ     = 0;
			
			self.THIS.find("IMG").each(function(ind){
              jQuery(this).addClass("ui-corner-all");
			  
              this.ondragstart = function(){return false;}; 
			  
 			  jQuery(this).innerHeight(self.options.item_height).innerWidth(self.options.item_width).css({left:"-1000px",top:"-1000px"}).show();
			  if(self.options.reflection){   
                jQuery(this).addClass("holest-carousel-item");
                jQuery(this).reflect({height: self.options.reflection_height, opacity: self.options.reflection_opacity});
              }
              else
              {
                jQuery(this).addClass("holest-carousel-item"); 
              }
			  
			  jQuery(this).click(function(){
			    if(!self.options.disablegoto){
					self.MoveToItem = ind;
					self.MoveToItemTriggered = true;
				}
			  });
			  
            });
			
            self.THIS.dblclick({_THIS:self},function(eArgs){
               if(eArgs.data._THIS.options.doubleclickstop)
               {
                 if(!eArgs.data._THIS.PAUSE) eArgs.data._THIS.PAUSE = true; 
               }
               eArgs.data._THIS.onDblClick(eArgs.data._THIS.TopIndex); 
            });
            self.THIS.click({_THIS:self},function(eArgs){
               if(eArgs.data._THIS.options.doubleclickstop)
               {
                 if(eArgs.data._THIS.PAUSE) eArgs.data._THIS.PAUSE = false; 
               }
               eArgs.data._THIS.onClick(eArgs.data._THIS.TopIndex); 
            }); 
            self.THIS.mousedown(function(){
             Speed = 0;
             self.THIS.css("cursor","pointer");  
             MOUSE_DOWN = true; 
             LAST_IMIDIETE_POS = 0;
             DRAG_START_TIME = (new Date()).getTime();
            });
            self.THIS.mouseup(function(){
             Speed = self.options.movment;
             self.THIS.css("cursor","default");
             MOUSE_DOWN = false; 
             if(DRAG_START_POS != null)
             {
               DRAG_FINISH = true; 
             }else 
             {
               ACCELERATION = self.options.speeddown_acc;
               ACCELERATION_RATE = self.options.speeddown_acc_rate;
             } 
            
            });
            self.THIS.mouseleave(function(){
             Speed = self.options.movment;
             self.THIS.css("cursor","default");
             MOUSE_DOWN = false; 
             if(DRAG_START_POS != null)
             {
               DRAG_END_POS =IMIDIETE_POS; 
               DRAG_FINISH  = false;
               if(DRAG_START_POS > DRAG_END_POS) self.ChangeDirection = -1;else self.ChangeDirection = 1;
               if(((new Date()).getTime() -  DRAG_START_TIME < self.options.speedup_trigger_time) && (Math.abs(DRAG_START_POS - DRAG_END_POS) > self.THIS.innerWidth()* self.options.speedup_path_length_ratio))
               {
			      if(!self.PAUSE){
					  ACCELERATION = self.options.speedup_acc;
					  ACCELERATION_RATE = self.options.speedup_acc_rate;
				  }
               }else
               {
                  ACCELERATION = self.options.speeddown_acc;
                  ACCELERATION_RATE = self.options.speeddown_acc_rate;
               }
               DRAG_START_POS = null; 
             }
            });  
            self.THIS.mousemove(function(e){
             if(DRAG_FINISH)
             {
               DRAG_END_POS = e.pageX - self.THIS.offset().left;
               DRAG_FINISH  = false;
               if(DRAG_START_POS > DRAG_END_POS) self.ChangeDirection = -1;else self.ChangeDirection = 1;
               if(((new Date()).getTime() -  DRAG_START_TIME < self.options.speedup_trigger_time) && (Math.abs(DRAG_START_POS - DRAG_END_POS) > self.THIS.innerWidth()* self.options.speedup_path_length_ratio))
               {
			      if(!self.PAUSE){
					  ACCELERATION = self.options.speedup_acc;
					  ACCELERATION_RATE = self.options.speedup_acc_rate;
				  }
               }else
               {
		          ACCELERATION = self.options.speeddown_acc;
                  ACCELERATION_RATE = self.options.speeddown_acc_rate;
		       }
               DRAG_START_POS = null;  
             }
             if(MOUSE_DOWN)
             {
               if(LAST_IMIDIETE_POS == 0)
               {
                  LAST_IMIDIETE_POS = e.pageX - self.THIS.offset().left;
                  DRAG_START_POS = LAST_IMIDIETE_POS;
               }
               else IMIDIETE_POS = e.pageX - self.THIS.offset().left;
             } 
            });   
            var newdir = 0;
            jQuery.holest_carousel_move = function(params) {
                   for(var i in params)
                   this[i] = params[i];
                   this.dir = this.dir || 1;
                   if(this.dir == 1)
                   self.CurrentDirection = 'left';
                   else self.CurrentDirection = 'right';
                   this.css = function(p) {
				        if(Speed != 0) Speed = self.options.movment; 
				   
                       if(CurrentItemIndex == 0){// limit frames
                           //if(self.PAUSE) return;  
                           if( (new Date()).getTime() - prev_refresh < self.options.refresh_interval) return {}; 
                           prev_refresh = (new Date()).getTime();
                           TopIndex = CurrentItemIndex; 
                           TopZ     = 0;
                           if(self.ChangeDirection != null)
                           {
                             this.dir = self.ChangeDirection;
                             self.ChangeDirection = null; 
                           } 
                       }
                       if(IMIDIETE_POS != 0)
                       {
                        try{
                             if(IMIDIETE_POS > LAST_IMIDIETE_POS)
                             self.CurrentDirection = 'left';
                             else self.CurrentDirection = 'right';  
                             var MoveAngle = (360 * ((IMIDIETE_POS - LAST_IMIDIETE_POS) / (2*self.options.radius *Math.PI)));
                             MoveAngle = MoveAngle % 45; 
                             pos = pos + MoveAngle;    
                             LAST_IMIDIETE_POS  = IMIDIETE_POS;   
                             IMIDIETE_POS = 0;
                         }
                         catch(ex)
                         {
                             IMIDIETE_POS = 0;
                             LAST_IMIDIETE_POS = 0;
                         }  
                       }
                       var a = pos;
                       if(pos > 360 || pos < -360)
                       {
                         pos = 0;
                       }
					   
					   if(self.MoveToItem != null ){
					    
					     var tPos = 360 - ((360 / self.ItemCount) * self.MoveToItem);									 
                         var cPos = pos < 0 ? pos + 360 : pos;
						 						 
						 if(self.MoveToItemTriggered){
						    self.MoveToItemTriggered = false;
							var change = false;
							if( Math.abs(tPos - cPos) < 1){
							 //
							}else if(this.dir == 1){
							  if(tPos > cPos){
							    if( tPos - cPos > 180) change = true;
							  }else{
							    if( cPos - tPos < 180) change = true;
							  }
							}else{
							  tPos = (360 - tPos);
							  cPos = (360 - cPos);
							 
							 if(tPos > cPos){
							    if( tPos - cPos > 180) change = true;
							  }else{
							    if( cPos - tPos < 180) change = true;
							  }
							}
							if( change) {
							   this.dir = this.dir * -1;
							   if(this.dir == 1)
                                     self.CurrentDirection = 'left';
                                     else self.CurrentDirection = 'right';
							}
						 }
					     
						 
						 if(Math.abs(tPos - cPos) < 2) {
						   pos = tPos;
						   self.MoveToItem = null;
						   self.PAUSE = true;
						 }else pos+= (Speed * self.options.speedup_goto * this.dir );
						 
					   }else 
                         if(!self.PAUSE) pos+= (Speed * ACCELERATION * this.dir);
						
					   var radius_diff = 1;
                       if(Math.abs(ACCELERATION - 1) > 0.1)
                       {
                         ACCELERATION += ACCELERATION_RATE;
                         if(ACCELERATION  > self.options.speedup_acc/2)
                         {
                            radius_diff = 1 + self.options.radius_effect * (1 -((ACCELERATION - self.options.speedup_acc/2)/(self.options.speedup_acc/2)));  
                         }
                         else if( ACCELERATION  <= self.options.speedup_acc/2 && ACCELERATION > 1)
                         {
                            radius_diff = 1 + self.options.radius_effect * (1 -(( self.options.speedup_acc/2 - ACCELERATION)/(self.options.speedup_acc/2 - 1)));
                         }    
						 self.FSActive = true;
                       }else {
					     self.FSActive = false;
 					     ACCELERATION = 1;
					   }
					   
                       a = (a + CurrentItemIndex * (360/self.ItemCount)) * Math.PI / 180; // to radians
                       
                       var Deepnes = (Math.ceil((Math.cos(a)+1)*(1000))) ; 
                       
                       if(Deepnes > TopZ)
                       {
                         TopZ = Deepnes;
                         TopIndex = CurrentItemIndex;  
                       }
                       
					   var Scale   = (Deepnes + 1000) / 3000;
					   //Deepnes *= (1 - Math.abs(self.options.horisontal_aspect));
					   var n = CurrentItemIndex;
                       CurrentItemIndex++;
					   
					   var Item = jQuery(self.THIS.children('.holest-carousel-item')[n]); 
					   var x = Math.sin(a) *  self.options.radius * radius_diff + self.THIS.offset().left + self.options.center[0] ;
                       var y = Math.cos(a) * (self.options.radius * self.options.horisontal_aspect) + self.THIS.offset().top + self.options.center[1] ;
					   
					   
                       if(CurrentItemIndex >=  self.ItemCount)
                       {
                             CurrentItemIndex = 0; 
                             if(newdir !=0){
                                 if(newdir != this.dir) {
                                    this.dir = newdir;
                                     if(this.dir == 1)
                                     self.CurrentDirection = 'left';
                                     else self.CurrentDirection = 'right';
                                 }
                                 newdir = 0;      
                             }
                            try
                            {
                              if(PrevTopIndex != TopIndex )
                              {
                                 self.onTopItemChange(self,PrevTopIndex,TopIndex);
                                 PrevTopIndex = TopIndex;  
                                 self.TopIndex = TopIndex;
                                 self.TopItem  = jQuery(self.THIS.children('.holest-carousel-item')[TopIndex]);
                              } 
                           }catch(ex){}  
                       }
                       var W = self.options.item_width * Scale;
                       var H = self.options.item_height * Scale;
					   
                       if(self.options.reflection)
                       {
                          
                         if(Item[0].tagName == "IMG") return {};
                         var TrueH = H * (1 + self.options.reflection_height);
                         Item.children()[0].width = Math.round(W);
                         Item.children()[0].height = Math.round(H); 
                         if(Item.children()[1])
                         {
                            if(Item.children()[1].tagName.toUpperCase() == "CANVAS" && false)
                            {
                              var DC = Item.children()[1].getContext('2d');
                              DC.scale(Scale,Scale);
                            }else
                            {
                              Item.children()[1].style.width  = Math.round(W) + 'px';
                              Item.children()[1].style.height = Math.round(H * self.options.reflection_height) + 'px';
                            } 
                         }
                         Item.innerHeight(Math.round(TrueH));
                         Item.innerWidth(Math.round(W));
                         Item.css({top: y + "px", left: x + "px",zIndex: Deepnes });
                         return {}; 
                       } 
                       else      
                            return {top: y + "px", left: x + "px",width: (self.options.item_width * Scale) + "px" ,height: (self.options.item_height * Scale) + "px" ,zIndex: Deepnes };
                   }
            };
            jQuery.fx.step.holest_carousel_move = function(fx){
                var css = fx.end.css(1 - fx.pos);
                for(var i in css) 
                {
                    try{
                    fx.elem.style[i] = css[i];
                    }catch(exp)
                    {}
                }
              }; 
            self.THIS.children(".holest-carousel-item").animate({
											holest_carousel_move : new jQuery.holest_carousel_move({
												//center	: self.options.center,
												//radius	: self.options.radius,
												dir	    : self.options.InitialDirection   
											})
										},60000000);
			};	
            /*
			var loaded = 0;
			images.each(function(ind){
			  var img = jQuery("<img />");
			  img.css('display','none').attr('src',jQuery(this).attr('src'));
			  img.load(function(){
			    loaded++;
				if(loaded == images.length){
				  self.render();	
				}
			  });
			  self.THIS.append(img);
			});
			*/
			self.render();	
			
	    },
        _setOption: function(key, value) {
            this.options[key] = value;
        },
        onTopItemChange: function(sender,prev_index,new_index){
          jQuery(sender.THIS.children()[prev_index]).removeClass('holest-carousel-top-item');
          jQuery(sender.THIS.children()[new_index]).addClass('holest-carousel-top-item');
          if(sender.options.contentdiv != null)
          {
           if(sender.INFOPANEL == null)
           {
             sender.INFOPANEL = jQuery("<div class='holest-carousel-info-panel'></div>"); 
             sender.THIS.append(sender.INFOPANEL);
             sender.INFOPANEL.css({zIndex:1000}); 
             jQuery(sender.options.contentdiv).children('DIV').each(function(){
                   var AnimCarrier = jQuery("<div class='holest-carousel-info-panel-content'></div>"); 
                   AnimCarrier.css('display','none');
                   AnimCarrier.css('overflow','hidden'); 
                   AnimCarrier.append(jQuery(this));
                   jQuery(this).css('width',(sender.INFOPANEL.innerWidth() - 20) + 'px'); 
                   sender.INFOPANEL.append(AnimCarrier);
             });
            sender.INFOPANEL.dblclick({_THIS:sender},function(eArgs){
               eArgs.data._THIS.onInfoDblClick(eArgs.data._THIS.TopIndex); 
            });
            sender.INFOPANEL.click({_THIS:sender},function(eArgs){
               eArgs.data._THIS.onInfoClick(eArgs.data._THIS.TopIndex); 
            }); 
           }
           if(prev_index == null)
           {
            jQuery(sender.INFOPANEL.children()[new_index]).show();  
           }
           else
           {
               jQuery(sender.INFOPANEL.children()[prev_index]).stop(true,true); 
               if(sender.options.info_animation == "fade")
               {  
			     try{
			       sender.INFOPANEL.children().each(function(ind){
						if(ind != prev_index && ind != new_index) jQuery(this).hide();
				   });
				   
				   var FOSpeed = sender.options.animation_speed;
				   var FISpeed = sender.options.animation_speed;
				   if(sender.FSActive){
				     FOSpeed = 0;
					 FISpeed = 200;
				   }
				   
				   jQuery(sender.INFOPANEL.children()[prev_index]).fadeOut(FOSpeed ,function(){
                     jQuery(sender.INFOPANEL.children()[new_index]).fadeIn(FISpeed ,function(){
					    jQuery(sender.INFOPANEL.children()[prev_index]).hide();    
					 });
                   });
				 }catch(e){};
               }
               else if(sender.options.info_animation == "updown")
               {
			       jQuery(sender.INFOPANEL.children()[prev_index]).slideUp(sender.options.animation_speed,function(){
				   sender.INFOPANEL.children().each(function(ind){
						if(ind != prev_index && ind != new_index) jQuery(this).hide();
				   });	       
                     jQuery(sender.INFOPANEL.children()[new_index]).slideDown(sender.options.animation_speed);
                   });
               }
               else if(sender.options.info_animation == "slide")
               {
			      
                   sender.INFOPANEL.children().each(function(ind){ 
                   jQuery(this).hide();
                   });
                   jQuery(jQuery(sender.INFOPANEL.children()[new_index]).children()[0]).hide();
                   jQuery(sender.INFOPANEL.children()[new_index]).show();
                   jQuery(jQuery(sender.INFOPANEL.children()[new_index]).children()[0]).show("slide",{ direction: sender.CurrentDirection  },sender.options.animation_speed);
               } 
           } 
          } 
		  sender.options.TopIndex = new_index;
		  try{
		     sender.options.onTopIndexChange(new_index);
		  }catch(e){
		  }
        },
		gotoItem:function(index){
		  var self = this;
		  self.MoveToItem = index;
		  self.MoveToItemTriggered = true;
		},
        onInfoClick: function(Index){},
        onClick: function(Index){},
        onInfoDblClick: function(Index){},
        onDblClick: function(Index){},
        destroy: function() {
            jQuery.Widget.prototype.destroy.call(this);
        }
    }); // widget
})(jQuery);