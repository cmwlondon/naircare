(function( $ ) {
  $.fn.cookieNotify = function(element) {
		this.element = element;
		if (this.element) {
			this.element.find('a.close').bind('click',jQuery.proxy(this,'hideNotice'));
		}
  };
	
	$.fn.hideNotice = function(e) {
		e.preventDefault();
		if (this.element) {
			//$(this.element).slideUp('slow');
			//var height = $('#cookie-notify').height() + 20 * -1;
			$(this.element).animate({'height': '0px', 'padding-top': 0, 'padding-bottom': 0},500);
			$('.cookieFix').animate({'margin-top': '0px'}, 500);
			this.noticeSeen();
		}
	};
		
	$.fn.noticeSeen = function () {
		$.ajax({
			url: siteurl+'ajax/cookies/setCookie',
			context: this
		}).done(function() { 
			//console.log("AJAX Done");
		});
	}
})( jQuery );