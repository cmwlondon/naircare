@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		 <img src="/images/main/consultation/online-consultation-background.jpg" />
	</div>
	<div id="right-content">
		<h2>Welcome to Nair Online <strong>Consultation</strong></h2>
		<p>It&rsquo;s important to us to help you find the right hair removal product that suits both your hair and skin type. So together with our team of expert Nair technicians we&rsquo;ve designed our online consultation.</p>
		<p>Complete this short survey of essential questions to help you find the best product and give you the beautifully smooth results that you deserve.</p>
		<div class="button-group">
			<div class="c-btn">
				<a href="/online-consultation/start"><div class="inner">Get Started&hellip;</div></a>
			</div>
		</div>
	</div>
</div>

@endsection

