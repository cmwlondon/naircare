@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/consultation/online-consultation-background.jpg" />
	</div>
	<div id="right-content">
		<h3 class="progress">Your progress: <span data-id="1">1</span> - <span data-id="2">2</span> - <span data-id="3">3</span> - <span data-id="4">4</span> - <span data-id="5">5</span> - <span data-id="6">6</span> - Results</h3>
		<div class="group hidden" data-grp="1">
			<h2 class="question"><span><strong>What&rsquo;s your natural hair colour?</strong></span></h2>
			<div class="button-group">
				<div class="c-btn b1">
					<a href="#" data-sec="1" data-ans="1">
						<div class="inner">Dark</div>
					</a>
				</div>
				<div class="c-btn b2">
					<a href="#" data-sec="1" data-ans="2">
						<div class="inner">Brown</div>
					</a>
				</div>
				<div class="c-btn b3">
					<a href="#" data-sec="1" data-ans="3">
						<div class="inner">Blonde</div>
					</a>
				</div>
				<div class="c-btn b4">
					<a href="#" data-sec="1" data-ans="4">
						<div class="inner">Red</div>
					</a>
				</div>
			</div>
		</div>
		<div class="group hidden" data-grp="2">
			<h2 class="question"><span><strong>Would you describe your skin as sensitive?</strong></span></h2>
			<div class="button-group"><div class="c-btn b1"><a href="#" data-sec="2" data-ans="1"><div class="inner">Yes</div></a></div><div class="c-btn b2"><a href="#" data-sec="2" data-ans="2"><div class="inner">No</div></a></div>  </div>
		</div>
		<div class="group hidden" data-grp="3">
			<h2 class="question"><span><strong>Which most describes the hairs on your legs?</strong></span></h2>
			<div class="button-group"><div class="c-btn b1"><a href="#" data-sec="3" data-ans="1"><div class="inner">Thick & grows too quickly</div></a></div><div class="c-btn b2"><a href="#" data-sec="3" data-ans="2"><div class="inner">Average, but re-growth is a problem</div></a></div><div class="c-btn b3"><a href="#" data-sec="3" data-ans="3"><div class="inner">It&rsquo;s fair and I&rsquo;d like to keep it that way</div></a></div>  </div>
		</div>
		<div class="group hidden" data-grp="4">
			<h2 class="question"><span><strong>Which area do you struggle with most?</strong></span></h2>
			<div class="button-group"><div class="c-btn b1"><a href="#" data-sec="4" data-ans="1"><div class="inner">Bikini</div></a></div><div class="c-btn b2"><a href="#" data-sec="4" data-ans="2"><div class="inner">Back of legs</div></a></div><div class="c-btn b3"><a href="#" data-sec="4" data-ans="3"><div class="inner">Knee</div></a></div><div class="c-btn b4"><a href="#" data-sec="4" data-ans="4"><div class="inner">Face</div></a></div><div class="c-btn b5"><a href="#" data-sec="4" data-ans="5"><div class="inner">Underarms</div></a></div>  </div>
		</div>
		<div class="group hidden" data-grp="5">
			<h2 class="question"><span><strong>Which of the following do you most suffer from?</strong></span></h2>
			<div class="button-group"><div class="c-btn b1"><a href="#" data-sec="5" data-ans="1"><div class="inner">In-growing hairs</div></a></div><div class="c-btn b2"><a href="#" data-sec="5" data-ans="2"><div class="inner">Patchy re-growth (e.g. knees, ankles, backs of legs)</div></a></div><div class="c-btn b3"><a href="#" data-sec="5" data-ans="3"><div class="inner">Razor rash/bumps</div></a></div><div class="c-btn b4"><a href="#" data-sec="5" data-ans="4"><div class="inner">Dry skin</div></a></div>  </div>
		</div>
		<div class="group hidden" data-grp="6">
			<h2 class="question"><span><strong>How long do you expect results to last?</strong></span></h2>
			<div class="button-group"><div class="c-btn b1"><a href="#" data-sec="6" data-ans="1"><div class="inner">A day or less</div></a></div><div class="c-btn b2"><a href="#" data-sec="6" data-ans="2"><div class="inner">2-5 days</div></a></div><div class="c-btn b3"><a href="#" data-sec="6" data-ans="3"><div class="inner">6 days plus</div></a></div>  </div>
		</div>
		<a href="" class="back hidden">Back</a>

	</div>
</div>
@endsection

