@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		 <img src="/images/main/consultation/online-consultation-background.jpg" />
	</div>
	<div id="right-content">

		<div id="scrollblock">
			<h2>Results</h2>
			@if ( count($products) > 0 )
			<h2>Your <strong>Products</strong></h2>
			<div id="product-images">
			@foreach($products as $product)
				<a href="{{ $product['url'] }}" title="{!! $product['title'] !!}"><image src="{{ $product['image'] }}" alt="{!! $product['title'] !!}" /></a>
			@endforeach
			</div>
			@endif

			<h2>Your <strong>Profile</strong></h2>
			@foreach($items as $item)
				<h4 class="mt0">{!! $item['question'] !!}<br/>You answered: {!! $item['response'] !!}</h4>
				<p class="mt0" style="margin-bottom:15px;">{!! $item['text'] !!}</p>
			@endforeach

			<p class="mt30"><strong>TOP TIP!</strong> {!! $toptip !!}</p>
		</div>
	</div>
</div>

@endsection

