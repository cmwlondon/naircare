@extends('Main.Layouts.main')

@section('content')
<div id="container">
	@if ( $mode === 'form')
    	@include('Main.Contact.partials.contact_form')
    @else
    	@include('Main.Contact.partials.contact_thanks')
    @endif
    @yield('right-content')
</div>
@endsection
