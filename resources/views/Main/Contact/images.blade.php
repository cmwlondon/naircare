@extends('Main.Contact.frame')

@section('right-content')
	<div id="right-content">
		<h2>Press <strong>Room</strong></h2>
		<ul id="page-tabs">
			<li><a href="/contact-us">Enquiries</a></li>
			<li class="active" ><a href="/contact-us/images">Images</a></li>
			<li ><a href="/contact-us/press-releases">Press Releases</a></li>
		</ul>
		<div id="copy">
			<div id="carousel">
				
				<ul>
					<!--
					<li><a href="http://www.naircare.co.uk/download/image/Nair-Brazilian-Spa-Clay-Body-Wax-Strips.png"><img src="/images/main/products/thumbs/nair-brazilian-spa-clay-body-wax-strips.png" alt="Nair Brazilian Spa Clay Body Wax Strips" title="Nair Brazilian Spa Clay Body Wax Strips"/></a></li>
					<li><a href="http://www.naircare.co.uk/download/image/Nair-Brazilian-Spa-Clay-Facial-Wax-Strips.png"><img src="/images/main/products/thumbs/nair-brazilian-spa-clay-facial-wax-strips.png" alt="Nair Brazilian Spa Clay Facial Wax Strips" title="Nair Brazilian Spa Clay Facial Wax Strips"/></a></li>
					
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair_Body_Wax_Strips.jpg"><img src="/images/main/products/thumbs/argon-body-wax-strips.png" alt="Argan Oil Body Wax Strips" title="Argan Oil Body Wax Strips"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair_Facial_Wax_Strips_with_Argan_Oil.jpg"><img src="/images/main/products/thumbs/argon-facial-wax-strips.png" alt="Argan Oil Facial Wax Strips" title="Argon Oil Facial Wax Strips"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair_Shower_Power_Cream_with_Argan_Oil.jpg"><img src="/images/main/products/thumbs/argon-shower-power-cream.png" alt="Argan Oil Shower Power Cream" title="Argan Oil Shower Power Cream"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair_Ultra_Precision_Bikini_Brush_On_Hair_Removal_.jpg"><img src="/images/main/products/thumbs/argon-bikini-brush-on.png" alt="Bikini Brush-On" title="Bikini Brush-On"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair_Washable_Roll_On_Wax_with_Argan_Oil.jpg"><img src="/images/main/products/thumbs/argan-oil-roll-on.png" alt="Argan Oil Roll-On Wax" title="Argan Oil Roll-On Wax"/></a></li>
					
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Facial-Brush-On.png"><img src="/images/main/products/thumbs/nair-facial-brush-on.png" alt="Nair Facial Brush On" title="Nair Facial Brush On"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Glide-On.png"><img src="/images/main/products/thumbs/nair-glide-on.png" alt="Nair Glide On" title="Nair Glide On"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Sensitive-Cream.png"><img src="/images/main/products/thumbs/nair-sensitive-cream.png" alt="Nair Sensitive Cream" title="Nair Sensitive Cream"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Sensitive-Cream-Sachet.png"><img src="/images/main/products/thumbs/nair-sensitive-cream-sachet.png" alt="Nair Sensitive Cream Sachet" title="Nair Sensitive Cream Sachet"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Soothing-Body-Wax-Strips.png"><img src="/images/main/products/thumbs/nair-soothing-body-wax-strips.png" alt="Nair Soothing Body Wax Strips" title="Nair Soothing Body Wax Strips"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Soothing-Wax-Strips.png"><img src="/images/main/products/thumbs/nair-soothing-wax-strips.png" alt="Nair Soothing Wax Strips" title="Nair Soothing Wax Strips"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Tough-Hair-Cream.png"><img src="/images/main/products/thumbs/nair-tough-hair-cream.png" alt="Nair Tough Hair Cream" title="Nair Tough Hair Cream"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Ultra-Precision.png"><img src="/images/main/products/thumbs/nair-ultra-precision.png" alt="Nair Ultra Precision" title="Nair Ultra Precision"/></a></li>
					<li><a target="_blank" href="http://www.naircare.co.uk/download/image/Nair-Upper-Lip-Kit.png"><img src="/images/main/products/thumbs/nair-upper-lip-kit.png" alt="Nair Upper Lip Kit" title="Nair Upper Lip Kit"/></a></li>
					//-->
				</ul>
			</div>
			<div class="arrow left" data-direction="left"></div>
			<div class="arrow right" data-direction="right"></div>
		</div>
		

	</div>
@endsection
