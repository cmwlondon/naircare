@extends('Main.Contact.frame')

@section('right-content')
	<div id="right-content">
		<h2>Press <strong>Room</strong></h2>
		<ul id="page-tabs">
			<li class="active"><a href="/contact-us">Enquiries</a></li>
			<li ><a href="/contact-us/images">Images</a></li>
			<li ><a href="/contact-us/press-releases">Press Releases</a></li>
		</ul>
		<div id="copy">
			<p>For press enquiries, please contact Capsule Comms:</p>
			<p>Email. <a href="mailto:nair@capsulecomms.com ">nair@capsulecomms.com</a></p>
		</div>
	</div>
@endsection
