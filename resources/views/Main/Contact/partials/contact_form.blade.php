	<div id="main-info">
		<h2>Contact <strong>Us</strong></h2>

		<!-- form START-->
		<p>If you have a question about the Nair range then please complete the form below or call our consumer enquiries team on 0800 121 6080.</p>

		{!! Form::open(array('url' => ['/contact-us'], 'method' => 'POST', 'id' => 'contactForm', 'class' => 'contactForm') )!!}		

			<div class="form-row mt25">
				{!! Form::label('title', 'Title: *') !!}
				{!! Form::select('title', $titleOptions, null, ['id' => 'title', 'class' => 'select'] ) !!}
			</div>
			{!! $errors->first('title', '<div class="error">:message</div>') !!}

			<div class="form-row">
				{!! Form::label('firstname', 'First Name: *') !!}
				{!! Form::text('firstname',null,['placeholder' => '', 'id' => 'firstname', 'class' => 'textField300']) !!}
			</div>
			{!! $errors->first('firstname', '<div class="error">:message</div>') !!}

			<div class="form-row">
				{!! Form::label('surname', 'Last Name: *') !!}
				{!! Form::text('surname',null,['placeholder' => '', 'id' => 'surname', 'class' => 'textField300']) !!}
			</div>
			{!! $errors->first('surname', '<div class="error">:message</div>') !!}

		<div class="form-row">
			{!! Form::label('company', 'Company:') !!}
			{!! Form::text('company',null,['placeholder' => '', 'id' => 'company', 'class' => 'textField300']) !!}
		</div>
		{!! $errors->first('company', '<div class="error">:message</div>') !!}

		<div class="form-row">
			{!! Form::label('phone', 'Phone Number: *') !!}
			{!! Form::text('phone',null,['placeholder' => '', 'id' => 'phone', 'class' => 'textField300']) !!}
		</div>
		{!! $errors->first('phone', '<div class="error">:message</div>') !!}

		<div class="form-row">
			{!! Form::label('email', 'Email Address: *') !!}
			{!! Form::text('email',null,['placeholder' => '', 'id' => 'email', 'class' => 'textField300']) !!}
		</div>
		{!! $errors->first('email', '<div class="error">:message</div>') !!}

		<div class="form-row">
			{!! Form::label('product', 'Product:') !!}
			{!! Form::text('product', null, ['placeholder' => '', 'id' => 'product', 'class' => 'textField300']) !!}
		</div>
		{!! $errors->first('product', '<div class="error">:message</div>') !!}

		<div class="form-row">
			{!! Form::label('comments', 'Comments: *') !!}
			{!! Form::textarea('comments',null,['placeholder' => '', 'id' => 'comments', 'rows' => '4', 'class' => 'textField300']) !!}
		</div>
		{!! $errors->first('comments', '<div class="error">:message</div>') !!}

		{!! Form::hidden('mfgcode',null,[]) !!}		

		<div class="form-row">
			<label>&nbsp;</label>
			{!! Form::submit('SUBMIT', array('class' => 'submit', 'id' => 'Submit', 'class' => 'bSubmit')) !!}
		</div>

		{!! Form::close() !!}
		<!-- form END -->

		<!-- confirm START -->
		<!-- 
		<p>Thank you for contacting us!</p>
		-->
		<!-- confirm END -->
	</div>
