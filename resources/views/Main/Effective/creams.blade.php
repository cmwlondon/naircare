@extends('Main.Effective.frame')

@section('column')
<p>It&rsquo;s not just your legs, bikini and underarms that need frequent de-fuzzing. Removing hair from those more subtle areas such as your eyebrows can be a dull and time consuming chore. That&rsquo;s why we have designed an easy to use range that quickly and effectively takes care of hair.</p>
<p><strong>Creams</strong></p>
<p>Nair fuss-free hair removal creams take just minutes to work and contain natural extracts that help moisturise and care for skin so that it feels soft to the touch for days.</p>
@endsection
