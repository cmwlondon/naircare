@extends('Main.Layouts.main')

@section('content')
<div id="container">
	 <div id="main-image">
			<div id="anim-frame">
				<div id="anim">
					<img src="/images/main/effective/shim.png" width="100%" class="shim"/>
					<div id="flash-anim"></div>
				</div>
				<menu id="anim-menu">
					<li class="b1 active"><a href="/effective-hair-removal/shaving">Shaving</a></li>
					<li class="b2"><a href="/effective-hair-removal/wax">Wax</a></li>
					<li class="b3"><a href="/effective-hair-removal/creams">Creams</a></li>
				</menu>
			</div>
	 </div>
	 <div id="right-content">
			<h2><strong>Nair.</strong> Effective hair removal</h2>
			<p>Each Nair formula is designed to gently remove hair from your skin to leave it feeling silky smooth for longer. Whether you choose cream, wax or shaving, Nair has a product that&rsquo;s right for you. </p>
			@yield('column')
	 </div>
</div>
@endsection
