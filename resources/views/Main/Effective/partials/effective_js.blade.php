<script src="/js/libs/swfobject/swfobject.js"></script>
<script src="/js/libs/greensock/minified/plugins/CSSPlugin.min.js"></script>
<script src="/js/libs/greensock/minified/easing/EasePack.min.js"></script>
<script src="/js/libs/greensock/minified/TweenLite.min.js"></script>
<script src="/js/libs/greensock/minified/TimelineLite.min.js"></script>
<script>
	var frameCount = {{ $framecount }};
	var section = '{{ $section }}';
</script>
<script src="/js/app-effective.js"></script>