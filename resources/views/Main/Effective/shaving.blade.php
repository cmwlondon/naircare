@extends('Main.Effective.frame')

@section('column')
<p><strong>Shaving</strong></p>
<p>With shaving often comes the risk of cuts and bumps which can leave skin looking dry and dull. Nair <a href="/nair-collection/triple-action-cream" style="color:#7b19aa;"><b>Cleanse and Shave Triple Action Cream</b></a> has been specially designed to gently cleanse while allowing you to always enjoy a luxurious yet effective smooth shave - perfect for bump-free skin, which feels moisturised and <b>smoother for longer</b>. The Nair range has been carefully developed to help you remove unwanted hair, quickly and effectively whilst soothing and caring for skin to leave it looking radiant. </p>
@endsection
