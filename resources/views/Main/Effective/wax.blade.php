@extends('Main.Effective.frame')

@section('column')
<p>It&rsquo;s not just your legs, bikini and underarms that need frequent de-fuzzing. Removing hair from those more subtle areas such as your eyebrows can be a dull and time consuming chore. That&rsquo;s why we have designed an easy to use range that quickly and effectively takes care of hair. </p>
<p><strong>Wax</strong></p>
<p>Nair waxes are a perfect hair removal solution if you&rsquo;re looking for longer lasting results form the comfort of your own home. Nair wax strips are quick and easy to use and even tackle short hairs (about 2mm) for results that last up to four weeks.</p>
@endsection
