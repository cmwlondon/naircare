<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">    
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Your Enquiry</title>
    <style type="text/css">
    
		/* Client-specific Styles */
		#outlook a { padding: 0; } /* Force Outlook to provide a "view in browser" button. */
		body { width: 100% !important; } .ReadMsgBody { width: 100%; } .ExternalClass { width: 100%; } /* Force Hotmail to display emails at full width */
		body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; } /* Prevent Webkit and Windows Mobile platforms from changing default font sizes. */

		/* Reset Styles */
		body { margin: 0; padding: 0; font-family: Arial, Helvetica, sans-serif;}
		img { height: auto; line-height: 100%; outline: none; text-decoration: none; }
		#backgroundTable { height: 100% !important; margin: 0; padding: 0; width: 100% !important; }
    
       p {
           margin: 1em 0;
       }
       
       h1, h2, h3, h4, h5, h6 {
           color: black !important;
           line-height: 100% !important;
       }
       
       h1 a, h2 a, h3 a, h4 a, h5 a, h6 a {
           color: blue !important;
       }
       
       h1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {
           color: red !important; /* Preferably not the same color as the normal header link color.  There is limited support for psuedo classes in email clients, this was added just for good measure. */
       }
       
       h1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {
           color: purple !important; /* Preferably not the same color as the normal header link color. There is limited support for psuedo classes in email clients, this was added just for good measure. */
       }
       
       table td {
           border-collapse: collapse;
       }

       .yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important; } /* Body text color for the New Yahoo.  This example sets the font of Yahoo's Shortcuts to black. */
       
       
       </style>
</head>
<body>
<table cellpadding="10" cellspacing="0" border="0" id="backgroundTable">
	<tr>
    <td>

      <p>Dear Consumer</p>
      <p>This is an automated response please do not reply to this message.</p>
      <p><strong>We acknowledge receipt of your emailed enquiry and thank you for taking the time to contact</strong> <strong>us.</strong> We advise that we will endeavour to respond to you within <strong>5 business days</strong>. If however your enquiry is of an urgent nature then please do not hesitate to contact us directly using the details below:</p>
      <p>Church &amp; Dwight UK Ltd<br />
      Consumer Relations Department<br />
      Wear Bay Road<br />
      Folkestone<br />
      Kent CT19 6PG<br />
      <br />
      Tel: 0800 121 6080</p>
      <p><strong>If you are contacting us with regard to seeking a refund or replacement product, please be aware that we can only process these requests if you provide the following;</strong></p>
      <p><strong>1) Full name</strong><br />
      <strong>2) Contact address</strong><br />
      <strong>3) Recorded batch number (when possible), this number is located on the end of a tube seal or in the join section of sachets, embossed and begins with FE.... Or is printed in black on the back of item labels or on the base. In the instance of wax products, then this number will either started with FX or FN</strong><br />
      <strong>4) Name of retail store</strong><br />
      <strong>5) Original purchase price</strong><br />
      <strong>6) Brief details regarding your experience with our product</strong></p>
      <p>In the event that you do contact us by alternative means please ensure you refer to your original email, this will prevent duplication. <strong>We thank you for taking the time to contact us regarding your enquiry, all consumer feedback is important to us and in that regard we will contact you again as soon as possible.</strong></p>
      <p>Yours faithfully<br />
      Consumer Relations Team<br />
      Church &amp; Dwight UK Ltd</p>
    	
    </td>
	</tr>
</table>  
</body>
</html>