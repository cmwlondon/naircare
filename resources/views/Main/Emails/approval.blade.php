A new product review has been added to the Naircare website.

name: {{ $r_name }}
product: {{ $r_product }}
rating: {{ $stars }}
date: {{ date('D, d M Y H:i:s',$r_when) }}
comment: {{ $r_comment }}

Approve the review:
{{ url('',[]) }}/product/approve-review/{{ $r_id }}