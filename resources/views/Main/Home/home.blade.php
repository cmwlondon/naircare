@extends('Main.Layouts.main')

@section('content')
<div id="home">
	<img src="/images/main/home-v6/background.gif" class="mobile-header"/>
	<div class="mobile-wrap">
		<h2>New Nair Nourish 7 in 1 Wax Strips</h2>
		<p>7 benefits in 1 unique formula. Without any heating or rubbing needed these strips are ready to be applied straight to your skin, removing even short hairs without leaving any waxy mess just beautifully soft, smooth skin for up to 4 weeks.</p>
		<img src="/images/main/home-v6/range.png" alt="Nair Nourish 7 in 1 Wax Strips" class="range"/>
		<a href="http://www.boots.com/en/Boots-Brands-A-to-Z/Nair/ " class="buy-now" target="_blank">Buy now</a>
	</div>
</div>
@endsection

