@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/hub/bg-bikini.jpg" />
	</div>
	<div id="right-content">
		<div id="copy">
			<h2>Take care of your <strong>bikini area</strong></h2>
			<p>It can sometimes be difficult to achieve skin which looks and feels smooth in your bikini area without the risk of nicks or razor bumps that can come with shaving.</p>
			<p>Nair creams, waxes &amp; balms quickly and effectively remove hair in sensitive areas to ensure your skin is looking its best.</p>
		</div>

		<div id="mini-nav">
			<ul id="carousel-master">
				<li><a href="/argan-oil/glide-on"><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on.png" class="norm" /><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/body-wax"><img src="/images/main/hub/carousels/salon-divine-body-wax.png" class="norm" /><img src="/images/main/hub/carousels/salon-divine-body-wax-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/bikini-brush-on"><img src="/images/main/hub/carousels/bikini-brush-on-cream.png" class="norm" /><img src="/images/main/hub/carousels/bikini-brush-on-cream-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/power-cream"><img src="/images/main/hub/carousels/shower-power-cream.png" class="norm" /><img src="/images/main/hub/carousels/shower-power-cream-blur.png" class="blur"/></a></li>
				<li><a href="/japanese-cherry-blossom/body-wax-strips"><img src="/images/main/hub/carousels/jcb-body-wax-strips.png" class="norm" /><img src="/images/main/hub/carousels/jcb-body-wax-strips-blur.png" class="blur"/></a></li>
				<li><a href="/nair-collection/triple-action-cream"><img src="/images/main/hub/carousels/triple-action-cream.png" class="norm" /><img src="/images/main/hub/carousels/triple-action-cream-blur.png" class="blur"/></a></li>
				<li><a href="/nair-collection/triple-action-balm"><img src="/images/main/hub/carousels/triple-action-balm.png" class="norm" /><img src="/images/main/hub/carousels/triple-action-balm-blur.png" class="blur"/></a></li>
			</ul>
			
			<div id="bLeft">
				<img src="/images/main/hub/arrow-left.png" width="16" height="17"/>
			</div>
			
			<div id="bRight">
				<img src="/images/main/hub/arrow-right.png" width="16" height="17"/>
			</div>
		</div>
		<div class="text-bottom-wrap">
			<div class="text-bottom">
				<p>click on the arrows to cycle through the products</p>
			</div>
		</div>
	</div>
</div>

@endsection

