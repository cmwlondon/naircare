@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/hub/bg-cream.jpg" />
	</div>
	<div id="right-content">

		<div id="copy">
			<h2>Take care with <strong>cream</strong></h2>
			<p>Want hair-free skin in minutes? Then Nair creams are the perfect solution.</p>
			<p>They're the fuss-free way to leave skin silky smooth and looking gorgeous. From upper lip to bikini area, there's a Nair cream for every need.</p>
		</div>

		<div id="mini-nav">
			<ul id="carousel-master">
				<li><a href="/argan-oil/facial-brush-on"><img src="/images/main/hub/carousels/facial-brush-on-cream.png" class="norm" /><img src="/images/main/hub/carousels/facial-brush-on-cream-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/glide-on"><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on.png" class="norm" /><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/bikini-brush-on"><img src="/images/main/hub/carousels/bikini-brush-on-cream.png" class="norm" /><img src="/images/main/hub/carousels/bikini-brush-on-cream-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/power-cream"><img src="/images/main/hub/carousels/shower-power-cream.png" class="norm" /><img src="/images/main/hub/carousels/shower-power-cream-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/upper-lip-kit"><img src="/images/main/hub/carousels/upper-lip-kit.png" class="norm" /><img src="/images/main/hub/carousels/upper-lip-kit-blur.png" class="blur"/></a></li>
				<li><a href="/nair-collection/triple-action-balm"><img src="/images/main/hub/carousels/triple-action-balm.png" class="norm" /><img src="/images/main/hub/carousels/triple-action-balm-blur.png" class="blur"/></a></li>
			</ul>

			<div id="bLeft">
				<img src="/images/main/hub/arrow-left.png" width="16" height="17"/>
			</div>

			<div id="bRight">
				<img src="/images/main/hub/arrow-right.png" width="16" height="17"/>
			</div>
		</div>
		<div class="text-bottom-wrap">
			<div class="text-bottom">
				<p>click on the arrows to cycle through the products</p>
			</div>
		</div>		
	</div>
</div>
@endsection

