@extends('Main.Layouts.main')

@section('content')
<div id="container">

	<div id="main-image">
		<img src="/images/main/hub/bg-extras.jpg" />
	</div>
	<div id="right-content">

		<div id="copy">
			<h2>Take care of <strong>everywhere</strong></h2>
			<p>Everyone has hair in places they'd prefer not to, but silky smooth skin can be just minutes away.</p>
			<p>Nair sensitive creams and soothing waxes help you to lose unwanted hair quickly and effectively. And with a product for every need, there's no excuse not to feel silky smooth all over.</p>
		</div>
		
		<div id="mini-nav">
			<ul id="carousel-master">
				<li><a href="/argan-oil/body-wax"><img src="/images/main/hub/carousels/salon-divine-body-wax.png" class="norm" /><img src="/images/main/hub/carousels/salon-divine-body-wax-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/glide-on"><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on.png" class="norm" /><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/power-cream"><img src="/images/main/hub/carousels/shower-power-cream.png" class="norm" /><img src="/images/main/hub/carousels/shower-power-cream-blur.png" class="blur"/></a></li>
				<li><a href="/nair-collection/triple-action-balm"><img src="/images/main/hub/carousels/triple-action-balm.png" class="norm" /><img src="/images/main/hub/carousels/triple-action-balm-blur.png" class="blur"/></a></li>
			</ul>

			<div id="bLeft">
				<img src="/images/main/hub/arrow-left.png" width="16" height="17"/>
			</div>

			<div id="bRight">
				<img src="/images/main/hub/arrow-right.png" width="16" height="17"/>
			</div>
		</div>
		<div class="text-bottom-wrap">
			<div class="text-bottom">
				<p>click on the arrows to cycle through the products</p>
			</div>
		</div>		
		
	</div>
</div>
@endsection
