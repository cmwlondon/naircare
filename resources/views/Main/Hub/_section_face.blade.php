@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/hub/bg-face.jpg" />
	</div>
	<div id="right-content">
		<div id="copy">
			<h2>Take care of your <strong>face</strong></h2>
			<p>Facial skin can be more sensitive than the rest of your body, so choosing the right product to address your needs is important.</p>
			<p>Whether you're a wax or cream user, Nair have developed sensitive products specifically for use on delicate facial skin, which effectively remove hair without fuss.</p>
		</div>

		<div id="mini-nav">
			<ul id="carousel-master">
				<li><a href="/argan-oil/facial-brush-on"><img src="/images/main/hub/carousels/facial-brush-on-cream.png" class="norm" /><img src="/images/main/hub/carousels/facial-brush-on-cream-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/upper-lip-kit"><img src="/images/main/hub/carousels/upper-lip-kit.png" class="norm" /><img src="/images/main/hub/carousels/upper-lip-kit-blur.png" class="blur"/></a></li>
				<li><a href="/japanese-cherry-blossom/facial-wax-strips"><img src="/images/main/hub/carousels/jcb-facial-wax-strips.png" class="norm" /><img src="/images/main/hub/carousels/jcb-facial-wax-strips-blur.png" class="blur"/></a></li>
			</ul>

			<div id="bLeft">
				<img src="/images/main/hub/arrow-left.png" width="16" height="17"/>
			</div>

			<div id="bRight">
				<img src="/images/main/hub/arrow-right.png" width="16" height="17"/>
			</div>
		</div>
		<div class="text-bottom-wrap">
			<div class="text-bottom">
				<p>click on the arrows to cycle through the products</p>
			</div>
		</div>		
	</div>
</div>
@endsection

