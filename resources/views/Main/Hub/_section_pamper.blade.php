@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/hub/bg-pampering.jpg" />
	</div>
	<div id="right-content">
		<div id="copy">
			<h2>Take care by <strong>pampering</strong></h2>
			<p>Hair removal doesn't have to be a chore.</p>
			<p>The new Argan Oil collection is designed to bring you the salon experience without having to leave the house.</p>
			<p>With pampering ingredients, the wax and cream formula's are kind to skin and leave it looking and feeling gorgeously smooth in minutes.</p>
		</div>
		<div id="mini-nav" style="text-align:center; margin-top:20px;">
			<a href="/argan-oil/body-wax"><img class="shift-x" src="/images/main/products/2017-products/argan-products.png" border="0" /></a>
		</div>
	</div>
</div>
@endsection

