@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/hub/bg-sensitive-skin.jpg" />
	</div>
	<div id="right-content">
		<div id="copy">
			<h2>Take care of <strong>sensitive skin</strong></h2>
			<p>Sensitive skin requires sensitive products that don't compromise on great results.</p>
			<p>That's why the makers of Nair created specific formulas that are dermatologically tested and enriched with natural ingredients so that you can remove hair with confidence.</p>
		</div>
		
		<div id="mini-nav">
			<ul id="carousel-master">
				<li><a href="/argan-oil/upper-lip-kit"><img src="/images/main/hub/carousels/upper-lip-kit.png" class="norm" /><img src="/images/main/hub/carousels/upper-lip-kit-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/glide-on"><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on.png" class="norm" /><img src="/images/main/hub/carousels/bikini-and-underarm-glide-on-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/facial-brush-on"><img src="/images/main/hub/carousels/facial-brush-on-cream.png" class="norm" /><img src="/images/main/hub/carousels/facial-brush-on-cream-blur.png" class="blur"/></a></li>
				<li><a href="/argan-oil/body-wax"><img src="/images/main/hub/carousels/salon-divine-body-wax.png" class="norm" /><img src="/images/main/hub/carousels/salon-divine-body-wax-blur.png" class="blur"/></a></li>
			</ul>
			
			<div id="bLeft">
				<img src="/images/main/hub/arrow-left.png" width="16" height="17"/>
			</div>
			
			<div id="bRight">
				<img src="/images/main/hub/arrow-right.png" width="16" height="17"/>
			</div>
		</div>
		<div class="text-bottom-wrap">
			<div class="text-bottom">
				<p>click on the arrows to cycle through the products</p>
			</div>
		</div>		
	</div>
</div>@endsection

