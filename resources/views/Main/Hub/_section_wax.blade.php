@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-image">
		<img src="/images/main/hub/bg-wax.jpg">
	</div>
	<div id="right-content">
		<div id="copy">
			<h2>Take care with <strong>wax</strong></h2>
			<p>If you're looking for longer lasting hair removal then wax is the answer.</p>
			<p>Every Nair wax kit contains soothing post-wax wipes that care for your skin, leaving it looking fabulous. Nair wax strips are quick and easy to use and even tackle short hairs (about 2mm) for results that last up to four weeks.</p>
		</div>

		<div id="mini-nav">
			<ul id="carousel-master">
				<li data-index="0" style="left: 163px; top: 126px; z-index: 126;"><a href="/argan-oil/body-wax"><img src="/images/main/hub/carousels/salon-divine-body-wax.png" class="norm" style="width: 230px; height: 230px; left: -115px; top: -115px;"><img src="/images/main/hub/carousels/salon-divine-body-wax-blur.png" class="blur" style="width: 230px; height: 230px; left: -115px; top: -115px; opacity: 0.08;"></a></li>
				<li data-index="1" style="left: 293px; top: 100px; z-index: 100;"><a href="/japanese-cherry-blossom/body-wax-strips"><img src="/images/main/hub/carousels/jcb-body-wax-strips.png" class="norm" style="width: 172.5px; height: 172.5px; left: -86.25px; top: -86.25px;"><img src="/images/main/hub/carousels/jcb-body-wax-strips-blur.png" class="blur" style="width: 172.5px; height: 172.5px; left: -86.25px; top: -86.25px; opacity: 0.6;"></a></li>
				<li data-index="2" style="left: 163px; top: 74px; z-index: 74;"><a href="/japanese-cherry-blossom/facial-wax-strips"><img src="/images/main/hub/carousels/jcb-facial-wax-strips.png" class="norm" style="width: 115px; height: 115px; left: -57.5px; top: -57.5px;"><img src="/images/main/hub/carousels/jcb-facial-wax-strips-blur.png" class="blur" style="width: 115px; height: 115px; left: -57.5px; top: -57.5px; opacity: 1.12;"></a></li>
				<li data-index="3" style="left: 33px; top: 100px; z-index: 100;"><a href="/nair-collection/triple-action-balm"><img src="/images/main/hub/carousels/triple-action-balm.png" class="norm" style="width: 172.5px; height: 172.5px; left: -86.25px; top: -86.25px;"><img src="/images/main/hub/carousels/triple-action-balm-blur.png" class="blur" style="width: 172.5px; height: 172.5px; left: -86.25px; top: -86.25px; opacity: 0.6;"></a></li>
			</ul>

			<div id="bLeft" class="">
				<img src="/images/main/hub/arrow-left.png" width="16" height="17">
			</div>

			<div id="bRight" class="">
				<img src="/images/main/hub/arrow-right.png" width="16" height="17">
			</div>
		</div>
		<div class="text-bottom-wrap">
			<div class="text-bottom">
				<p>click on the arrows to cycle through the products</p>
			</div>
		</div>		
	</div>
</div>
@endsection

