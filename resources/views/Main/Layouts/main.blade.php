<!doctype html>
<html class="no-js" lang="en">
	<head>
<!-- OneTrust Cookies Consent Notice start for naircare.co.uk -->
<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js"  type="text/javascript" charset="UTF-8" data-domain-script="43633b9a-5fdf-456a-b0e7-f68f4bf651bb" ></script>
<script type="text/javascript">
function OptanonWrapper() { }
</script>
<!-- OneTrust Cookies Consent Notice end for naircare.co.uk -->
		<title>{!! $meta['title'] !!}</title>
		<meta charset="utf-8">

		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0" />
		<meta name="description" content="{{ $meta['desc'] }}" />
		<meta name="keywords" content="nair,hair,removal,product,cream,wax,gel,face,arms,bikini,lip,legs" />
		<meta name="keyphrases" content="nair hair removal, bikini line wax, body wax, facial wax, sensitive hair removal cream" />
		<meta name="publisher" content="Redtangle Ltd" />
		<meta http-equiv="Content-Language" content="en-gb, English" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="robots" content="INDEX, FOLLOW" />
		<meta name="classification" content="GENERAL" />
		<meta name="distribution" content="global" />
		<!-- Chrome Frame -->
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<!-- For Facebook -->
		<meta property="og:title" content="Nair Hair Removal"/>
		<meta property="og:type" content="website"/>
		<meta property="og:url" content="/"/>
		<meta property="og:image" content="/images/fb-opengraph.png"/>
		<meta property="og:site_name" content="Nair Hair Removal"/>
		<meta property="fb:admins" content="518920895"/>
		<meta property="og:description" content="Naircare.co.uk is home to our innovative range of hair removal products that leave your skin feeling beautifully smooth all over."/>

		<meta name="p:domain_verify" content="8707cd40e656aef1d2696eb9539a0d7c"/>

		<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
		<!--[if lt IE 9]>
			<script src="/js/libs/html5/html5.js"></script>
		<![endif]-->

		<!--[if gte IE 9]>
	    <style type="text/css">
				.gradient {
					 filter: none;
				}
		</style>
		<![endif]-->

	
		<link href="/css/main/styles.css" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Quattrocento+Sans|Varela" rel="stylesheet" type="text/css" />
		<link href="/css/main/layouts_generic.css" rel="stylesheet" type="text/css" />
		<link href="/css/main/queries.css" rel="stylesheet" type="text/css" />
		<link href="/css/main/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />

	@if (isset($pageViewCSS) && $pageViewCSS != '')
		<link rel="stylesheet" href="/css/{{{$pageViewCSS}}}.css"/>
	@endif
	
		<!-- Le fav and touch icons -->
		<link rel="shortcut icon" href="/images/favicon.ico">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/apple-touch-icon-144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/apple-touch-icon-114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/apple-touch-icon-72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="/images/apple-touch-icon-57-precomposed.png">

		@include('Main.Layouts.partials._analytics')
</head>

	<body>
		{{-- @include('Main.Layouts.partials._cookie-notify') --}}

	<div id="cookie-shim">
		<div id="wrapper">
			<div id="side-nav-mask">
					@include('Main.Layouts.partials._side-nav')
			</div>

			<header>
				<div id="social">
					@include('Main.Layouts.partials._social')
				</div>
				<a href="/"><img src="/images/main/nair-logo.svg" width="102" height="42" alt="Nair Hair Removal" class="main-logo"/></a>
				<img src="/images/main/takes-care-of-hair.svg" alt="takes care of hair" title="takes care of hair" id="headerImage" />
			</header>
			<div class="menu-bars">
				@include('Main.Layouts.partials._main-menu')
			</div>

			<section>
				@yield('content')
			</section>

			<footer>
				<div class="line"></div>
				@include('Main.Layouts.partials._footer')
				@if ($class_section === 'bikini-brush-on' )
					<div class="footer-text" style="position:absolute; bottom:-10px; left">
						<p style="color:#f00; font-size:10px;">*76% out of 79 women agreed in consumer study. Individual results may vary.</p>
					</div>
				@endif

				@if ($class_section === 'body-wax' )
					<div class="footer-text" style="position:absolute; bottom:-10px; left">
						<p style="color:#f00; font-size:10px;">*83.6% of a consumer test of 73 women who tried and agreed the product ensured painless hair removal.</p>
					</div>
				@endif
			</footer>
			@include('Main.Layouts.partials._video-popup')
			
		</div>
	</div>

	@include('Main.Layouts.partials._js_shared')
	@if (isset($pageViewJS) && $pageViewJS != '')
		@include($pageViewJS)
	@endif

	</body>	
</html>
