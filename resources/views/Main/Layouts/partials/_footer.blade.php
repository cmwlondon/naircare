<ul>
	<!--<li class="fitem1"><a href="/site-map">Site Map</a></li>
	<li class="fbar1">|</li>-->
	<li class="fitem2"><a href="/terms-and-conditions">Terms &amp; Conditions</a></li>
	<li class="fbar2">|</li>
	<li class="fitem3"><a href="/privacy-policy">Privacy Policy</a></li>
	<li class="fbar2">|</li>
	<li class="fitem3"><a href="/cookie-notice">Cookie Notice</a></li>
	<li class="fbar3">|</li>
	<li class="fitem4"><a href="http://www.churchdwight.co.uk" target="_blank">Visit Church &amp; Dwight</a></li>
	<li class="fbar4">|</li>
</ul>
<!-- OneTrust Cookies Settings button start -->
<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
<!-- OneTrust Cookies Settings button end -->

<div class="copyright mr5">&copy; Copyright Church &amp; Dwight UK Ltd. 2012. All rights reserved.</div>
<div class="trademark">Nair is a registered trademark of Church &amp; Dwight Co., Inc</div>
