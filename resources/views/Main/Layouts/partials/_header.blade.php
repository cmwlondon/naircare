	<header>
    <strong class="logo">
    	<a href="/"><img src="/images/main/logo.jpg" alt="Welcome to Church &amp; Dwight UK Ltd" width="219"/></a>
        <span>Welcome to Church &amp; Dwight UK Ltd</span>
    </strong>
    <nav>
        <ul>
            <li>
				<a href="/about">About</a>
                <img src="/images/main/menu_arrow_down.png" class="sub-arrow" alt=""/>
                <ul class="about">
                	<li><a href="/about">About Us</a></li>
                    <li><a href="/about/in-the-uk">In The UK</a></li>
                    <li><a href="/about/globally">Globally</a></li>
                </ul>
			</li>
            <li><a href="/brands">Our Brands</a>
            <img src="/images/main/menu_arrow_down.png" class="sub-arrow brands" alt=""/>
            <ul class="brands">
                    <li><a href="http://armandhammer.co.uk/" target="_blank">Arm &amp; Hammer</a></li>
                    <li><a href="http://www.batistehair.co.uk/" target="_blank">Batiste</a></li>
                    <li><a href="http://femfresh.co.uk/" target="_blank">femfresh</a></li>
                    <li><a href="http://www.firstresponsefertility.com/" target="_blank">First Response</a></li>
                    <li><a href="/" target="_blank">Nair</a></li>
                    <li><a href="http://orajel.co.uk/" target="_blank">Orajel</a></li>
                    <li><a href="http://pearldrops.co.uk/" target="_blank">Pearl Drops</a></li>
                    <li><a href="http://www.sterimarnasal.co.uk/" target="_blank">St&eacute;rimar</a></li>
                    <li><a href="http://www.replens.co.uk/" target="_blank">Replens</a></li>
                </ul>
            </li>
            <li>
				<a href="/careers">Careers</a>
                <img src="/images/main/menu_arrow_down.png" class="sub-arrow careers" alt=""/>
                <ul class="careers">
                	<li><a href="/careers">Career development</a></li>
                    <li><a href="/careers/what-our-employees-say">What our employees say</a></li>
                    <li><a href="/careers/why-join-us">Why join us?</a></li>
                    <li><a href="/careers/vacancies">Vacancies</a></li>
                </ul>
            </li>
            <li>
				<a href="/exports">Export</a>
                <img src="/images/main/menu_arrow_down.png" class="sub-arrow export" alt=""/>
                <ul class="export">
                	<li><a href="/exports">Export</a></li>
                    <li><a href="/exports/become-a-church-dwight-partner">Become a partner</a></li>
                    <li><a href="/exports/list-of-export-brands">List of export brands</a></li>
                </ul>
			</li>
            <li>
				<a href="/contact">Contact Us</a>
                <img src="/images/main/menu_arrow_down.png" class="sub-arrow contact" alt=""/>
                <ul class="contact">
                	<li><a href="/contact">Contact Us</a></li>
                    <li><a href="/faqs/general/1">FAQ's</a></li>
                    <li><a href="/contact/enquiry">Enquiry Form</a></li>
                </ul>
			</li>
        </ul>
    </nav>
    <aside class="search">
    	<form action="/search" method="post" accept-charset="utf-8">
            {{ csrf_field() }}
            <input type="text" name="search_criteria" value="" placeholder="Search" class="search-input"  />
            <button name="submit" type="submit" ></button>
        </form>
    </aside>
</header>