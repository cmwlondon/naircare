<script>
	// Global Vars defined from PHP
	var siteurl = '{{ $site_url }}';
	var mobile = '';
	var agent = '';
	var isHome = true;
</script>
<script src="/js/libs/jquery/jquery-min.js"></script>
<script src="/js/libs/jplayer/jquery.jplayer.min.js"></script>
<script src="/js/app-shared.js"></script>
<script src="/js/libs/redtangle/cookies.js"></script>
