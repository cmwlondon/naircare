<menu class="main-menu menu-grp1">
	<li class="b1"><a href="#" class="l1 bExpand" data-id="1">Japanese Cherry Blossom</a>
		<menu class="sub-menu">
			<li class="heading">Wax</li>
			<li><a href="/japanese-cherry-blossom/body-wax-strips" class="l2">Body Wax Strips</a></li>
			<li><a href="/japanese-cherry-blossom/facial-wax-strips" class="l2">Facial Wax Strips</a></li>
		</menu>
	</li>
	<li class="b2"><a href="#" class="l1 bExpand" data-id="2">Argan Oil</a>
		<menu class="sub-menu">
			<li class="heading">Wax</li>
			<li><a href="/argan-oil/body-wax" class="l2">Salon Divine Body Wax</a></li>

			<li class="heading">Cream</li>
			<li><a href="/argan-oil/upper-lip-kit" class="l2">Upper Lip Kit</a></li>
			<li><a href="/argan-oil/facial-brush-on" class="l2">Facial Brush-On</a></li>
			<li><a href="/argan-oil/glide-on" class="l2">Bikini and Underarm Glide-On</a></li>
			<li><a href="/argan-oil/bikini-brush-on" class="l2">Bikini Brush-On</a></li>
			<li><a href="/argan-oil/power-cream" class="l2">Shower Power Cream</a></li>
		</menu>
	</li>
	<li class="b3"><a href="#" class="l1 bExpand" data-id="3">Gentle Creams &amp; Balms</a>
		<menu class="sub-menu">
			<li class="heading">Hair Removal</li>
			<li><a href="/nair-collection/sensitive" class="l2">Sensitive</a></li>
			<li><a href="/nair-collection/tough-hair" class="l2">Tough Hair</a></li>
			<li class="heading">Shave</li>
			<li><a href="/nair-collection/triple-action-cream" class="l2">Triple Action Cream</a></li>
			<li class="heading">Balm</li>
			<li><a href="/nair-collection/triple-action-balm" class="l2">Triple Action Balm</a></li>
		</menu>
	</li>
</menu>
<menu class="main-menu menu-grp2">
	<li class="b4"><a href="/online-consultation" class="l1">Online Consultation</a></li>
	<li class="b5"><a href="/effective-hair-removal" class="l1">Effective Hair Removal</a></li>
	<li class="b6"><a href="/faqs" class="l1">FAQs</a></li>
	<li class="b7"><a href="/contact-us" class="l1">Contact Us</a></li>
</menu>
