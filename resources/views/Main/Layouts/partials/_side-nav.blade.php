<menu id="side-nav">
<img src="/images/main/side-nav-background.png" alt="" width="178" height="461" border="0"/>
<div id="bSideNav"></div>
<div class="sn-arr-up"></div>
<div class="sn-mask">
	<ul class="sn-list">
		<li><a href="/hub/wax"><img src="/images/main/side-nav/thumbnail-wax.jpg" alt="Wax"/><span>Wax</span></a></li>
		<li><a href="/hub/cream"><img src="/images/main/side-nav/thumbnail-cream.jpg" alt="Cream"/><span>Cream</span></a></li>
		<li><a href="/hub/face"><img src="/images/main/side-nav/thumbnail-face.jpg" alt="Face"/><span>Face</span></a></li>
		<li><a href="/hub/underarm"><img src="/images/main/side-nav/thumbnail-underarm.jpg" alt="Underarm"/><span>Underarm</span></a></li>
		<li><a href="/hub/bikini"><img src="/images/main/side-nav/thumbnail-bikini.jpg" alt="Bikini"/><span>Bikini</span></a></li>
		<li><a href="/hub/legs"><img src="/images/main/side-nav/thumbnail-legs.jpg" alt="Legs"/><span>Legs</span></a></li>
		<li><a href="/hub/everywhere"><img src="/images/main/side-nav/thumbnail-extras.jpg" alt="Extras"/><span>Everywhere</span></a></li>
		<li><a href="/hub/pamper"><img src="/images/main/side-nav/thumbnail-pamper.jpg" alt="Pamper"/><span>Pamper</span></a></li>
		<li><a href="/hub/sensitive-skin"><img src="/images/main/side-nav/thumbnail-sensitive.jpg" alt="Sensitive"/><span>Sensitive</span></a></li>
		<!--<li><a href="/hub/brazilian-spa"><img src="/images/main/side-nav/thumbnail-brazilian.jpg" alt="Brazilian Spa"/><span>Brazilian</span></a></li>//-->
	</ul>
</div>
<div class="sn-arr-down"></div>
</menu>