@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-info" class="scroll legal">
		<h2>Cookie Notice</h2>

		<p>We have outlined below the types of cookie that are used on this Site.</p>

{{--
		<h3>Cookies</h3>
<p>Cookies are small text files that are placed on your computer by a website in order to make websites work, or work more efficiently, as well as to provide information to the owners of the site. Cookies help us to run the Site and improve its interactivity and our services.</p>


		<h3>Strictly necessary cookies</h3>
		<p>We use first-party cookies to help the Site work in the best possible manner. You may opt out of receiving these cookies and delete them using your internet browser settings. However, your user experience may be impacted.</p>

		<table cellpadding="0" cellspacing="0" border="1" id="cookie-table" class="mt1">
			<tbody>
				<tr>
					<th width="120">Name</th>
					<th>Purpose</th>
					<th width="120">Retention</th>
				</tr>
				<!-- <tr>
					<td>__atuvc</td>
					<td>This cookie is associated with the AddThis social sharing widget which is commonly embedded in websites to enable visitors to share content with a range of networking and sharing platforms. It stores an updated page share count.</td>
					<td>1 year and 1 month</td>
				</tr>
				<tr>
					<td>__atuvs</td>
					<td>This cookie is associated with the AddThis social sharing widget which is commonly embedded in websites to enable visitors to share content with a range of networking and sharing platforms. This is believed to be a new cookie from AddThis which is not yet documented, but has been categorised on the assumption it serves a similar purpose to other cookies set by the service.</td>
					<td>30 minutes</td>
				</tr>
				<tr>
					<td>__utma</td>
					<td>This is one of the four main cookies set by the Google Analytics service which enables website owners to track visitor behaviour and measure site performance. This cookie lasts for 2 years by default and distinguishes between users and sessions. It it used to calculate new and returning visitor statistics. The cookie is updated every time data is sent to Google Analytics. The lifespan of the cookie can be customised by website owners.</td>
					<td>2 years</td>
				</tr>
				<tr>
					<td>__utmb</td>
					<td>This is one of the four main cookies set by the Google Analytics service which enables website owners to track visitor behaviour and measure site performance. This cookie determines new sessions and visits and expires after 30 minutes. The cookie is updated every time data is sent to Google Analytics. Any activity by a user within the 30 minute life span will count as a single visit, even if the user leaves and then returns to the site. A return after 30 minutes will count as a new visit, but a returning visitor.</td>
					<td>30 minutes</td>
				</tr>
				<tr>
					<td>__utmc</td>
					<td>This is one of the four main cookies set by the Google Analytics service which enables website owners to track visitor behaviour and measure site performance. It is not used in most sites but is set to enable interoperability with the older version of Google Analytics code known as Urchin. In this older versions this was used in combination with the __utmb cookie to identify new sessions/visits for returning visitors. When used by Google Analytics this is always a Session cookie which is destroyed when the user closes their browser. Where it is seen as a Persistent cookie it is therefore likely to be a different technology setting the cookie.</td>
					<td>Until exit</td>
				</tr>
				<tr>
					<td>__utmt</td>
					<td>This cookie is set by Google Analytics. According to their documentation it is used to throttle the request rate for the service - limiting the collection of data on high traffic sites. It expires after 10 minutes</td>
					<td>10 minutes</td>
				</tr>
				<tr>
					<td>__utmz</td>
					<td>This is one of the four main cookies set by the Google Analytics service which enables website owners to track visitor behaviour measure of site performance. This cookie identifies the source of traffic to the site - so Google Analytics can tell site owners where visitors came from when arriving on the site. The cookie has a life span of 6 months and is updated every time data is sent to Google Analytics.</td>
					<td>6 months</td>
				</tr> -->
				<tr>
					<td>ci_session</td>
					<td>Cookie associated with the CodeIgniter framework for building PHP based applications. Usually used to maintain a user state during a browser session for consistancy of user experience. By default the cookie is destroyed when the browser session ends.</td>
					<td>2 hours</td>
				</tr>
			</tbody>
		</table>

		<h3>Analytics cookies</h3>
		<p>We use Google Analytics cookies to analyse how this Site is used. Google analytics cookies monitor how visitors navigate the Site and how they found their way here. We use these so that we can see the total amount of visitors to the Site; this does not offer individual information. It also allows us to see what content our users enjoy most which helps us to provide you with the best service. You may at any time change your cookie settings to accept or delete these cookies by <a href="https://tools.google.com/dlpage/gaoptout" target="_blank" style="text-decoration:underline;">clicking here</a>.</p>

		<!-- <p>We use a Facebook Widget within the homepage of the website, this utilizes a range of cookies to know if youâ€™re logged into Facebook in the background, allowing you to LIKE and share posts.</p> -->
--}}
<!-- OneTrust Cookies List start -->
<div id="ot-sdk-cookie-policy">Cookie Notice</div>
<!-- OneTrust Cookies List end -->


</div>
</div>

@endsection
