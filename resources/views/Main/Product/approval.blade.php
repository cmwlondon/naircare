@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-info" class="scroll">
		<h1>Approve review</h1>
		<p>Name: {{ $review['r_name'] }}</p>
		<p>Product: {{ $review['r_product'] }}</p>
		<p>Date: {{ date('D, d M Y H:i:s',$review['r_when']) }}</p>
		<p>Rating: {{ $review['r_rating'] }}</p>
		<p>Comment: <br>{!! $review['r_comment'] !!}</p>
		<p>@if ($review['r_active'] === 1) Approved @else Not Approved @endif</p>
	</div>
</div>
@endsection
