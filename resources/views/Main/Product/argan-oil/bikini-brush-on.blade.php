@extends('Main.Layouts.main')

@section('content')
				<div id="container">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p><strong>PLEASE READ THE INSTRUCTION LEAFLET CAREFULLY BEFORE USE. FOLLOW EXACTLY.</strong></p>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="1">Precautions</h3>
					<div class="instructionText" id="i-1">
						<p>Follow the instructions.<br/>
							Contains thioglycolate. Contains alkali. Avoid contact with eyes.<br/>
							In the event of contact with eyes, rinse immediately with plenty of water and seek medical advice.<br/>
							Can be used anywhere except around eyes, in nose, ears or on breast nipples, perianal or vaginal/genital areas.<br/>
							Avoid contact with other materials as this could lead to damage or discolouration.<br/>
							Do not use on sunburned irritated, inflamed or broken skin or on weak scars.<br/>
							If you feel burning or stinging, remove product immediately and rinse well with water.<br/>
							Store at room temperature. If stored at low temperature, crystals may form.<br/>
							Keep out of reach of children</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="2">Application</h3>
					<div class="instructionText" id="i-2">
						<p>1. Gently brush on the cream to completely cover the area of unwanted hair with a thick, even layer, covering the hair to be removed. Do not rub in.<br/>
							2. After 5-6 minutes, test a small area. If the hair does not come away from the test area easily, leave on for a few minutes longer. Do not leave for more than 15 minutes in total.<br/>
							3. Shower off with lukewarm water using a wet cloth if required and pat skin dry with a soft towel. Do not rub or use soap.</p>
					</div>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p class="mt10">Aqua, Paraffinum Liquidum, Calcium Thioglycolate, Cetearyl Alcohol, Calcium Hydroxide, Sodium, Hydroxide, Ceteth-20, Silica, Synthetic Beeswax, Parfum, Argania Spinosa Kernel Oil, Camellia, Oleifera Seed Oil, Propylene Glycol, Benzyl Benzoate, Limonene, Butylphenyl Methylpropional, Linalool, Citronellol, Alpha-Isomethyl Ionone, Cananga Odorata Extract, Methylparaben, Propylparaben, Tocopherol</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Bikini Brush-On</h1>
			<p><span>Gentle &amp; Effective&hellip;</span> with Argan Oil and a <i>Moisture</i>BOOST Complex for beautiful, moisturised and smooth skin.</p>
			<p><span>Removes&hellip;</span> all visible hairs to stay silky smooth for up to 7 days.</p>
			<p><span>Quick &amp; Easy&hellip;</span> The brush on applicator allows fine precision. Simply brush on and rinse off for effective hair removal in minutes.</p>
			<p><span>Sensitive to skin&hellip;</span> specially formulated to be kind on your intimate skin.</p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="alt-placing-1" src="/images/main/products/2017-products/bikini-brush-on-cream.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/argan-bikini-brush-on.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions" class="btn">Instructions</a>
					<a href="http://www.boots.com/en/Nair-Ultra-Precision-Bikini-Brush-On-Hair-Removal-Cream-60ml_1440734/" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/nair-collection/sensitive"><img class="alt-height-1" src="/images/main/products/suggested/nair-sensitive.jpg" alt="Sensitive Cream" title="Sensitive Cream"/></a>
			<a href="/argan-oil/power-cream"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/shower-power-cream-suggested.jpg" alt="Shower Power Cream" title="Shower Power Cream"/></a>
			<a href="/argan-oil/glide-on"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/bikini-and-underarm-glide-on-suggested.jpg" alt="Bikini and Underarm Glide-On" title="Bikini and Underarm Glide-On"/></a>

		</div>
	</div>

</div>

@endsection