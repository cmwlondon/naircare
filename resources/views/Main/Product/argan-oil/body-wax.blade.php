@extends('Main.Layouts.main')

@section('content')
<div id="container">
<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p><strong>PLEASE READ THE INSTRUCTION LEAFLET CAREFULLY BEFORE USE. FOLLOW EXACTLY.</strong></p>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="1">Precautions</h3>
					<div class="instructionText" id="i-1">
						<p>Please refer to the leaflet in the product and follow the heating/mixing instructions exactly as indicated to avoid any risk of burning.<br/>
							CAUTION: PATCH TEST BEFORE EACH USE BY APPLYING THE PRODUCT TO A SMALL PART OF THE AREA WHERE HAIR IS TO BE REMOVED. FOLLOW DIRECTIONS AND WAIT 24 HOURS.<br/>Keep out of reach of children. Adolescents should be monitored by adults when using this product.<br/>
							Wax should not be used by people suffering from diabetes or circulatory problems or on areas with varicose veins, moles or warts.<br/>
							Not recommended for elderly people or people with loose skin.<br/>
							Do not apply wax over sunburned, chapped, broken or core skin, cuts, weak scars or eczema.<br/>
							Rinse thoroughly with warm, soapy water after each hair removal session.</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="2">Heating</h3>
					<div class="instructionText" id="i-2">
						<p>Heat only in a microwave oven.<br/>
							Heating times for 400g tub based on a 900W microwave. Full tub of wax: Maximum time 105 seconds, sitting time 3 minutes. Half tub of wax: maximum time 120 seconds, sitting time 3 minutes.<br/>
							Note microwaves can vary - please check the manufacturer's instructions to confirm wattage of unit being used.<br/>
							Check the temperature: once the ideal consistency has been obtained, test a small quantity of the wax on the inside of your wrist to check that the temperature is comfortable before proceeding, (the wax should feel warm, not hot).</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="3">Application</h3>
					<div class="instructionText" id="i-3">
						<p>If the temperature is acceptable, use the applicator to spread a thin, even layer of wax in the direction of hair growth.<br/>
							Leave the wax thicker at the end to assist with removal. Do not spread the wax back and forth as it will have started to set.<br/>At the end of the strip create a roll up with your fingers so that you have a free end to make it easier to remove.<br/>
							Remove: when the wax has set completely, about 20 seconds, lift up the lower end while holding the skin taut and pull back the strip in one quick movement, in the opposite direction to application and as close as possible to the treated skin. Do not leave the wax on the skin for longer than 1 minute.</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="4">After Care</h3>
					<div class="instructionText" id="i-4">
						<p>Complete hair removal by going over the skin with aftercare wipe provided.<br/>
							It is normal for thre area to appear a little red after waxing and will go away on its own after a couple of hours.<br/>
							Wait 24 hours before using deodorant, perfume or lotions after waxing to avoid irritation.</p>
					</div>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p class="mt10">Wax: Glyceryl Rosinate, Triethylene Glycol Rosinate, Hydrogenated Butylene / Ethylene / Styrene Copolymer, Paraffin, Cera Microcristallina, Zinc Oxide, Mica, Parfum, BHT, Caprylic / Capric Triglyceride, Citrus Aurantium Amara Flower Extract, Argania Spinosa Kernel Oil, Butylphenyl Methylpropional, Benzyl Salicylate, Citronellol, Hexyl Cinnamal, Limonene, Linalool, Eugenol, CI 77891.</p>
				<p class="mt10">Aftercare wipes: Paraffinum Liquidum, Ethylhexyl Sterate, Cetearyl Ethylhexanoate, Isopropyl Myristate, Phenethyl Alcohol, Bisabol, Glycine Soja Oil, Caprylyl Glycol, Parfum, Benzyl Salicylate, Farnesol, Butylphenyl Methylpropional, Hydroxyisohexyl 3-Cyclohexene Carboxaldehyde, Linalool, Citronellol, Alpha-Isomethyl Ionone, Eugenol, Chamomilla Recutita Flower Extract, BHT, Tocopherol.</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Salon Divine Body Wax</h1>
			<p><span>Smooth on and peel off&hellip;</span> contains natural Argan Oil and a pro-sensitive complex, leaving your skin soft and irresistibly smooth for up to 4 weeks. Suitable for sensitive skin.</p>
			<p><span>Removes the shortest of hairs&hellip;</span> (even 1-2mm) so you&rsquo;ll never have to wait too long for hair to regrow a certain length.</p>
			<p><span>A painless waxing experience&hellip;</span> The new unique &ldquo;Bye Bye Pain&rdquo; formula makes the process of waxing more pleasurable. 83.6% of people agree it removes hair painlessly.*</p>
			<p><span>Quick and easy to use&hellip;</span> Just heat in the microwave for 3 minutes. No Strips. No Mess. No Fuss. Professional results delivered in the comfort of your own home.</p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="shift-left" src="/images/main/products/2017-products/salon-divine-body-wax.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/argan-body-wax.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="http://www.boots.com/nair-argan-oil-salon-divine-body-wax-bye-bye-pain-formula" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>
	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/japanese-cherry-blossom/body-wax-strips"><img src="/images/main/products/suggested/jcb-body-wax-strips.jpg" alt="7 in 1 Body Wax Strips" title="7 in 1 Body Wax Strips"/></a>
			<a href="/nair-collection/triple-action-balm"><img src="/images/main/products/2017-products/suggested/triple-action-balm-suggested.jpg" alt="7 in 1 Body Wax Strips" title="7 in 1 Body Wax Strips" width="50px"/></a>
			<!-- <a href="/argan-oil/roll-on-wax"><img src="/images/main/products/suggested/argan-roll-on-wax.jpg" alt="Washable Roll-On Wax" title="Washable Roll-On Wax"/></a> -->
		</div>
	</div>

</div>

@endsection