@extends('Main.Layouts.main')

@section('content')
				<div id="container">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p><strong>PLEASE READ THE INSTRUCTION LEAFLET CAREFULLY BEFORE USE. FOLLOW EXACTLY.</strong></p>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="1">Precautions</h3>
					<div class="instructionText" id="i-1">
						<p>CAUTION: FOLLOW THE INSTRUCTIONS. CONTAINS THIOGLYCOLATE. CONTAINS ALKALI. KEEP OUT OF REACH OF CHILDREN. AVOID CONTACT WITH EYES. IN THE EVENT OF CONTACT WITH EYES, RINSE IMMEDIATELY WITH PLENTY OF WATER AND SEEK MEDICAL ADVICE. HAIR REMOVERS CAN BE USED ANYWHERE EXCEPT AROUND EYES, IN NOSE, EARS OR ON BREAST NIPPLES, PERIANAL OR VAGINAL/GENITAL AREAS. AVOID CONTACT WITH OTHER MATERIALS AS THIS COULD LEAD TO DAMAGE OR DISCOLOURATION. DO NOT USE ON IRRITATED, INFLAMED OR BROKEN SKIN. IF YOU FEEL BURNING OR STINGING, REMOVE PRODUCT IMMEDIATELY AND RINSE WELL WITH WATER. STORE AT ROOM TEMPERATURE. IF STORED AT A LOW TEMPERATURE, CRYSTALS MAY FORM.</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="2">Preparation</h3>
					<div class="instructionText" id="i-2">
						<p>Remove all make up and other creams or lotions from your face.</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="3">Application</h3>
					<div class="instructionText" id="i-3">
						<p>Gently brush on the cream to completely cover the area of unwanted hair with a thick, even layer. Do not rub in.<br/>
							Replace the cap after use.<br/>
							After 3 minutes, test a small area. If the hair does not wipe off easily, leave on for 1-2 minutes longer.<br/>
							Do not leave for more than 5 minutes in total.<br/>
							Rinse off thoroughly with lukewarm water, using a wet flannel if required and pat the face dry with a soft towel.<br/>
							Do not rub or use soap.</p>
					</div>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p class="mt10">Aqua, Paraffinum Liquidum, Calcium Thioglycolate, Cetearyl Alcohol, Calcium Hydroxide, Sodium, Hydroxide, Ceteth-20, Silica, Synthetic Beeswax, Parfum, Argania Spinosa Kernel Oil, Camellia, Oleifera Seed Oil, Propylene Glycol, Benzyl Benzoate, Limonene, Butylphenyl Methylpropional, Linalool, Citronellol, Alpha-Isomethyl Ionone, Cananga Odorata Extract, Methylparaben, Propylparaben, Tocopherol</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Facial Brush-On</h1>
			<p><span>Unique&hellip;</span> brush on applicator for precise and easy use. Simply brush on and rinse off, to unveil beautifully smooth skin.</p>
			<p><span>Top Tip&hellip;</span> for hair that is coarse or hard to remove, soak the hair with warm water for several minutes, pat dry and then apply cream.</p>
			<p><span>Works deep down&hellip;</span> to gently remove and nourish hair below the surface of your skin, leaving silky smooth skin and long lasting results.</p>
			<p><span>Cares&hellip;</span> contains Argan Oil to protect and moisturise. Suitable for dry and sensitive skin.</p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="alt-placing-1" src="/images/main/products/2017-products/facial-brush-on-cream.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/argan-facial-brush-on.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="http://www.boots.com/en/Nair-Sensitive-Formula-Facial-Brush-On-50ml_981306/" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/argan-oil/upper-lip-kit"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/upper-lip-kit-suggested.jpg" alt="Upper Lip Kit" title="Upper Lip Kit"/></a>
			<!-- <a href="/argan-oil/face-and-eyebrow-roll-on-wax"><img src="/images/main/products/suggested/argan-face-and-eyebrow-roll-on-wax.jpg" alt="Face and Eyebrow Roll-On Wax" title="Face and Eyebrow Roll-On Wax"/></a> -->
			<a href="/japanese-cherry-blossom/facial-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-face-3d-pack-suggested.jpg" alt="7 in 1 Facial Wax Strips" title="7 in 1 Facial Wax Strips"/></a>
		</div>
	</div>

</div>
@endsection