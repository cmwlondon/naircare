@extends('Main.Layouts.main')

@section('content')
				<div id="container">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p><strong>PLEASE READ THE INSTRUCTION LEAFLET CAREFULLY BEFORE USE. FOLLOW EXACTLY.</strong></p>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="1">Precautions</h3>
					<div class="instructionText" id="i-1">
						<p>FOLLOW THE INSTRUCTIONS ON CARTON. FOR EXTERNAL USE ONLY.<br/>
							CAUTION: PATCH TEST BEFORE EACH USE BY APPLYING THE PRODUCT TO A SMALL PART OF THE AREA WHERE HAIR IS TO BE REMOVED. FOLLOW DIRECTIONS AND WAIT 24 HOURS.<br/>
							CONTAINS THIOGLYCOLATE. CONTAINS ALKALI.<br/>
							KEEP OUT OF REACH OF CHILDREN.<br/>
							AVOID CONTACT WITH EYES. IN THE EVENT OF CONTACT WITH EYES, RINSE IMMEDIATELY WITH PLENTY OF WATER AND SEEK MEDICAL ADVICE. HAIR REMOVERS CAN BE USED ANYWHERE EXCEPT AROUND EYES, IN NOSE, EARS OR ON BREAST NIPPLES, PERIANAL OR VAGINAL/GENITAL AREAS. AVOID CONTACT WITH OTHER MATERIALS AS THIS COULD LEAD TO DAMAGE OR DISCOLOURATION. DO NOT USE ON IRRITATED, SUNBURNED, INFLAMED OR BROKEN SKIN. IF YOU FEEL BURNING OR STINGING, REMOVE PRODUCT IMMEDIATELY AND RINSE WELL WITH WATER. STORE AT ROOM TEMPERATURE. IF STORED AT A LOW TEMPERATURE, CRYSTALS MAY FORM.</p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="2">Application</h3>
					<div class="instructionText" id="i-2">
						<p>Turn dial until about 5mm of Nair is on the applicator. Smooth on using the top of the applicator, completely covering the area of unwanted with a thick, even layer. Do not rub in. Replace the cap after use.<br/>
							After 5-6 minutes, test a small area. If the hair does not come away from the test area easily, leave on for a few minutes longer. Do not leave for more than 10 minutes in total.<br/>
							Rinse off thoroughly with lukewarm water, using a wet flannel if required and pat the area dry with a soft towel.  Do not rub or use soap.</p>
					</div>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p class="mt10">Aqua, Paraffinum Liquidum, Calcium Thioglycolate, Cetearyl Alcohol, Calcium Hydroxide, Sodium Hydroxide, Ceteth-20, Silica, Synthetic Beeswax, Parfum, Argania Spinosa Kernel Oil, Camellia Oleifera Seed Oil, Propylene Glycol, Benzyl Benzoate, Limonene, Butylphenyl Methylpropional, Linalool, Citronellol, Alpha-Isomethyl Ionone, Cananga Odorata Extract, Methylparaben, Propylparaben, Tocopherol, CT 77491, CI 77492.</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Bikini and Underarm <span class="dontwrap">Glide-On</span></h1>
			<p><span>Simply&hellip;</span> twist up, smooth on and rinse off. Perfect for bikini, underarms and even suitable for legs.</p>
			<p><span>Quick, Clean Application&hellip;</span> The no-touch glide on applicator helps remove unwanted hair with less mess and fuss.</p>
			<p><span>Contains&hellip;</span> Argan Oil to moisturise and exfoliate your skin while it removes all visible hairs.</p>
			<p><span>Kind to Skin&hellip;</span> with a sensitive formula and <i>Moisture</i>BOOST Complex to gently care for dry and sensitive skin types. </p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="alt-placing-1" src="/images/main/products/2017-products/bikini-and-underarm-glide-on.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/argan-bikini-glide-on.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="https://www.tesco.com/groceries/en-GB/products/253629255" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/argan-oil/power-cream"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/shower-power-cream-suggested.jpg" alt="Shower Power Cream" title="Shower Power Cream"/></a>
			<!-- <a href="/argan-oil/roll-on-wax"><img src="/images/main/products/suggested/argan-roll-on-wax.jpg" alt="Washable Roll-On Wax" title="Washable Roll-On Wax"/></a> -->
			<a href="/nair-collection/sensitive"><img class="alt-height-1" src="/images/main/products/suggested/nair-sensitive.jpg" alt="Sensitive Cream" title="Sensitive Cream"/></a>
		</div>
	</div>

</div>

@endsection