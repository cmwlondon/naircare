@extends('Main.Layouts.main')

@section('content')
				<div id="container">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p><strong>Please read the instructions and all warnings and cautions before use. Follow exactly. Read the precautions on use carefully before applying.</strong></p>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="1">Quick</h3>
					<div class="instructionText" id="i-1">
						<ol class="mt10">
							<li>Ensure skin is clean and dry then using the spatula provided apply a thick, even layer of the cream. Wait for at least 1-2 minutes before entering the shower.</li>
							<li>In the shower, leave the cream on the skin for at least 3 minutes; try to avoid excessive exposure to water on the areas which are covered with the cream.</li>
							<li>After 3 minutes, test a small area with the spatula. If the hair does not come away from the test area easily, leave for a few minutes longer do not exceed 10 minutes in all.</li>
							<li>Remove the cream using the spatula, then rinse the area immediately with plenty of warm water to remove all traces of the cream. Do not rub or use soap.</li>
						</ol>
						<ul>
							<li>Store at room temperature. If stored at a low temperature, crystals may form.</li>
						</ul>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="2">Hazards and Cautions</h3>
					<div class="instructionText" id="i-2">
						<ul>
							<li>Irritation or allergic reaction may occur with some people, even after prior use without adverse effects.</li>
							<li>Therefore, a test should always be carried out on a small part of the area to be treated before each use.</li>
							<li>Follow the instructions and if the skin appears normal after 24 hours, proceed with the full application.</li>
							<li>Follow the instructions. Contains thioglycolate. Contains alkali.</li>
							<li>Avoid contact with eyes. In the event of contact with eyes, rinse immediately with plenty of water and seek medical advice.</li>
							<li>Can be used anywhere except around eyes, in nose, ears or on breast nipples, perianal or vaginal/genital areas.</li>
							<li>Avoid contact with other materials as this could lead to damage or discolouration.</li>
							<li>Do not use on irritated, sunburned, inflamed or broken skin or on weak scars.</li>
							<li>If you feel burning or stinging, remove the product immediately and rinse well with water.</li>
							<li>Keep out of reach of children.</li>
						</ul>
					</div>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p class="mt10">Aqua, Paraffinum Liquidum, Cetearyl Alcohol, Calcium Thioglycolate, Calcium Hydroxide, Ceteareth-20, Sodium Hydroxide, Parfum, Silica, Theobroma Cacao Butter, Argania Spinosa Oil, Simmondsia Chinensis Oil, Amyl Cinnamal, Geraniol, Alpha-Isomethyl Ionone, Butylphenyl Methylpropional</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Shower Power Cream</h1>
			<p><span>Works during showering&hellip;</span> for handy, quick and effective hair removal. Gentle enough to use on legs, bikini and underarms - even works on short hairs.</p>
			<p><span>Indulge&hellip;</span> your skin with a unique formula containing Argan Oil, Jojoba Oil and Cocoa Butter that helps to leave your skin feeling softer and smoother.</p>
			<p><span>Removes&hellip;</span> all visible hairs to stay silky smooth for up to 7 days.</p>
			<p><span>Cares&hellip;</span> for best results, it&rsquo;s recommended that you use Nair at least 24 hours before applying tanning products</p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="alt-placing-1" src="/images/main/products/2017-products/shower-power-cream.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/argan-shower-power-cream.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="https://www.superdrug.com/Nair/NAIR-ARGAN-OIL-SHOWER-POWER-SACHET-30MLX2/p/731271 " target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/argan-oil/glide-on"><img class="alt-height-1 no-right-margin" src="/images/main/products/2017-products/suggested/bikini-and-underarm-glide-on-suggested.jpg" alt="Bikini and Underarm Glide-On" title="Bikini and Underarm Glide-On"/></a>
			<a href="/argan-oil/body-wax"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/salon-divine-body-wax-suggested.jpg" alt="Salon Divine Body Wax" title="Salon Divine Body Wax"/></a>
			<!-- <a href="/argan-oil/face-and-eyebrow-roll-on-wax"><img src="/images/main/products/suggested/argan-face-and-eyebrow-roll-on-wax.jpg" alt="Face and Eyebrow Roll-On Wax" title="Face and Eyebrow Roll-On Wax"/></a> -->
		</div>
	</div>

</div>
@endsection