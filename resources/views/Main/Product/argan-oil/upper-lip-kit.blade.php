@extends('Main.Layouts.main')

@section('content')
				<div id="container">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p><strong>PLEASE READ THE INSTRUCTION LEAFLET CAREFULLY BEFORE USE. FOLLOW EXACTLY.</strong></p>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="1">Precautions</h3>
					<div class="instructionText" id="i-1">
						<p>FOLLOW THE INSTRUCTIONS ON CARTON. FOR EXTERNAL USE ONLY.<br/>
							CAUTION: PATCH TEST BEFORE EACH USE BY APPLYING THE PRODUCT TO A SMALL PART OF THE AREA WHERE HAIR IS TO BE REMOVED. FOLLOW DIRECTIONS AND WAIT 24 HOURS.<br/>
							CONTAINS THIOGLYCOLATE. CONTAINS ALKALI.<br/>
							KEEP OUT OF REACH OF CHILDREN.<br/>
							AVOID CONTACT WITH EYES. IN THE EVENT OF CONTACT WITH EYES, RINSE IMMEDIATELY WITH PLENTY OF WATER AND SEEK MEDICAL ADVICE. HAIR REMOVERS CAN BE USED ANYWHERE EXCEPT AROUND EYES, IN NOSE, EARS OR ON BREAST NIPPLES, PERIANAL OR VAGINAL/GENITAL AREAS. AVOID CONTACT WITH OTHER MATERIALS AS THIS COULD LEAD TO DAMAGE OR DISCOLOURATION. DO NOT USE ON IRRITATED, SUNBURNED, INFLAMED OR BROKEN SKIN. IF YOU FEEL BURNING OR STINGING, REMOVE PRODUCT IMMEDIATELY AND RINSE WELL WITH WATER. STORE AT ROOM TEMPERATURE. IF STORED AT A LOW TEMPERATURE, CRYSTALS MAY FORM. </p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="2">Application</h3>
					<div class="instructionText" id="i-2">
						<p>Ensure skin is clean and dry.<br/>
							Gently squeeze tube to dispense cream. With precision applicator, spread a thick, even layer to cover hair. DO NOT RUB IN.<br/>
							After 3  minutes, test a small area. If the hair does not come away from the test area easily, leave for a few minutes longer.<br/>
							Do not exceed 5 minutes in total.<br/>
							3 min Max. 5 min.<br/>
							Gently wipe off cream and hair with a damp washcloth. Do not rub or use soap.     </p>
					</div>
				<h3 class="mt10 mb10 instructionsExpand skyBlue" id="3">After care</h3>
					<div class="instructionText" id="i-3">
						<p>Once all hair has been removed,  RINSE SKIN THOROUGHLY with lukewarm water. Pat area dry with a soft towel.<br/>
							Complete hair removal by going over the skin with aftercare wipe provided.</p>
					</div>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p class="mt10">DEPILATORY<br/>
					Aqua, Paraffinum Liquidum, Calcium Thioglycolate, Cetearyl Alcohol, Calcium Hydroxide, Sodium Hydroxide, Ceteth-20, Silica, Synthetic Beeswax, Parfum, Argania Spinosa Kernel Oil, Camellia Oleifera Seed Oil, Propylene Glycol, Benzyl Benzoate, Limonene, Butylphenyl Methylpropional, Linalool, Citronellol, Alpha-Isomethyl Ionone, Cananga Odorata Extract, Methylparaben, Propylparaben, CT 77491, CI 77492.</p>
					<p class="mt10">MOISTURISER Ingredients: Aqua, Cetyl Alcohol, Isopropyl Palmitate, Stearyl Alcohol, Glyceryl Stearate SE, Ceteareth-20, Phenoxyethanol, Sodium Citrate, Imidazolidinyl Urea, Propylene Glycol, Sodium Methylparaben, Allantoin, Prunus Amygsalus Dulcis Oil, Tartaric Acid, Sodium Ethylparaben, Chamomilla Recutita Flower Extract, Sorbitol, Sodium Propylparaben, Methylparaben.</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Upper Lip Kit</h1>
			<p><span>Cares for skin&hellip;</span> contains natural Argan Oil and a <i>Moisture</i>BOOST Complex. Leaves skin beautiful, moisturised and smooth.</p>
			<p><span>Effective &amp; Gentle&hellip;</span> to delicate and sensitive skin, whilst removing unwanted facial hair for up to 7 days. </p>
			<p><span>Dual Action / Double Act&hellip;</span> Follow up with fragrance free moisteriser to penetrate skin deep down to leave it feeling  nourished.</p>
			<p><span>Repeat&hellip;</span> every 4 weeks to keep skin hair free and gorgeously soft.</p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="alt-placing-1" src="/images/main/products/2017-products/upper-lip-kit.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/argan-upper-lip-kit.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="http://www.boots.com/en/Nair-Upper-lip-kit-20ml_865526/" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/argan-oil/facial-brush-on"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/facial-brush-on-cream-suggested.jpg" alt="Facial Brush-On" title="Facial Brush-On"/></a>
			<!-- <a href="/argan-oil/face-and-eyebrow-roll-on-wax"><img src="/images/main/products/2017-products/suggested/argan-face-and-eyebrow-roll-on-wax.jpg" title="Face and Eyebrow Roll-On Wax" alt="Face and Eyebrow Roll-On Wax"/></a> -->
			<a href="/japanese-cherry-blossom/facial-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-face-3d-pack-suggested.jpg" alt="7 in 1 Facial Wax Strips" title="7 in 1 Facial Wax Strips"/></a>
		</div>
	</div>

</div>


@endsection