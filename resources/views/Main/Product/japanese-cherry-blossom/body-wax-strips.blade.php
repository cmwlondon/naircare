@extends('Main.Layouts.main')

@section('content')
<div id="container" class="jcb">
	<a name="instructions"></a>
	<a name="ingredients"></a>
	<a name="reviews"></a>
	<div id="overlay_master" class="masker hidden">
		<div id="overlay_panel">
			<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
			<div class="inner">
				<menu class="overlay-menu">
					<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
					<li>|</li>
					<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
					<li>|</li>
					<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
				</menu>
				<div class="closer"><a href="#" id="bClose">close</a></div>
				
				<!-- Instructions -->
				<div id="p_instructions" class="panel_content skyBlue hidden">
					<p class="mt10"><strong>Instructions</strong></p>
					<p>Irritation or allergic reaction may occur with some people, even after prior use without adverse effects. Therefore, a test should always be carried out on a small part of the area to be treated <strong>before each use</strong>. Follow the instructions and if the skin appears normal after 24 hours proceed with the full application.</p>
					<p><strong>Hair Length:</strong> Suitable for short hair, down to 2mm. Trim the hair before waxing if necessary.</p>
					<ol>
						<li>Wash the area to be waxed with mild soap and water (do not take a bath). Dry thoroughly. If the climate is hot and humid, lightly dust the area to be waxed with powder.</li>
						<li><strong>Slowly separate the strips</strong> (no need for prior rubbing to warm the wax*). Use one single strip at a time. Place the second single strip (folded in half with the wax on the inside) to one side while you are using the first single strip.</li>
						<li>Apply the wax strip by pressing it down over the hair to be removed in the <strong>direction of hair growth</strong> (on legs this means knees to toes) and make it adhere by pressing down firmly with the palm of the hand.</li>
						<li>Holding the skin taut, quickly pull off the strip in the <strong>opposite </strong>direction, parallel to the skin, not up and away from the body. Holding the skin taut during wax removal from the bikini area is very IMPORTANT to minimise bruising.</li>
						<li><strong>The same strip may be used several times</strong> until it no longer adheres to the skin.</li>
						<li>After waxing is complete, use a <strong>Nair </strong>Post-Wipe to help soothe and moisturise the skin after waxing to remove any wax residue. Avoid hot baths and sunbathing for at least 24 hours. After underarm waxing, do not use a deodorant or anti-perspirant for at least 24 hours.</li>
					</ol>
					<p>*Test performed at temperature equal to 20&deg;C and beyond.</p>

					<p class="mt10"><strong>Warning</strong></p>
					<p>Never reapply wax to the same area within a 24 hour period. If the skin to be treated is not held taut while the strip is removed, or the strip is not pulled back quickly and close to the skin, a pinching effect may be felt, a wax residue may remain on the skin or worse, skin could be removed, resulting in injury.</p>

					<p><strong>Caution</strong></p>
					<p><strong>Follow the instructions exactly. </strong>Wax should not be used by people suffering from diabetes or circulatory problems or on areas with varicose veins, moles or warts. Do not wax inside nose or ears, on nipples, perianal, vaginal/genital areas or eyelashes. Do not use on irritated, inflamed or broken skin. Not suitable for people with extra sensitive or problem skin. Not recommended for the elderly or people with loose skin. Do not apply wax over sunburned, chapped, broken or sore skin, cuts, weak scars or eczema. Do not use immediately after a bath. Keep away from heat, sparks and open flame. Do not use deodorant or anti-perspirant for 24 hours after underarm hair removal. Avoid contact with eyes. In the event of contact with eyes, rinse immediately with water and seek medical advice. Do not ingest. Failure to follow these warnings may result in severe skin irritation, skin removal or other injury. Proceed at all times with caution. For external use only. <strong>Keep out of reach of children.</strong></p>

				</div>
				
				<!-- Ingredients -->
				<div id="p_ingredients" class="panel_content skyBlue hidden">
					<p class="mt10"><strong>Ingredients</strong></p>
					<p class="mt10">WAX STRIPS: Triethylene Glycol Rosinate, Glyceryl Rosinate, Polyethylene, Parfum, Ricinus Communis Seed Oil, Pentaerythrityl Tetra-di-t-Butyl Hydroxyhydrocinnamate, Caprylic/Capric Triglyceride, Helianthus Annuus Seed Oil, Oryza Sative Bran Oil, Prunus Serrulata Flower Extract, Dendrobium Phalaenopsis Flower Extract, Tocopherol, Citronellol, Coumarin, Geraniol, Limonene, Linalool, CI 77891, CI 75470.</p>
					<p class="mt10">POST-WIPES: Paraffinum Liquidum, Ethylhexyl Stearate, Cetearyl Ethylhexanoate, Isopropyl Myristate, Phenethyl Alcohol, Caprylyl Glycol, Bisabolol, Glycine Soja Oil, Parfum, Benzyl Salicylate, Butylphenyl Methylpropional, Farnesol, Hydroxyisohexyl 3-Cyclohexene Carboxaldehyde, Linalool, Citronellol, Alpha-Isomethyl lonone, Eugenol, Chamomilla Recutita Flower Extract, BHT Tocopherol.</p>
				</div>
				
				<!-- Reviews -->
				<div id="p_reviews" class="panel_content hidden">
					@include('Main.Product.partials.product_reviews')
					@include('Main.Product.partials.product_review_form')
				</div>
			</div>
		</div>
	</div>
	<div class="copy">
		<div class="col1">
			<h1>7 in 1<br/>Body Wax Strips</h1>
			<p><span>No heat, no rub&hellip;</span> ready for use straight out of the box.</p>
			<p><span>Clean&hellip;</span> without any messy residue to clean up, your gorgeously smooth skin is ready to show off in minutes.</p>
			<p><span>Smooth&hellip;</span> skin that lasts up to 4 weeks.</p>
			<p><span>Effective&hellip;</span> for hair as short as 2mm, so you&rsquo;ll never have to wait too long for hair to regrow to a certain length.</p>
		</div>

		<div class="col2">
			<div class="packshot alt-mobile-height">
				<img class="alt-placing-1" src="/images/main/products/2017-products/nair-jcb-body-3d-pack.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/japanese-cherry-blossom-body-wax-strips.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="https://www.superdrug.com/Toiletries/Female-Hair-Removal/Waxing/Nair-Cherry-Blossom-Body-Wax-Strips-20/p/701804" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/argan-oil/body-wax"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/salon-divine-body-wax-suggested.jpg" alt="Salon Divine Body Wax" title="Salon Divine Body Wax"/></a>
			<a href="/japanese-cherry-blossom/facial-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-face-3d-pack-suggested.jpg" alt="Japanese Cherry Blossom Facial Wax Strips" title="Japanese Cherry Blossom Facial Wax Strips"/></a>
			<a href="/argan-oil/power-cream"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/shower-power-cream-suggested.jpg" alt="Shower Power Cream" title="Shower Power Cream"/></a>
		</div>
	</div>

</div>
@endsection

