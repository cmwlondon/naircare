@extends('Main.Layouts.main')

@section('content')
				<div id="container" class="sensitive">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p>Irritation or allergic reaction may occur with some people, even after prior use without adverse effects.&nbsp; Therefore, a test should always be carried out on a small part of the area to be treated <strong>before each use</strong>.&nbsp; Follow the instructions and if the skin appears normal after 24 hours, proceed with the full application.</p>
				<ol>
					<li>Ensure skin is clean and dry, then using the spatula provided apply a thick, even layer of the cream.</li>
					<li>After 5 to 6 minutes, test a small area with the spatula.&nbsp; If the hair does not come away from the test area easily, leave for a few minutes longer.&nbsp; <strong>Do not exceed 15 minutes in total.</strong></li>
					<li>Remove the cream using the spatula, then rinse the area immediately with plenty of warm water to remove all traces of the cream.&nbsp; <strong>Do not rub or use soap</strong>.</li>
				</ol>
				<p class="mt10"><strong>Cautions</strong></p>
				<p>Follow the instructions.&nbsp; Contains thioglycolate.&nbsp; Contains alkali.&nbsp; Avoid contact with eyes.&nbsp; In the event of contact with eyes, rinse immediately with plenty of water and seek medical advice.&nbsp; Can be used anywhere except around eyes, in nose, ears or on breast nipples, perianal or vaginal/genital areas.&nbsp; Avoid contact with other materials as this could lead to damage or discolouration.&nbsp; Do not use on irritated, sunburned, inflamed or broken skin or on weak scars.&nbsp; If you feel burning or stinging, remove the product immediately and rinse well with water.&nbsp; Store at room temperature.&nbsp; If stored at a low temperature, crystals may form.</p>
				<p><strong>Keep out of reach of children.</strong></p>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p>Aqua, Paraffinum Liquidum, Calcium Thioglycolate, Cetearyl Alcohol, Calcium Hydroxide, Sodium Hydroxide, Ceteth-20, Silica, Synthetic Beeswax, Parfum, Propylene Glycol, Cananga Odorata Flower Extract, Methylparaben, Propylparaben, Camellia Oleifera Seed Oil, Butylphenyl Methylpropional, Benzyl Benzoate, Limonene, Linalool, Alpha-Isomethyl Ionone, Citronellol, CI 77491, CI 77492.</p>	
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Sensitive</h1>
			<p><span>Specially&hellip;</span> formulated for dry and sensitive skin, it's the gentle but effective way to leave skin hair free in minutes.</p>
			<p><span>Enriched&hellip;</span> with moisturising Camellia Oil and a protecting Ylang Ylang extract, it nourishes and moisturises skin to leave it silky smooth and soft to touch.</p>
			<p><span>Top Tip&hellip;</span> the handy travel sachets are perfect to pop in your suitcase to keep you soft and smooth all holiday long.</p>
			<p><span>Cares&hellip;</span> gentle enough to use on legs, bikini and underarms.</p>
		</div>

		<div class="col2">
			<div class="packshot">
				<img src="/images/main/products/nair-sensitive.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/nair-sensitive.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="http://www.boots.com/en/Nair-Sensitive-Hair-Removal-Cream-200ml_1049445/" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<a href="/argan-oil/glide-on"><img class="alt-height-1 no-right-margin" src="/images/main/products/2017-products/suggested/bikini-and-underarm-glide-on-suggested.jpg" alt="Bikini and Underarm Glide-On" title="Bikini and Underarm Glide-On"/></a>
			<a href="/argan-oil/facial-brush-on"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/bikini-brush-on-cream-suggested.jpg" alt="Facial Brush-On" title="Facial Brush-On"/></a>
			<a href="/japanese-cherry-blossom/facial-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-face-3d-pack-suggested.jpg" alt="7 in 1 Facial Wax Strips" title="7 in 1 Facial Wax Strips"/></a>
		</div>
	</div>

</div>
@endsection