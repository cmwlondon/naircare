@extends('Main.Layouts.main')

@section('content')
				<div id="container" class="tough-hair">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>
			
			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p>Irritation or allergic reaction may occur with some people, even after prior use without adverse effects.&nbsp; Therefore, a test should always be carried out on a small part of the area to be treated <strong>before each use</strong>.&nbsp; Follow the instructions and if the skin appears normal after 24hours, proceed with the full application.</p>
				<ol>
					<li>Read the instructions carefully before first use.</li>
					<li>Ensure skin is clean and dry, then using the spatula provided apply a thick, even layer of the cream.</li>
					<li>After 5 to 6 minutes, test a small area with the spatula.&nbsp; If the hair does not come away from the test area easily, leave for a few minutes longer.&nbsp; <strong>Do not exceed 15 minutes in total.</strong></li>
					<li>Remove the cream using the spatula, then rinse the area immediately with plenty of warm water to remove all traces of the cream.&nbsp; <strong>Do not rub or use soap</strong>.</li>
				</ol>
				<p class="mt10"><strong>Cautions</strong></p>
				<p>Follow the instructions.&nbsp; Contains thioglycolate.&nbsp; Contains alkali.&nbsp; Keep out of reach of children.&nbsp; Avoid contact with eyes.&nbsp; In the event of contact with eyes, rinse immediately with plenty of water and seek medical advice.&nbsp; Hair removers can be used anywhere except around eyes, in nose, ears or on breast nipples, perianal or vaginal/genital areas.&nbsp; Avoid contact with other materials as this could lead to damage or discolouration.&nbsp; Do not use on irritated, inflamed or broken skin.&nbsp; If you feel burning or stinging, remove product immediately and rinse well with water.&nbsp; Store at room temperature.&nbsp; If stored at a low temperature, crystals may form.</p>
			</div>
			
			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p>Aqua, paraffinum liquidum, calcium thioglycolate, cetearyl alcohol, calcium hydroxide, sodium hydroxide, ceteth-20, silica, synthetic beeswax, parfum, propylene glycol, lilium candidum flower extract, methylparaben, propylparaben, hydrogenated cotton seed oil, butylphenyl methylpropional, benzyl benzoate, limonene, linalool, alpha-isomethyl ionone, citronellol.</p>
			</div>
			
			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>
	<div class="copy">
		<div class="col1">
			<h1>Tough Hair</h1>
			<p><span>Remove&hellip;</span> unwanted tough and dark hairs for up to 7 days at a time.</p>
			<p><span>Enriched&hellip;</span> with lily flower extract and cotton seed oil to gently but effectively remove tough hair as well as moisturise and nourish skin, to leave it soft and smooth to the touch.</p>
			<p><span>Top Tip&hellip;</span> keep your hands cream free by using the spatula provided to apply and remove cream.</p>
			<p><span>Cares&hellip;</span> gentle formula designed specifically for tough or dark hair.</p>
		</div>

		<div class="col2">
			<div class="packshot">
				<img src="/images/main/products/nair-tough-hair.jpg"/>
			</div>

			<div class="buttons">
				<img src="/images/main/products/smooth-icons/nair-tough-hair.png" class="mb15"/>
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions"class="btn">Instructions</a>
					<a href="http://www.boots.com/en/Nair-Tough-Hair-Hair-Removal-Cream-200ml_1049447/" target="_blank" class="btn dark">Buy now</a>
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<!-- <a href="/argan-oil/face-and-eyebrow-roll-on-wax"><img src="/images/main/products/suggested/argan-face-and-eyebrow-roll-on-wax.jpg" alt="Face and Eyebrow Roll-On Wax" title="Face and Eyebrow Roll-On Wax"/></a> -->
			<a href="/nair-collection/sensitive"><img class="alt-height-1" src="/images/main/products/suggested/nair-sensitive.jpg" alt="Sensitive Cream" title="Sensitive Cream"/></a>
			<a href="/japanese-cherry-blossom/body-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-body-3d-pack-suggested.jpg" alt="7 in 1 Body Wax Strips" title="7 in 1 Body Wax Strips"/></a>
		</div>
	</div>

</div>

@endsection