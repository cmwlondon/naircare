@extends('Main.Layouts.main')

@section('content')
				<div id="container" class="gcb">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>

			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p>Rub the desired amount gently into skin. Use daily for ultimate results or use directly after hair removal; shaving, waxing or using depilatory creams.
					Avoid contact with eyes.
					For external use only.</p>
			</div>

			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p>Aqua, Cetyl Alcohol, Stearyl Alcohol, Glyceryl Stearate SE, Isopropyl Palmitate, Ceteareth-20, Salicylic Acid, Aloe Barbadensis Extract, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Ribes Nigrum (Black Currant Seed) Oil, Helianthus Annuus (Sunflower) Seed Oil, Rosmarinus Officinalis (Rosemary) Extract, Cardiospermum Halicacabum Flower/Leaf/Vine Extract, Tocopherol, Helianthus Annuus Seed Oil Unsaponifiables, Allantoin, Maltodextrin, Octyldodecanol, Sodium Hydroxide, Phenoxyethanol.</p>
			</div>

			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>

	<div class="copy">
		<div class="col1">
			<h1>Post Hair Removal <br>Triple Action Balm</h1>
			<p><span>Moisturise&hellip;</span> with this soothing balm which is clinically proven to minimise ingrown hairs, while calming & moisturing the skin.</p>
			<p><span>Beautiful smooth skin&hellip;</span> Hair removal can often leave skin feeling dry, irritated or inflamed. With natural extracts of Aloe Vera & Sweet Almond Oil, Post Hair Removal Balm leaves skin soft, beautiful and smoother for longer.</p>
			<p><span>Use every day&hellip;</span> Suitable for use every day so you can enjoy beautiful smooth skin every day of the week.</p>
		</div>

		<div class="col2">
			<div class="packshot">
				<img src="/images/main/products/2017-products/triple-action-balm.jpg"/>
			</div>

			<div class="buttons">
				<!-- <img src="/images/main/products/smooth-icons/japanese-cherry-blossom-body-wax-strips.png" class="mb15"/> -->
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions" class="btn">Instructions</a>
					<!-- <a href="http://www.boots.com/en/Nair-Tough-Hair-Hair-Removal-Cream-200ml_1049447/" target="_blank" class="btn dark">Buy now</a> -->
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<!-- <a href="/argan-oil/face-and-eyebrow-roll-on-wax"><img src="/images/main/products/suggested/argan-face-and-eyebrow-roll-on-wax.jpg" alt="Face and Eyebrow Roll-On Wax" title="Face and Eyebrow Roll-On Wax"/></a> -->
			<a href="/nair-collection/sensitive"><img class="alt-height-1" src="/images/main/products/suggested/nair-sensitive.jpg" alt="Sensitive Cream" title="Sensitive Cream"/></a>
			<a href="/japanese-cherry-blossom/body-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-body-3d-pack-suggested.jpg" alt="7 in 1 Body Wax Strips" title="7 in 1 Body Wax Strips"/></a>
			<a href="/nair-collection/triple-action-cream"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/triple-action-cream-suggested.jpg" alt="Cleanse & Shave Triple Action Cream" title="Cleanse & Shave Triple Action Cream"/></a>
		</div>
	</div>

</div>
@endsection