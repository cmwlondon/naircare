@extends('Main.Layouts.main')

@section('content')
				<div id="container" class="gcb">
	<a name="instructions"></a>
<a name="ingredients"></a>
<a name="reviews"></a>
<div id="overlay_master" class="masker hidden">
	<div id="overlay_panel">
		<img src="/images/main/products/overlay-panel.png" width="412" height="396" class="bg"/>
		<div class="inner">
			<menu class="overlay-menu">
				<li><a href="#instructions" id="b_inner_instructions">Instructions</a></li>
				<li>|</li>
				<li><a href="#ingredients" id="b_inner_ingredients">Ingredients</a></li>
				<li>|</li>
				<li><a href="#reviews" id="b_inner_reviews">Reviews</a></li>
			</menu>
			<div class="closer"><a href="#" id="bClose">close</a></div>

			<!-- Instructions -->
			<div id="p_instructions" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Instructions</strong></p>
				<p>Rub the desired amount gently into damp skin, always shave in the same direction as the hair grows.
					 Follow with Nair Post Hair Removal Triple Action Balm for ultimate results.
				</p>
			</div>

			<!-- Ingredients -->
			<div id="p_ingredients" class="panel_content skyBlue hidden">
				<p class="mt10"><strong>Ingredients</strong></p>
				<p>Aqua, Sodium Laureth Sulfate, Sodium Chloride, Cocamidopropyl Betaine, Argania Spinosa (Argan) Oil, Prunus Amygdalus Dulcis (Sweet Almond) Oil, Plukenetia Volubilis Seed Oil, Polymnia Sonchifolia Root Juice, Glycerin, Lactic Acid, Alpha-Glucan Oligosaccharide, Maltodextrin, Lysine, Lactobacillus, Sodium Lactate, Carbomer, Triethyl Citrate, Sodium Hydroxide, Parfum, Polyquaternium-39, Sodium Carbonate, Sodium Benzoate, Dichlorobenzyl Alcohol, 2-Bromo-2-Nitropropane-1, 3-Diol, Cl 77891.</p>
			</div>

			<!-- Reviews -->
			<div id="p_reviews" class="panel_content hidden">
				@include('Main.Product.partials.product_reviews')
				@include('Main.Product.partials.product_review_form')
			</div>
		</div>
	</div>
</div>

	<div class="copy">
		<div class="col1">
			<h1>Cleanse &amp; Shave <br> Triple Action Cream</h1>
			<p><span>Cleanse&hellip;</span> with a luxurious cream that is clinically proven to leave skin soft, smooth and moisturised.</p>
			<p><span>Easy and effective smooth shaving&hellip;</span> contains extracts of Almond, Inca Inchi, Argan Oil and a moisture-boosting complex, to reduce shaving bumps and soothe irritation, leaving your skin gorgeously smooth and soft. </p>
			<p><span>Quick and easy to use&hellip;</span> suitable for use in the shower so you can cleanse, shave and go!</p>
		</div>

		<div class="col2">
			<div class="packshot">
				<img src="/images/main/products/2017-products/triple-action-cream.jpg"/>
			</div>

			<div class="buttons">
				<!-- <img src="/images/main/products/smooth-icons/japanese-cherry-blossom-body-wax-strips.png" class="mb15"/> -->
				<div class="btn-group-mobile">
					<a href="#reviews" id="bReview" class="btn">Reviews</a>
					<a href="#instructions" id="bInstructions" class="btn">Instructions</a>
					<!-- <a href="http://www.boots.com/en/Nair-Tough-Hair-Hair-Removal-Cream-200ml_1049447/" target="_blank" class="btn dark">Buy now</a> -->
				</div>
			</div>
		</div>
	</div>

	<div class="suggested-products">
		<div class="inner">
			<h3>You may also like...</h3>
			<!-- <a href="/argan-oil/face-and-eyebrow-roll-on-wax"><img src="/images/main/products/suggested/argan-face-and-eyebrow-roll-on-wax.jpg" alt="Face and Eyebrow Roll-On Wax" title="Face and Eyebrow Roll-On Wax"/></a> -->
			<a href="/nair-collection/sensitive"><img class="alt-height-1" src="/images/main/products/suggested/nair-sensitive.jpg" alt="Sensitive Cream" title="Sensitive Cream"/></a>
			<a href="/japanese-cherry-blossom/body-wax-strips"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/nair-jcb-body-3d-pack-suggested.jpg" alt="7 in 1 Body Wax Strips" title="7 in 1 Body Wax Strips"/></a>
			<a href="/nair-collection/triple-action-balm"><img class="alt-height-1" src="/images/main/products/2017-products/suggested/triple-action-balm-suggested.jpg" alt="Post Hair Removal Triple Action Balm" title="Post Hair Removal Triple Action Balm"/></a>
		</div>
	</div>

</div>

@endsection