					<div id="theForm">
						<h1>How was it for <strong>you?</strong></h1>

						{!! Form::open(array('url' => ['#'], 'method' => 'POST', 'id' => 'frmReview') )!!}		
							<div class="form_row">
								{!! Form::label('frnName', 'Name:') !!}
								<div class="stars-select">
									<a href="#" data-id="1"></a>
									<a href="#" data-id="2"></a>
									<a href="#" data-id="3"></a>
									<a href="#" data-id="4"></a>
									<a href="#" data-id="5"></a>
								</div>
								<input type="hidden" name="stars" id="stars" value="5" />
								{!! Form::hidden('stars',null,['id' => 'stars']) !!}
								{!! Form::text('frmName',null,['id' => 'frmName', 'class' => 'skyBlueInput inputText']) !!}
								
							</div>
							<div id="phone-stars" class="form_row">
								{!! Form::label('stars', 'Stars:') !!}
								<div class="stars-select phone">
									<a href="#" data-id="1"></a>
									<a href="#" data-id="2"></a>
									<a href="#" data-id="3"></a>
									<a href="#" data-id="4"></a>
									<a href="#" data-id="5"></a>
								</div>
							</div>
							<div class="form_row">
								<label>Comment:</label>
								{!! Form::textarea('frmComment',null,['placeholder' => '', 'id' => 'frmComment', 'rows' => '4', 'class' => 'skyBlueInput inputTextArea']) !!}
							</div>
							<input type="button" class="submit" value="Submit"/>
						{!! Form::close() !!}
					</div>
					<div id="theThanks" class="hidden">
						<h1 style="margin-left:0;">Thank you!</h1>
						<p>Your review has been processed and will need to be verified by a team member before going live.</p>
					</div>
