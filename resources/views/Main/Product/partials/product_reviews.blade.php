<div id="reviews_list">
	@if (count($reviews) > 0)
	@foreach($reviews as $review)
	<div id="review-container">
		<p>&ldquo;{{ $review->r_comment }}&rdquo;</p>
		<div id="customer-stars-select" class="s{{ $review->r_rating }}"></div>
		<p class="name">{{ $review->r_name }}</p>
	</div>
	@endforeach
	@else
	<div id="review-container"><p>No reviews found</p></div>
	@endif
</div>
