@extends('Main.Layouts.main')

@section('content')
<div id="container">
	<div id="main-info" class="scroll">
		<h2>Unapproved revies</h2>
		
		<ul>
			@foreach($reviewList as $review)
			<li>
				<p>Name: <strong>{{ $review['r_name'] }}</strong> Product: <strong>{{ $review['r_product'] }}</strong> Rating: {{ $review['r_rating'] }} Date: {{ date('D, d M Y H:i:s',$review['r_when']) }}<br>Comment:<br>{{ $review['r_comment'] }}</p>
				<a href="/product/approve-review/{{ $review['r_id'] }}">Approve form</a>
			</li>
			@endforeach
		</ul>
	</div>
</div>
@endsection
