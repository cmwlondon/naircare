@extends('Main.Question.frame')

@section('mainimage')
<img src="/images/main/your-questions/nair-shower-power-cream-group2.jpg" />
@endsection

@section('column')
      <h3 class="question-highlighted"><a href="#">Why use Nair creams?</a></h3>
      <div>
        <p>They're quick, effective and easy to use. In as little as 3 minutes you can be free of unwanted hair and left with beautifully smooth skin without nicks or cuts.</p>
      </div>

      <h3><a href="#">How often can I use Nair creams?</a></h3>
      <div>
        <p>You should leave at least 72 hours between subsequent applications of Nair creams.</p>
      </div>

      <h3><a href="#">How long should I leave cream on for?</a></h3>
      <div>
        <p>This varies by each Nair product but we recommend that you leave creams on from a minimum of 3 minutes then test to see if the hair comes away easily. If not, leave for a bit longer but do not exceed 10 minutes.</p>
      </div>

      <h3><a href="#">What should I use to wash cream off of my skin?</a></h3>
      <div>
        <p>For best results we recommend that you wash cream away using a wet flannel.</p>
      </div>

      <h3><a href="#">Do depilatories also exfoliate and moisturise skin?</strong></a></h3>
      <div>
        <p>Yes, in addition to dissolving hair Nair depilatories help remove unwanted dead cells on the skins surface. Nair depilatories are also packed full of caring ingredients, to moisturise and nourish your skin as they remove hair.</p>
      </div>

      <h3><a href="#">Will this product affect how my hair grows back?</a></h3>
      <div>
        <p>It won't change the rate at which your hair grows or the thickness of the hair. However, because the hair is dissolved rather than cut off at the skin level, re-growth will appear reduced compared to shaving.</p>
      </div>
@endsection
