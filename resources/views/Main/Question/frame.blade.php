@extends('Main.Layouts.main')

@section('content')
<div id="container">
     <div id="main-image">
          @yield('mainimage')
     </div>
     <div id="right-content">
          <h2>Your <strong>questions</strong></h2>

          <ul id="question-tab">
          	@foreach ($sections as $item)
          	<li @if ($section === $item['name']) id="active" @endif><a href="{{ $item['link'] }}">{{ $item['label'] }}</a></li>
          	@endforeach
          </ul>

          <div id="questions">
          	@yield('column')
          </div>
     </div>
</div>

@endsection
