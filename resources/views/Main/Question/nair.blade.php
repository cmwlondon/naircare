@extends('Main.Question.frame')

@section('mainimage')
<img src="/images/main/your-questions/nair-group1.jpg" class="home"/>
@endsection

@section('column')
   <h3 class="question-highlighted"><a href="#">What&#39;s the difference between new Nair with Argan Oil products and the rest of the Nair range?</a></h3>
   <div>
        <p>The makers of Nair never stop seeking new ways to help make skin smoother; so they&rsquo;ve tapped into one of nature&rsquo;s most valuable assets by infusing &ldquo;liquid gold&rdquo; Argan Oil into their latest hair removal launch. Argan Oil is a source of inner and outer beauty that delivers powerful benefits, including soft, supple and glowing skin.</p>
   </div>

   <h3><a href="#">Why should I always do a patch test before using Nair products?</a></h3>
   <div>
        <p>Irritation or allergic reaction can occur with some people, even if they've used a product before without any effect. Therefore a test should always be carried out on a small part of the area to be treated before each use. Follow the pack instructions and if the skin appears normal after 24 hours, proceed with full application.</p>
   </div>

   <h3><a href="#">What if I experience a reaction after a patch test?</a></h3>
   <div>
        <p>Then we advise that you cease use of the product immediately and rinse the area thoroughly with warm water. If in doubt, seek medical advice.</p>
   </div>

   <h3><a href="#">What products should I use if I have sensitive skin?</a></h3>
   <div>
        <p>We would recommend that you use Nair Sensitive Cream or Soothing Wax Strips.</p>
   </div>

   <h3><a href="#">Are there any Nair products which are suitable for shaving?</a></h3>
   <div>
        <p>Yes, Nair Cleanse &amp; Shave Triple Action Cream has been specially designed to gently cleanse while allowing you to always enjoy a luxurious yet effective smooth shave. It&rsquo;s clinically approved to help skin stay moisturised & smoother for longer.</p>
   </div>

   <h3><a href="#">What products can I use on my skin after waxing?</a></h3>
   <div>
        <p>Nair Post Hair Removal Triple Action Balm is perfect for use after your hair removal routine. This is designed specifically to be gentle & caring on your skin. The formula helps to minimise ingrown hairs, moisturise skin and reduce redness and bumps, commonly caused by shaving, waxing or using depilatory creams.</p>
   </div>
@endsection
