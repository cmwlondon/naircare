@extends('Main.Question.frame')

@section('mainimage')
<img src="/images/main/your-questions/nair-washable-roll-on-wax-group3.jpg" style="max-height:100%;"/>
@endsection

@section('column')
<h3 class="question-highlighted"><a href="#">How long does my hair need to be before waxing?</a></h3>
<div>
    <p>Waxes can differ in terms of the length of hair required before waxing. The general guideline for Nair waxes is about 2mm in length.</p>
</div>

<h3><a href="#">Can I use a wax strip more than once?</a></h3>
<div>
    <p>Yes, Nair wax strips can be re-used but only within a single hair removal application. Re-use a strip by folding it in half on itself and rubbing between the palms, before unfolding and using again.</p>
</div>

<h3><a href="#">How long will the waxing effect last?</a></h3>
<div>
    <p>Results can last for several weeks. If hair re-grows within a day or two we&rsquo;d recommend practicing your technique as you may be snapping off the hair rather than removing from the root. See pack for detailed instructions.</p>
</div>

<h3><a href="#">If I run out of post-wax wipes, is there another way to remove any wax residue?</a></h3>
<div>
    <p>Yes, if using Nair Wax Strips use baby oil to remove any remaining wax. If using Nair Roll-On use warm water and a clean cloth.</p>
</div>

@endsection
