<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// home
Route::get('/','Main\HomeController@index');

// Product hubs
Route::get('/hub/{section}','Main\HubController@index');

// Products
Route::get('/japanese-cherry-blossom/{product}','Main\ProductController@index')->name('japanese-cherry-blossom');
Route::get('/argan-oil/{product}','Main\ProductController@index')->name('argan-oil');
Route::get('/nair-collection/{product}','Main\ProductController@index')->name('nair-collection');

// product reviews - ajax/email routes
Route::post('/product/addreview','Main\ProductController@addReview')->name('add-review');
Route::get('/product/approve-review/{id}','Main\ProductController@approveReviewForm')->name('approve-review-form'); // might need to put this route behind login
Route::get('/product/approve-review-action/{id}','Main\ProductController@approveReview')->name('approve-review');
Route::get('/product/unapproved-reviews','Main\ProductController@listUnapprovedReviews')->name('unapproved-reviews');

// consult
Route::get('/online-consultation','Main\ConsultationController@index');
Route::get('/online-consultation/start','Main\ConsultationController@questions');
Route::get('/online-consultation/results/{choices}','Main\ConsultationController@results');

// effective hair removal
Route::get('/effective-hair-removal/{section?}','Main\EffectiveController@index');

// questions
Route::get('/faqs/{section?}','Main\QuestionController@index');

// contactus
Route::get('/contact-us','Main\ContactController@index');
Route::post('/contact-us','Main\ContactController@store');
Route::get('/contact-us/images','Main\ContactController@images');
Route::get('/contact-us/press-releases','Main\ContactController@press_releases');

// misc
Route::get('/terms-and-conditions','Main\MiscController@terms');
Route::get('/cookie-notice','Main\MiscController@cookies');
Route::get('/privacy-policy','Main\MiscController@privacy');

Route::get('/mailtest','Main\MiscController@mailtest');

Route::get('/sitemap', 'Main\SitemapController@refresh');

Route::get('/gdpr', function () {
    $exitCode = Artisan::call('gdpr:check', []);
    echo $exitCode;
});

